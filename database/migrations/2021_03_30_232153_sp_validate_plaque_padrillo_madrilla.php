<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpValidatePlaquePadrilloMadrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `validate_plaque_padrillo_madrilla`(in p_madrilla bigint(20))
        BEGIN
            select category from specimens where plate = p_madrilla and status = 1;
        END";
        DB::unprepared("DROP procedure IF EXISTS validate_plaque_padrillo_madrilla");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
