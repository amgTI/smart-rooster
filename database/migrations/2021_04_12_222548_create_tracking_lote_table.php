<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingLoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_lote', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('lote_id');
            $table->unsignedBigInteger('user_id');
            $table->enum('status', ['CREACION LOTE', 'REVISION INFERTILES', 'REVISION VIVOS', 'NACIMIENTO POLLOS', 'NACEDORA']);
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_lote');
    }
}
