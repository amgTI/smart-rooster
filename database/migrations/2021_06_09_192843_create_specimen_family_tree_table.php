<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecimenFamilyTreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specimen_family_tree', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->integer('specimen_id')->nullable();
            $table->string('alias')->nullable();
            $table->string('plate')->nullable();
            $table->date('dob')->nullable();
            $table->enum('type', ['EJEMPLAR', 'MADRE', 'PADRE'])->nullable();
            $table->integer('created_by');
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specimen_family_tree');
    }
}
