<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetVideoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_video_id`(in idvideo int)
        BEGIN   
            select v.type_of_video + 0 type_of_video, v.type_of_rival + 0 type_of_rival, v.amarrador, v.careador,
                    v.event_result , v.event_date, v.description, v.id, v.idspecimen, if(v.type_of_video = 2 or v.type_of_rival = 2,v.rival_id, s.plate) rival,
                    r.name rival_name
            from video v
                left join specimens s on s.id = v.rival
                left join rivals r on r.id = v.rival_id
            where v.id = idvideo;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_video_id");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
