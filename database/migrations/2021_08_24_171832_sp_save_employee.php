<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_employee`(
            in t_employee_id int,
            in t_first_name varchar(255),
            in t_middle_name varchar(255),
            in t_last_name varchar(255),
            in t_known_as varchar(255),
            in t_doc_type int,
            in t_document_number varchar(255),
            in t_doe varchar(255),
            in t_nationality varchar(255),
            in t_dob varchar(255),
            in t_gender int,
            in t_martial_status int,
            in t_mailling_address varchar(255),
            in t_city varchar(255),
            in t_state varchar(255),
            in t_zip_code varchar(255),
            in t_country varchar(255),
            in t_home_phone varchar(255),
            in t_mobile_phone varchar(255),
            in t_work_phone varchar(255),
            in t_work_email varchar(255),
            in t_private_email varchar(255),
            in t_contact_name varchar(255),
            in t_contact_phone varchar(255),
            in t_relation int,
            in t_contact_name_second varchar(255),
            in t_contact_phone_second varchar(255),
            in t_relation_second int,
            in t_start_date varchar(255),
            in t_end_date varchar(255),
            in t_position varchar(255),
            in t_salary decimal(11,2),
            in t_created_by int
        )
        BEGIN
            if(t_employee_id is null) then
                insert
                    into
                    employees (first_name,
                    middle_name,
                    last_name,
                    known_as,
                    doc_type,
                    document_number,
                    doe,
                    nationality,
                    dob,
                    gender,
                    martial_status,
                    mailling_address,
                    city,
                    state,
                    zip_code,
                    country,
                    home_phone,
                    mobile_phone,
                    work_phone,
                    work_email,
                    private_email,
                    contact_name,
                    contact_phone,
                    relation,
                    contact_name_second,
                    contact_phone_second,
                    relation_second,
                    start_date,
                    end_date,
                    position,
                    salary,
                    created_by,
                    created_at,
                    updated_at)
                values(t_first_name,
                    t_middle_name,
                    t_last_name,
                    t_known_as,
                    t_doc_type,
                    t_document_number,
                    t_doe,
                    t_nationality,
                    t_dob,
                    t_gender,
                    t_martial_status,
                    t_mailling_address,
                    t_city,
                    t_state,
                    t_zip_code,
                    t_country,
                    t_home_phone,
                    t_mobile_phone,
                    t_work_phone,
                    t_work_email,
                    t_private_email,
                    t_contact_name,
                    t_contact_phone,
                    t_relation,
                    t_contact_name_second,
                    t_contact_phone_second,
                    t_relation_second,
                    t_start_date,
                    t_end_date,
                    t_position,
                    t_salary,
                    t_created_by,
                    now(),
                    now());
                select @@identity employee_id;
            else

                update
                    employees
                set
                    first_name = t_first_name,
                    middle_name = t_middle_name,
                    last_name = t_last_name,
                    known_as = t_known_as,
                    doc_type = t_doc_type,
                    document_number = t_document_number,
                    doe = t_doe,
                    nationality = t_nationality,
                    dob = t_dob,
                    gender = t_gender,
                    martial_status = t_martial_status,
                    mailling_address = t_mailling_address,
                    city = t_city,
                    state = t_state,
                    zip_code = t_zip_code,
                    country = t_country,
                    home_phone = t_home_phone,
                    mobile_phone = t_mobile_phone,
                    work_phone = t_work_phone,
                    work_email = t_work_email,
                    private_email = t_private_email,
                    contact_name = t_contact_name,
                    contact_phone = t_contact_phone,
                    relation = t_relation,
                    contact_name_second = t_contact_name_second,
                    contact_phone_second = t_contact_phone_second,
                    relation_second = t_relation_second,
                    start_date = t_start_date,
                    end_date = t_end_date,
                    position = t_position,
                    salary = t_salary,
                    updated_at = now()
                where
                    id = t_employee_id;
                select t_employee_id employee_id;
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_employee");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
