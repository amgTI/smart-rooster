<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_tasks_table`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare cc int;

            select
                count(*)
            into
                cc
            from
                tasks t
            join users u on
                u.id = t.created_by
            where
                ( campo is null
                or concat(u.name, ' ', u.last_name) like concat('%', campo, '%')
                or t.task like concat('%', campo, '%') and t.deleted_at is null);

            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select
                        t.id id, t.task task, t.day day, t.module module, t.module+0 module_number, u.name first_name, u.last_name last_name, t.created_at created_at, \", cc ,\" cc from tasks t join users u on u.id = t.created_by

            where \", if(campo is null, true, concat(\" concat(u.name, ' ', u.last_name) like '%\", campo, \"%'  or t.task like '%\", campo, \"%'\")), \" and t.deleted_at is null
                order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_tasks_table");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
