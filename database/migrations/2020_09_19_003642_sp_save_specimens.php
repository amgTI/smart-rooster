<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveSpecimens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_specimens`(in iduser int, in t_origin int,in t_alias varchar(255), in t_plate varchar(255), in t_additional varchar(255),in t_breeder varchar(255),in t_breederid int,in t_price decimal, in t_color varchar(255),in t_category int, in t_gender int,in t_status int ,in t_dob date,in t_obs text,in t_update int, in t_photo text,in t_thumb text,in t_currency int,t_url_evidence text,in t_url_evidence_thumb text, in t_description_evidence varchar(255),in t_death_date date,in t_definition varchar(255))
        BEGIN
		declare id_specimen int;
        declare routeevidence text default('ejemplares/imagesEjemplares/');
        declare status_specimen int;
        declare idtracking int;
		START TRANSACTION;

                if(t_update is null) then
            
                    insert into specimens(created_by,origin,alias,definition,plate,additional_plate,breeder,breeder_id,currency, price,color,category,gender,status,dob,observations,created_at,updated_at)
                        values(iduser,t_origin,t_alias,t_definition,t_plate,t_additional,t_breeder,t_breederid,t_currency,t_price,t_color,t_category,t_gender,t_status,t_dob,t_obs,now(),now());
                        
                    select @@identity into id_specimen;
                    
                    if(t_status = 3)then
						insert into tracking_status_specimen(idspecimen,user_id,status,url_evidence,url_evidence_thumb,description,death_date,created_at,updated_at) 
                        values(id_specimen,iduser,t_status,concat(routeevidence,id_specimen,'/',t_url_evidence),concat(routeevidence,id_specimen,'/',t_url_evidence_thumb),t_description_evidence,t_death_date,now(),now());
                    end if;
                    
                    if(t_photo is not null) then
                        update specimens set photo  = concat('ejemplares/imagesEjemplares/',id_specimen,'/',t_photo) where id = id_specimen;
                        update specimens set thumb  = concat('ejemplares/imagesEjemplares/',id_specimen,'/',t_thumb) where id = id_specimen;
                    end if;
                    select id_specimen;
                    
                else 
					if(t_photo is not null) then
                        update specimens set photo  = concat('ejemplares/imagesEjemplares/',t_update,'/',t_photo) where id = t_update;
                        update specimens set thumb  = concat('ejemplares/imagesEjemplares/',t_update,'/',t_thumb) where id = t_update;
                    end if;
                    
                    select status into status_specimen from specimens where id = t_update;
                    
                    select id into idtracking
					 from tracking_status_specimen 
					 where idspecimen = t_update  order by created_at desc limit 1;
                     
                    if(status_specimen <> t_status)then
                    
						insert into tracking_status_specimen(idspecimen,user_id,status,url_evidence,url_evidence_thumb,description,death_date,created_at,updated_at) 
                        values(t_update,iduser,t_status,concat(routeevidence,t_update,'/',t_url_evidence),concat(routeevidence,t_update,'/',t_url_evidence_thumb),t_description_evidence,t_death_date,now(),now());
                        
                    end if;
					 
					if(status_specimen = t_status)then
						update tracking_status_specimen set description = t_description_evidence, death_date = t_death_date, updated_at = now() where id = idtracking;
                      
                    end if;
                    
                   
					if(status_specimen = t_status and t_url_evidence is not null)then
						update tracking_status_specimen set url_evidence = concat(routeevidence,t_update,'/',t_url_evidence), url_evidence_thumb = concat(routeevidence,t_update,'/',t_url_evidence_thumb) where id = idtracking;
					end if;
                    
                    
                    update specimens set origin = t_origin, alias = t_alias,definition = t_definition ,plate = t_plate,additional_plate = t_additional,
                    breeder = t_breeder,breeder_id = t_breederid,currency = t_currency , price = t_price, color = t_color, category = t_category,gender = t_gender, status = t_status, dob = t_dob, observations = t_obs,updated_at = now()  where id = t_update;
                    

                    select t_update as id_specimen;
            end if;
    
		commit;
    
        END";
        DB::unprepared("DROP procedure IF EXISTS save_specimens");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
