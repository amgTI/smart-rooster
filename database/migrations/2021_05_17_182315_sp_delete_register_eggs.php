<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDeleteRegisterEggs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `delete_register_eggs`(in t_eggs_id int, in t_elimination_comment text, in t_deleted_by int)
        BEGIN
            update eggs set elimination_comment = t_elimination_comment, deleted_by = t_deleted_by, deleted_at = now() where id = t_eggs_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS delete_register_eggs");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
