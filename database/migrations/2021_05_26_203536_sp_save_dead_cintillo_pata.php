<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveDeadCintilloPata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_dead_cintillo_pata`(in t_cintillo_pata_id int, in t_quantity int, in t_action int, in t_dead_specimen_id int, in t_postura_id int, in t_lote_id int)
        BEGIN
            declare t_lote_postura_id int;
            declare t_total_quantity_born_eggs int;
            declare t_total_quantity_dead_eggs int;

            select lp.id into t_lote_postura_id from lote_posturas lp where lp.postura_id = t_postura_id and lp.lote_id = t_lote_id;


            if(t_action = 1) then
                update dead_specimens set lote_postura_id = t_lote_postura_id where id = t_dead_specimen_id;
                select lp.quantity_born_eggs - t_quantity into t_total_quantity_born_eggs from lote_posturas lp where lp.id = t_lote_postura_id;
                select lp.quantity_dead_eggs + t_quantity into t_total_quantity_dead_eggs from lote_posturas lp where lp.id = t_lote_postura_id;
            else
                select lp.quantity_born_eggs + t_quantity into t_total_quantity_born_eggs from lote_posturas lp where lp.id = t_lote_postura_id;
                select lp.quantity_dead_eggs - t_quantity into t_total_quantity_dead_eggs from lote_posturas lp where lp.id = t_lote_postura_id;
            end if;
            update lote_posturas set quantity_born_eggs = t_total_quantity_born_eggs where id = t_lote_postura_id;
            update lote_posturas set quantity_dead_eggs = t_total_quantity_dead_eggs where id = t_lote_postura_id;

        END";
        DB::unprepared("DROP procedure IF EXISTS save_dead_cintillo_pata");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
