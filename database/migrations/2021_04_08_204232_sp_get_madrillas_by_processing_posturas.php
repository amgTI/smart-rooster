<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetMadrillasByProcessingPosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_madrillas_by_processing_posturas`()
        BEGIN
            select
            s.id,
            s.alias,
            s.plate,
            p.remaining_eggs,
            p.removed_eggs,
            p.id postura_id
        from
            specimens s
        left join posturas p on
            p.placa_madrilla = s.plate
        where
            s.category = 2
            and p.status = 3
            and s.status = 1 order by s.plate*1 asc;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_madrillas_by_processing_posturas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
