<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetPosturasProcessingMadrillas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_posturas_processing_madrillas`(
        )
        BEGIN
        select
		p.*,
		s.alias madrilla_alias
	from
		posturas p
	join specimens s on
		s.plate = p.placa_madrilla
	where
		p.status = 3
		and p.remaining_eggs > 0 order by s.plate*1 asc;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_posturas_processing_madrillas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
