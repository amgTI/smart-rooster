<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chicks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cintillo_ala')->unique();
            $table->unsignedBigInteger('postura_id');
            $table->foreign('postura_id')->references('id')->on('posturas');
            $table->date('date_of_birth');
            $table->enum('gender', ['MACHO', 'HEMBRA', 'SIN ASIGNAR']);
            $table->enum('status', ['ACTIVO']);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chicks');
    }
}
