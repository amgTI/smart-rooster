<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetColorSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_color_specimen`(in t_gender int)
            BEGIN
                    select
                    id,
                    upper(color) color
                from
                    color_specimen
                where
                    if(t_gender is null, true, gender = t_gender)
                order by
                    color asc;
            END";
        DB::unprepared("DROP procedure IF EXISTS get_color_specimen");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
