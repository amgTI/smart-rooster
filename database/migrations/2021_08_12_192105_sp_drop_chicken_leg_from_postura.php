<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDropChickenLegFromPostura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `drop_chicken_leg_from_postura`(in t_postura_id int)
        BEGIN
            declare t_cintillo_pata_id int;
            select cpp.cintillo_pata_id into t_cintillo_pata_id from cintillo_pata_posturas cpp where cpp.postura_id = t_postura_id and cpp.status = 1;
            update cintillo_pata_posturas set status = 2 where postura_id = t_postura_id and status = 1;
            update cintillo_patas set status = 1 where id = t_cintillo_pata_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS drop_chicken_leg_from_postura");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
