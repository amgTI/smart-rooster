<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDataSelectSpecimens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `data_select_specimens`()
        BEGIN

        select (select json_arrayagg(JSON_OBJECT('id', id, 'description',description))from status_specimens )data_statuse,
        (select json_arrayagg(JSON_OBJECT('id', id, 'description',description))  from origin_specimens) data_origin,
        (select json_arrayagg(JSON_OBJECT('id', id, 'description',description, 'gender', gender, 'gender_number', gender+0))  from category_specimens) data_category,
        (select json_arrayagg(JSON_OBJECT('id', id, 'currency',currency,'description',description))  from types_currency) type_currency,
    (select json_arrayagg(JSON_OBJECT('id', id, 'name',name))  from rivals) rivals;
        END";
        DB::unprepared("DROP procedure IF EXISTS data_select_specimens");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
