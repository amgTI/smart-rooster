<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCintilloPataPosturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cintillo_pata_posturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cintillo_pata_id');
            $table->foreign('cintillo_pata_id')->references('id')->on('cintillo_patas');
            $table->unsignedBigInteger('postura_id');
            $table->foreign('postura_id')->references('id')->on('posturas');
            $table->enum('status', ['ACTIVA', 'FINALIZADA']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cintillo_pata_posturas');
    }
}
