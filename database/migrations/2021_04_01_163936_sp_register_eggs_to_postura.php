<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpRegisterEggsToPostura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `register_eggs_to_postura`(in t_egg_id int, in t_placa_madrilla varchar(255), in good_eggs int, in t_postura_id int, in t_egg_postura_id int)
        BEGIN

        declare total_good_eggs int;
       	declare current_number_eggs int;
       	if(t_egg_postura_id is not null) then
       		select (pg.number_eggs_good - ep2.quantity) into current_number_eggs from egg_postura ep2 join posturas pg on pg.id = ep2.postura_id where ep2.id = t_egg_postura_id;
       		select current_number_eggs + good_eggs into total_good_eggs;
       		update egg_postura set quantity = good_eggs where id = t_egg_postura_id;
       	else
        /* total_number_eggs */
            select
                p.number_eggs_good + good_eggs
            into
                total_good_eggs
            from
                posturas p
            where
                p.id = t_postura_id;
            /* update qtity of eggs */
            insert
                into
                egg_postura (eggs_id, postura_id, quantity, created_at, updated_at)
            values(t_egg_id, t_postura_id,
            good_eggs,
            now(),
            now());
         end if;
        update
                posturas p
            set
                p.number_eggs_good = total_good_eggs, p.remaining_eggs = total_good_eggs-p.removed_eggs
            where
                p.id = t_postura_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS register_eggs_to_postura");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
