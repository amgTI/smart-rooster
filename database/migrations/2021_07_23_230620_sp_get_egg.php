<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetEgg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_egg`(
            in t_egg_id int
        )
        BEGIN
        select ep.postura_id, ep.quantity number_eggs, ep.id, p.placa_madrilla from egg_postura ep join posturas p on p.id = ep.postura_id where ep.eggs_id = t_egg_id and ep.deleted_at is null;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_egg");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
