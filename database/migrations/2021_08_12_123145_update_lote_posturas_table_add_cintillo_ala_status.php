<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLotePosturasTableAddCintilloAlaStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lote_posturas', function (Blueprint $table) {
            $table->enum('cintillo_ala_status', ['SIN COLOCAR', 'COLOCADO'])->default('SIN COLOCAR')->after('quantity_dead_eggs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
