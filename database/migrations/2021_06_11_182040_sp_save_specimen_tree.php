<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveSpecimenTree extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_specimen_tree`(
            in t_parent_id int, in t_specimen_id int, in t_alias varchar(255), in t_plate varchar(255), in t_dob date, in t_type int, in t_created_by int
        )
        BEGIN
            insert
                into
                specimen_family_tree (parent_id,
                specimen_id,
                alias,
                plate,
                dob,
                type,
                created_by,
                created_at,
                updated_at)
            values (t_parent_id,
            t_specimen_id,
            t_alias,
            t_plate,
            t_dob,
            t_type,
            t_created_by,
            now(),
            now());
            select @@identity first_specimen;

        END";
        DB::unprepared("DROP procedure IF EXISTS save_specimen_tree");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
