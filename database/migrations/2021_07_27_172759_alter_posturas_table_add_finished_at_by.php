<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPosturasTableAddFinishedAtBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posturas', function (Blueprint $table) {
            $table->datetime('finished_at')->nullable()->after('updated_at');
            $table->integer('finished_by')->nullable()->after('finished_at');
        });

        $procedure = "update posturas set finished_at = now(), finished_by = 1 where status = 4";
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
