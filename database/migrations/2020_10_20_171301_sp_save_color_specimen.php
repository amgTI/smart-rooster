<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveColorSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  PROCEDURE `save_color_specimen`(in namecolor varchar(255), in t_gender int)
        BEGIN
            declare color_exists varchar(20) default null;
            select color into color_exists from color_specimen where color = namecolor;
            insert into color_specimen(color,gender,created_at,updated_at) values(namecolor,t_gender,now(),now());
                select color_exists;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_color_specimen");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
