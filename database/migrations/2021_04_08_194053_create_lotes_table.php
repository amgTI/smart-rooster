<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('placa_madrilla')->nullable();
            $table->bigInteger('total_eggs')->nullable();
            $table->integer('total_infertile_eggs')->nullable();
            $table->integer('total_born_eggs')->nullable();
            $table->integer('total_dead_eggs')->nullable();
            $table->date('date_lote');
            $table->integer('created_by');
            $table->enum('status', ['BORRADOR LOTE', 'EN PROCESO LOTE', 'BORRADOR INFERTILE', 'EN PROCESO INFERTILE', 'BORRADOR VIVOS', 'EN PROCESO VIVOS', 'BORRADOR NACIMIENTO', 'EN PROCESO NACIMIENTO', 'FINALIZADO', 'NACEDORA']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes');
    }
}
