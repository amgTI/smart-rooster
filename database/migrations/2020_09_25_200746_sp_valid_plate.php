<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpValidPlate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `validate_plate`(in t_plate varchar(255),in idspecimen int)
        BEGIN
                    declare p_plate varchar(255) default null;
                    declare vid int default null;
                    if(idspecimen is null) then
						select plate into p_plate from specimens where plate = t_plate;
                        select p_plate, idspecimen;
					else 
						select plate into p_plate from specimens where plate = t_plate and id = idspecimen;	
                        select id into vid from specimens where plate = t_plate;
                        select p_plate, idspecimen,vid;
                    end if;
            END";
        DB::unprepared("DROP procedure IF EXISTS validate_plate");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
