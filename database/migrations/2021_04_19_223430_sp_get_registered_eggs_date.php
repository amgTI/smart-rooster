<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetRegisteredEggsDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_registered_eggs_date`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare count_registered_eggs_date int;

			select count(*) into count_registered_eggs_date from (select
					sum(ep.quantity) suma,
					ep.egg_postura_date egg_postura_date,
					count(*) count_eg
				from
					egg_postura ep
				join posturas p on
					p.id = ep.postura_id
				group by 2) v where
                ( campo is null
                or v.egg_postura_date like concat('%', campo, '%'));
            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select v.suma, v.egg_postura_date, \", count_registered_eggs_date, \" count_registered_eggs_date, v.can_delete_until from (select
				sum(ep.quantity) suma,
				ep.egg_postura_date egg_postura_date,
				count(*) count_eg,
				ep.can_delete_until can_delete_until
			from
				egg_postura ep
			join posturas p on
				p.id = ep.postura_id
			group by 2,4) v

            where \", if(campo is null, true, concat(\" v.egg_postura_date like '%\", campo, \"%'\")), \"
                order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_registered_eggs_date");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
