<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetSpecimens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  PROCEDURE `get_specimens`(in iduser int,in npage int,in perpage int,in orderby varchar(4),in orden varchar(255),in campo text,in f_origin int, in f_color int,in f_category int, in f_status int, in f_rival int)
        BEGIN
        declare cc int;
        select count(*) into cc
        from specimens s
            join users u on u.id = s.created_by
                join origin_specimens os on os.id = s.origin
                join category_specimens cs on cs.id = s.category
                join status_specimens ss on ss.id = s.status
                left join types_currency tc on tc.id = s.currency
        where deleted_at is null
        and (campo is null or  s.plate like concat( '%',campo,'%') or s.alias like concat('%',campo, '%'))
        and if(f_origin is null, true,s.origin = f_origin)
        and if(f_color is null, true,s.color = f_color)
        and if(f_category is null, true,s.category = f_category)
        and if(f_status is null, true,s.status = f_status)
           and if(f_rival is null, true,s.breeder_id = f_rival);

        set npage=perpage*(npage-1);

        SET lc_time_names = 'es_ES';
    set @query=concat(\"select s.id, s.created_by,s.breeder_id,s.created_at,s.updated_at,os.description os_description,cs.description cs_description,\",cc,\" cc,
            ss.description s_description, s.plate, s.alias , s.additional_plate, s.breeder, s.price, s.color, s.countmovies,
            if(s.gender = 1, 'MACHO','HEMBRA') s_gender,concat(YEAR(FROM_DAYS(DATEDIFF(NOW(),s.dob))), ' años ',MONTH(FROM_DAYS(DATEDIFF(NOW(),s.dob))) ,' meses') s_dob,
            TIMESTAMPDIFF( YEAR, s.dob, now() ) as years
            , TIMESTAMPDIFF( MONTH, s.dob, now() ) % 12 as months
            , FLOOR( TIMESTAMPDIFF( DAY, s.dob, now() ) % 30.4375 ) as days,
            concat(concat(u.name,' ',ifnull(u.last_name,'')),' <br> ',DATE_FORMAT(s.created_at,'%d  %M  %Y') ,' ',DATE_FORMAT(s.created_at,'%r') ) created_at, s.observations, s.photo, ifnull(s.thumb,s.photo) thumb, tc.currency, s.breeder,
            (select DATE_FORMAT(death_date,'%d  %M  %Y')  from tracking_status_specimen where idspecimen = s.id order by created_at desc limit 1 ) death_date, s.status, s.created_at created_at_date
            from specimens s
                join users u on u.id = s.created_by
                join origin_specimens os on os.id = s.origin
                join category_specimens cs on cs.id = s.category
                join status_specimens ss on ss.id = s.status
                left join types_currency tc on tc.id = s.currency
            where  deleted_at is null
            \",
            if(campo is null,'',concat(\" and (s.plate like '%\",campo,\"%' or s.alias like '%\",campo, \"%')\")),
            if(f_origin is null,'',concat(\" and s.origin = \" , f_origin )),
            if(f_color is null,'',concat(\" and s.color = \" , f_color)),
            if(f_category is null,'',concat(\" and s.category = \" , f_category)),
            if(f_status is null,'',concat(\" and s.status = \", f_status)),
            if(f_rival is null,'',concat(\" and s.breeder_id = \", f_rival)), \"
            order by \",if(orden=10,\"s.plate*1\",orden),' ',orderby,\" limit \",perpage,\" offset \",npage,\";\");
            PREPARE stmt1 FROM @query;
            EXECUTE stmt1;
            DEALLOCATE PREPARE stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_specimens");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
