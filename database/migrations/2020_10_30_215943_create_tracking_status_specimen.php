<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingStatusSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_status_specimen', function (Blueprint $table) {
            $table->id();
            $table->integer('idspecimen')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('status')->nullable();
            $table->text('url_evidence')->nullable();
            $table->text('url_evidence_thumb')->nullable();
            $table->string('description')->nullable();
            $table->date('death_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_status_specimen');
    }
}
