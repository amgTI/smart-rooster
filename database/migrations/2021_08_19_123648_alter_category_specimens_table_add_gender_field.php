<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCategorySpecimensTableAddGenderField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_specimens', function (Blueprint $table) {
            $table->enum('gender', ['MACHO', 'HEMBRA'])->after('description');
        });

        $statement_padrillo = "update category_specimens set gender = 1 where description = 'PADRILLO'";
        $statement_madrilla = "update category_specimens set gender = 2 where description = 'MADRILLA'";
        $statement_gallo = "update category_specimens set gender = 1 where description = 'GALLO'";
        $statement_gallina = "update category_specimens set gender = 2 where description = 'GALLINA'";
        $statement_pollon = "update category_specimens set gender = 1 where description = 'POLLON'";
        $statement_polla = "update category_specimens set gender = 2 where description = 'POLLA'";

        DB::select($statement_padrillo);
        DB::select($statement_madrilla);
        DB::select($statement_gallo);
        DB::select($statement_gallina);
        DB::select($statement_pollon);
        DB::select($statement_polla);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
