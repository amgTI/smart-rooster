<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetGroupedPosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_grouped_posturas`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_status int, in t_placa_specimen varchar(255), in t_category_specimen int)
        BEGIN
        declare count_posturas int;

        select
            count(distinct p.placa_madrilla, p.placa_padrillo) into count_posturas
            from
            posturas p
            join lote_posturas lp on
            lp.postura_id = p.id
            join specimens s1 on
            s1.plate = p.placa_madrilla
            join specimens s2 on
            s2.plate = p.placa_padrillo
        where
            ( campo is null
            or s1.alias like concat('%', campo, '%')
            or s1.alias like concat('%', campo, '%')
            or p.placa_madrilla like concat('%', campo, '%')
            or p.placa_padrillo like concat('%', campo, '%') )
            and p.status = t_status
            and if(t_placa_specimen is null,
            true,
            if(t_category_specimen = 1,
            p.placa_padrillo = t_placa_specimen,
            p.placa_madrilla = t_placa_specimen));

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select p.placa_madrilla, p.placa_padrillo, sum(lp.quantity_born_eggs) pollos_vivos, s1.alias madrilla_alias, s2.alias padrillo_alias, s1.photo photo, s1.thumb thumb, \", count_posturas , \" cc from posturas p join lote_posturas lp on lp.postura_id = p.id join specimens s1 on s1.plate = p.placa_madrilla join specimens s2 on s2.plate = p.placa_padrillo

                                where  p.status = \", t_status, \" and( \", if(t_placa_specimen is null, true, if(t_category_specimen = 1, concat(\"p.placa_padrillo = '\", t_placa_specimen, \"'\"), concat(\"p.placa_madrilla = '\", t_placa_specimen, \"'\"))) , \")  and (\", if(campo is null, true, concat(\" s1.alias like '%\", campo, \"%' or s2.alias like '%\", campo, \"%' or p.placa_madrilla like '%\", campo, \"%' or p.placa_padrillo like '%\", campo, \"%'\")), \"
                                ) group by 1,2,4,5,6,7 order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_grouped_posturas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
