<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpAccessLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `access_log_report`(in status_access int ,in iduser int)
            BEGIN
                if(status_access = 1) then
                    insert into access_log(user_id,created_at) values(iduser,now());
                else
                    update access_log  set updated_at = now() where user_id = iduser order by id desc limit 1 ;
                end if;
            END";
        DB::unprepared("DROP procedure IF EXISTS access_log_report");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
