<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpCalculateDeadChicks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `calculate_dead_chicks`(in t_lote_postura_id int, in t_quantity int)
        BEGIN
        declare total_dead_chicks int;
        declare total_alive_chicks int;


        declare total_dead_chicks_lote int;
        declare total_alive_chicks_lote int;
        declare t_lote_id int;

        select (quantity_dead_eggs + t_quantity) into total_dead_chicks from lote_posturas where id = t_lote_postura_id;
        select (quantity_born_eggs - t_quantity) into total_alive_chicks from lote_posturas where id = t_lote_postura_id;


        update lote_posturas set quantity_dead_eggs = total_dead_chicks, quantity_born_eggs = total_alive_chicks where id = t_lote_postura_id;
        select lote_id into t_lote_id from lote_posturas where id = t_lote_postura_id;

        select (total_dead_eggs + t_quantity) into total_dead_chicks_lote from lotes where id = t_lote_id;
        select (total_born_eggs - t_quantity) into total_alive_chicks_lote from lotes where id = t_lote_id;

        update lotes set total_dead_eggs = total_dead_chicks_lote, total_eggs = total_alive_chicks_lote where id = t_lote_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS calculate_dead_chicks");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
