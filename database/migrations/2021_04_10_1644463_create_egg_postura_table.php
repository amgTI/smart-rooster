<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEggPosturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('egg_postura', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('eggs_id');
            $table->foreign('eggs_id')->references('id')->on('eggs');
            $table->unsignedBigInteger('postura_id');
            $table->foreign('postura_id')->references('id')->on('posturas');
            $table->integer('quantity');
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users');
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('egg_postura');
    }
}
