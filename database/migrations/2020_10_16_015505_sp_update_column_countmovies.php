<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class SpUpdateColumnCountmovies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  PROCEDURE `update_column_countmovies`()
        BEGIN	
                declare finished int;
                declare idspe int;
                declare curs cursor for	select s.id 
                    from specimens s 
                        join video v on v.idspecimen = s.id
                    group by s.id;
                    
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
                DECLARE EXIT HANDLER FOR SQLEXCEPTION SET finished = 2;	
                OPEN curs;
                SET finished = 0;	
                
                REPEAT
                FETCH curs INTO idspe;
                
                    update specimens s 
						join video v  on s.id = v.idspecimen
					set s.countmovies = if(v.deleted_at is null ,1 ,0) where s.id = idspe;
                   
                UNTIL finished END REPEAT;
                CLOSE curs;
            END";
        DB::unprepared("DROP procedure IF EXISTS update_column_countmovies");
        DB::unprepared($procedure);

        DB::select('call update_column_countmovies()');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
