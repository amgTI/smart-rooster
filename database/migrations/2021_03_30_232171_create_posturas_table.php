<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('placa_madrilla');
            $table->foreign('placa_madrilla')->references('plate')->on('specimens');
            $table->string('placa_padrillo');
            $table->foreign('placa_padrillo')->references('plate')->on('specimens');
            $table->integer('number_eggs_good')->default(0);
            $table->integer('removed_eggs')->default(0);
            $table->integer('remaining_eggs')->default(0);
            $table->longtext('observation')->nullable();
            $table->text('cancel_comment')->nullable();
            $table->text('activation_comment')->nullable();
            $table->text('finished_comment')->nullable();
            $table->enum('method', ['NATURAL', 'ARTIFICIAL']);
            $table->date('postura_date');
            $table->integer('created_by');
            $table->enum('status', ['ANULADA', 'PENDIENTE', 'EN PROCESO', 'FINALIZADA'])->default('PENDIENTE');
            $table->enum('seen_status', ['NOT SEEN', 'SEEN'])->default('NOT SEEN');
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posturas');
    }
}
