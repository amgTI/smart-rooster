<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCintilloAlaLote70 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $statement_first = 'delete from chicks where id in (select id from (select c.id from chicks c join lote_posturas lp on lp.id = c.lote_postura_id where lp.lote_id = 70) deleteds);';
        $statement_second = 'update lote_posturas set cintillo_ala_status = 1 where lote_id = 70;';
        $statement_three = 'update lotes set status = 11 where id = 70';
        DB::select($statement_first);
        DB::select($statement_second);
        DB::select($statement_three);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
