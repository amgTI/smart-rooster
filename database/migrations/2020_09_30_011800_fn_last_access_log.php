<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FnLastAccessLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  FUNCTION `last_access_log`( iduser int ) RETURNS varchar(25) CHARSET latin1
        BEGIN
            declare date_time varchar(25);
            
            select max(created_at) into date_time  
            from access_log 
            where user_id = iduser limit 1;
            
            RETURN date_time;
        END";
        DB::unprepared("DROP function IF EXISTS last_access_log");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
