<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveLoteTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_lote_task`(in t_task_id int, in t_lote_id int, in t_status int, in t_born_date date, in t_days int)
        BEGIN
                insert
                into
                lote_tasks (task_id,
                lote_id,
                delivery_date,
                status,
                created_at,
                updated_at)
            values (t_task_id,
            t_lote_id,
            DATE_ADD(t_born_date , interval t_days day),
            t_status,
            now(),
            now());
        END";
        DB::unprepared("DROP procedure IF EXISTS save_lote_task");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
