<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
        DB::table('role')->insert(
            ['name' => 'Dueño','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('role')->insert(
            ['name' => 'Administrador','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('role')->insert(
            ['name' => 'Cuidador','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );DB::table('role')->insert(
            ['name' => 'Galponero','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );DB::table('role')->insert(
            ['name' => 'Preparador','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );DB::table('role')->insert(
            ['name' => 'Encastador','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
    }
}
