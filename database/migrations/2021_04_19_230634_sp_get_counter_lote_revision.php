<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetCounterLoteRevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_counter_lote_revision`(in t_current_date date)
        BEGIN
        select
                count(*) count_lotes_without_revision
            from
                lotes l
            where
                l.deleted_at is null and (l.status not in (1, 4, 7, 8, 9, 11, 12)
                and ( DATE_ADD(l.date_lote , interval 8 day) <= t_current_date)
                and ( t_current_date < DATE_ADD(l.date_lote , interval 19 day))
                or (l.status = 10
                and ( DATE_ADD(l.date_lote , interval 19 day) <= t_current_date)
                and ( t_current_date < DATE_ADD(l.date_lote , interval 21 day)))
                or ( l.status not in (1, 6, 7, 8, 9, 10, 11, 12)
                and ( DATE_ADD(l.date_lote , interval 21 day) <= t_current_date)
                and ( t_current_date < DATE_ADD(l.date_lote , interval 42 day)))
                or ( l.status not in (1, 5, 7, 8, 9, 10)
                and t_current_date >= DATE_ADD(l.date_lote , interval 42 day)));
        END";
        DB::unprepared("DROP procedure IF EXISTS get_counter_lote_revision");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
