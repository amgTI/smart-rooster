<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetDeadSpecimens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_dead_specimens`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_type int)
        BEGIN
        declare count_posturas int;

        select
            count(*)
        into
            count_posturas
        from
            dead_specimens ds
            left join cintillo_patas cp on cp.id = ds.cintillo_pata
            left join cintillos c2 on c2.id = cp.cintillo_id
            left join chickenlegs c3 on c3.id = cp.chickenleg_id
        where
        ( campo is null
                    or c2.description like concat('%', campo, '%')
                    or c3.description like concat('%', campo, '%')
                    or ds.cintillo_ala like concat('%', campo, '%')
                    or ds.plate like concat('%', campo, '%'))
            and ds.type_specimen = t_type and ds.deleted_at is null;

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select ds.*, ds.type_specimen+0 type_specimen_number, rv.name rival, \", count_posturas , \" cc, c.description cintillo_description,
						 c.color cintillo_color, ck.description chickenleg_description, sc.alias specimen_alias, sc.photo photo, sc.thumb thumb, sc.id specimen_id,
						 r.reason observation, p.code, p.id postura_id, l.code lote_code from dead_specimens ds

                    left join reasons r on r.id = ds.observation
					left join cintillo_patas cp on cp.id = ds.cintillo_pata
					left join cintillos c on c.id = cp.cintillo_id
					left join chickenlegs ck on ck.id = cp.chickenleg_id
					left join specimens sc on sc.plate = ds.plate
					left join rivals rv on rv.id = ds.breeder_id
					left join lote_posturas lp on lp.id = ds.lote_postura_id
					left join posturas p on p.id = lp.postura_id
					left join lotes l on l.id = lp.lote_id
						where (\", if(campo is null, true, concat(\" c.description like '%\", campo, \"%' or ck.description like '%\", campo, \"%' or ds.cintillo_ala like '%\", campo, \"%' or ds.plate like '%\", campo, \"%'\")),
						\" ) and ds.deleted_at is null and ds.type_specimen = \", t_type, \" order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;


        END";
        DB::unprepared("DROP procedure IF EXISTS get_dead_specimens");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
