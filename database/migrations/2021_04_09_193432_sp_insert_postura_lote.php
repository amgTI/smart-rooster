<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpInsertPosturaLote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `insert_postura_lote`(in t_lote_id int, in t_placa_madrilla varchar(255), in t_quantity_eggs int, in t_postura_id int, in t_status int, in t_lote_postura_id int)
        BEGIN
        declare total_removed_eggs int;

        declare total_remaining_eggs int;

        start transaction;

       if(t_lote_postura_id is null) then
        insert
            into
            lote_posturas (lote_id,
            postura_id,
            quantity_eggs,
            created_at,
            updated_at)
        values (t_lote_id,
        t_postura_id,
        t_quantity_eggs,
        now(),
        now());
        set @last_lote_id = @@identity;
          else
              update lote_posturas set postura_id = t_postura_id,quantity_eggs = t_quantity_eggs where id = t_lote_postura_id;

              set @last_lote_id = t_lote_postura_id;
          end if;




          if(t_status = 2) then
              select
                p.removed_eggs + t_quantity_eggs
            into
                total_removed_eggs
            from
                posturas p
            where
                p.id = t_postura_id;

            select
                p.remaining_eggs-t_quantity_eggs
            into
                total_remaining_eggs
            from
                posturas p
            where
                p.id = t_postura_id;

            update
                posturas p
            set
                p.removed_eggs = total_removed_eggs,
                p.remaining_eggs = total_remaining_eggs
            where
                p.id = t_postura_id;
          end if;
          select @last_lote_id postura_lote_id;
        commit;
        END";
        DB::unprepared("DROP procedure IF EXISTS insert_postura_lote");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
