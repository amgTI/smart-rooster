<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetChickVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_chick_videos`(
            in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text
        )
        BEGIN
        declare cc int;
        select count(*) into cc from chick_videos v
        			join chicks s on s.id = v.chick_id
        			where v.deleted_at is null and
                    ( campo is null
                    or s.cintillo_ala like concat('%', campo, '%'));

                set npage = perpage*(npage-1);

                set
                lc_time_names = 'es_ES';

                set
                @query = concat(\"select v.*, s.cintillo_ala, r.name rival, cv.cintillo_ala rival_cintillo, v.type_of_rival+0 type_of_rival_number, u.name first_name, u.last_name last_name,  \", cc, \" cc from chick_videos v join chicks s on s.id = v.chick_id
				left join rivals r on r.id = v.rival_id left join chicks cv on cv.id = v.rival join users u on u.id = v.user_id
				where v.deleted_at is null and \", if(campo is null, true, concat(\"s.cintillo_ala like '%\", campo, \"%'\")), \"
                    order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

                prepare stmt1
                from
                @query;

                execute stmt1;

                deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_chick_videos");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
