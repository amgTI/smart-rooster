<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetGroupedFinishedPosturaDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_grouped_finished_postura_detail`(
            in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_madrilla_plate varchar(255), in t_padrillo_plate varchar(255), in t_status int
        )
        BEGIN
        declare cc int;

            select
				count(distinct p.id) into cc
			from
				posturas p
            where
                  p.placa_madrilla = t_madrilla_plate and p.placa_padrillo = t_padrillo_plate and p.status = t_status;

            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select
							p.*,
							(
							select
								JSON_ARRAYAGG(JSON_OBJECT('lote_id',
								l.code,
								'quantity',
								lp2.quantity_born_eggs, 'quantities', (select JSON_OBJECT('macho_counter', sum(IF(ck2.gender = 1, 1, 0)), 'hembra_counter', sum(IF(ck2.gender = 2, 1, 0)), 'sin_asignar_counter', sum(IF(ck2.gender = 3, 1, 0)) ) from chicks ck2 where ck2.lote_postura_id = lp2.id and ck2.status = 1)))
							from
								lote_posturas lp2
								join lotes l on l.id = lp2.lote_id
							where
								lp2.postura_id = p.id ) json_data,
							(select ifnull(sum(lp2.quantity_born_eggs),0) from lote_posturas lp2 where lp2.postura_id = p.id) total_nacidos,
							(select count(*) from lote_posturas lp2 left join chicks ck on lp2.id = ck.lote_postura_id where lp2.postura_id = p.id and ck.gender = 1 and ck.status = 1) total_sum,
							(select count(*) from lote_posturas lp2 left join chicks ck on lp2.id = ck.lote_postura_id where lp2.postura_id = p.id and ck.gender = 2 and ck.status = 1) total_sum_hembra,
							(select count(*) from lote_posturas lp2 left join chicks ck on lp2.id = ck.lote_postura_id where lp2.postura_id = p.id and ck.gender = 3 and ck.status = 1) total_sum_sin_definir, \", cc ,\" cc
						from
							posturas p
						where
							p.placa_madrilla = '\", t_madrilla_plate, \"'\", \" and p.placa_padrillo = '\", t_padrillo_plate, \"'\" ,\" and p.status = \", t_status ,\" order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_grouped_finished_postura_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
