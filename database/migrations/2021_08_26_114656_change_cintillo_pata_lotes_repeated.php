<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCintilloPataLotesRepeated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $statement_first = 'update cintillo_pata_posturas set cintillo_pata_id = 111 where id = 106;';
        $statement_update_first = 'update cintillo_patas set status = 2 where id = 111;';
        $statement_second = 'update cintillo_pata_posturas set cintillo_pata_id = 112 where id = 107;';
        $statement_update_second = 'update cintillo_patas set status = 2 where id = 112;';
        $statement_third = 'update cintillo_pata_posturas set cintillo_pata_id = 113 where id = 108;';
        $statement_update_third = 'update cintillo_patas set status = 2 where id = 113;';
        // $statement_fourth = 'update cintillo_pata_posturas set cintillo_pata_id = 114 where id = 109;';
        // $statement_update_fourth = 'update cintillo_patas set status = 2 where id = 114;';
        DB::select($statement_first);
        DB::select($statement_update_first);
        DB::select($statement_second);
        DB::select($statement_update_second);
        DB::select($statement_third);
        DB::select($statement_update_third);
        // DB::select($statement_fourth);
        // DB::select($statement_update_fourth);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
