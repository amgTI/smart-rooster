<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveAdminTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_admin_transactions`(in t_transaction_id int, in t_type int, in t_transaction_date varchar(255), in t_memo longtext,
        in t_total decimal(11, 2), in t_status int, in t_created_by int)
        BEGIN
            if (t_transaction_id is null) then
            insert
                into
                admin_transactions (type,
                transaction_date,
                memo,
                total,
                status,
                created_by,
                created_at,
                updated_at)
            values(t_type,
            t_transaction_date,
            t_memo,
            t_total,
            t_status,
            t_created_by,
            now(),
            now());
            select @@identity transaction_id;
        else
            update admin_transactions set transaction_date = t_transaction_date, memo = t_memo, total = t_total, status = t_status where id = t_transaction_id;
            select t_transaction_id transaction_id;
        end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_admin_transactions");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
