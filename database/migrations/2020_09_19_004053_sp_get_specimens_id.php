<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetSpecimensId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_specimens_id`(in s_id int)
        BEGIN
            select s.id,s.origin,s.alias,s.plate,s.additional_plate,s.breeder,format(price,2) price,s.breeder_id,
            s.color,s.category,s.gender,s.status,s.dob,s.observations,s.photo,ifnull(s.thumb,s.photo) thumb,s.currency,s.definition,
            tss.status status_tracking, tss.description,tss.death_date,r.name name_breeder,tss.url_evidence,tss.url_evidence_thumb,
            (select FORMAT(sum(lp.quantity_born_eggs)/sum(lp.quantity_eggs)*100,0)  from lote_posturas lp join posturas p on p.id = lp.postura_id where p.placa_padrillo = s.plate) porcentaje_infertilidad_padrillo,
            (select FORMAT(sum(lp.quantity_born_eggs)/sum(lp.quantity_eggs)*100,0)  from lote_posturas lp join posturas p on p.id = lp.postura_id where p.placa_madrilla = s.plate) porcentaje_infertilidad_madrilla,
            x.res_counter
            from specimens s
                left join tracking_status_specimen tss on tss.idspecimen = s.id and s.status = tss.status
                left join rivals r on r.id = s.breeder_id
                left join (select count(*) res_counter, p.res_infertility from posturas p group by 2) x on x.res_infertility = s.plate
            where s.id = s_id order by tss.created_at desc limit 1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_specimens_id");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
