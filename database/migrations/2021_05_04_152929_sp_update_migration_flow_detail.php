<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpUpdateMigrationFlowDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `update_migration_flow_detail`(in t_migration_detail_id int, in t_flow_migration_id int, in t_cintillo_ala text, in t_gender int)
        BEGIN
                if(t_migration_detail_id is null) then
                insert
                into
                    reproduction_flow_migrations_detail (flow_migration_id,
                    cintillo_ala,
                    gender,
                    created_at,
                    updated_at)
                values (t_flow_migration_id,
                t_cintillo_ala,
                t_gender,
                now(),
                now());
            else
                    update
                        reproduction_flow_migrations_detail
                    set
                        cintillo_ala = t_cintillo_ala,
                        gender = t_gender,
                        updated_at = now()
                    where
                        id = t_migration_detail_id;
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS update_migration_flow_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
