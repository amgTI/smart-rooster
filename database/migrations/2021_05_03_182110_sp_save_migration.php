<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `sp_save_migration`(in t_placa_madrilla text, in t_placa_padrillo text, in t_date_of_birth date, in t_observation text, in t_status int, in t_created_by int)
        BEGIN
                insert
                into
                reproduction_flow_migrations (placa_madrilla,
                placa_padrillo,
                date_of_birth,
                observation,
                status,
                created_by,
                created_at,
                updated_at)
            values (t_placa_madrilla,
            t_placa_padrillo,
            t_date_of_birth,
            t_observation,
            t_status,
            t_created_by,
            now(),
            now());

            select @@identity migration_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS sp_save_migration");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
