<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetGroupedPosturaChicksDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_grouped_postura_chicks_detail`(
            in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_madrilla_plate varchar(255), in t_padrillo_plate varchar(255), in t_status int, in t_gender int
        )
        BEGIN
        declare cc int;

        select
            count(distinct p.id) into cc
        from
            posturas p
            join lote_posturas lp on lp.postura_id = p.id
            join chicks c on c.lote_postura_id = lp.id
        where
              p.placa_madrilla = t_madrilla_plate and p.placa_padrillo = t_padrillo_plate;

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select
                        distinct p.*,
                        (
                        select
                            JSON_ARRAYAGG(JSON_OBJECT('lote_id',
                            lp2.id,
                            'chicks',
                            (select json_arrayagg(json_object('chick_id', ck.id, 'cintillo_ala', ck.cintillo_ala, 'date_of_birth', ck.date_of_birth, 'gender', ck.gender, 'gender_number', ck.gender+0, 'status', ck.status, 'status_number', ck.status+0, 'new_dob', '', 'is_editing_dob', 0, 'assigned_plate', ck.assigned_plate, 'is_assigned_plate', ck.assigned_plate is not null, 'is_correct_plate', 0) )
                                from chicks ck where ck.lote_postura_id = lp2.id and ck.deleted_at is null and \",
                         case
                              when t_status is null and t_gender is not null then concat(\" ck.gender+0 = \", t_gender)
                              when t_gender is null and t_status is not null then concat(\" ck.status+0 = \", t_status) else true
                          end, \"), 'code', l.code ))
                        from
                            lote_posturas lp2
							join lotes l on l.id = lp2.lote_id
                        where
                            lp2.postura_id = p.id) json_data,
                        (select sum(lp2.quantity_born_eggs) from lote_posturas lp2 where lp2.postura_id = p.id) total_sum, \", cc ,\" cc,
						cint.description cintillo_description,
			            cint.color cintillo_color,
			            cll.description chickenleg_description,
						cpp.status+0 cintillo_pata_postura_number
                    from
                        posturas p
                        join lote_posturas lp on lp.postura_id = p.id
                        join chicks c on c.lote_postura_id = lp.id
						left join cintillo_pata_posturas cpp on
				            cpp.postura_id = p.id
				        left join cintillo_patas cp on
				            cp.id = cpp.cintillo_pata_id
				        left join cintillos cint on
				            cint.id = cp.cintillo_id
				        left join chickenlegs cll on
				            cll.id = cp.chickenleg_id
                    where
                        p.placa_madrilla = '\", t_madrilla_plate, \"'\", \" and p.placa_padrillo = '\", t_padrillo_plate, \"' order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_grouped_postura_chicks_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
