<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetActiveMadrillasByActivePosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_active_madrillas_by_active_posturas`()
        BEGIN
        select
        s.id,
        s.alias,
        s.plate,
        p.number_eggs_good
    from
        specimens s
    left join (
        select
                tbl2.placa_madrilla,
                tbl2.created_at,
                max(tbl2.number_eggs_good) number_eggs_good,
                tbl2.status
            from
                (
                select
                    placa_madrilla,
                    max(created_at) as `created_at`
                from
                    posturas
                group by
                    placa_madrilla ) tbl1
            inner join posturas tbl2 on
                tbl2.placa_madrilla = tbl1.placa_madrilla
                and tbl2.created_at = tbl1.created_at
            group by
                tbl2.placa_madrilla,
                tbl2.created_at,
                tbl2.status
    ) p on p.placa_madrilla = s.plate
    where
        s.category = 2
        and s.status in (1,5,4)
           and (p.status not in(2,3) or p.status is null) order by 3 asc;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_active_madrillas_by_active_posturas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
