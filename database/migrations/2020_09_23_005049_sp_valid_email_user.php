<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpValidEmailUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  PROCEDURE `valid_email_user`(in t_email varchar(255), in iduser int)
            BEGIN
				declare u_email varchar(255) default null;
				declare u_id int default null;
                if(iduser is null) then 
					select email into u_email from users where email = t_email;
                     select u_email,iduser;
				else
					select email into u_email from users where email = t_email and id = iduser;	
					select id into u_id from users where email = t_email;
					select u_email, iduser,u_id;
                end if;
                
            END";
        DB::unprepared("DROP procedure IF EXISTS valid_email_user");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
