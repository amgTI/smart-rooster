<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertInfertileEggs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `insert_infertile_eggs`(in t_lote_postura_id int, in t_infertile_eggs int)
        BEGIN
            update lote_posturas set quantity_infertile_eggs = t_infertile_eggs where id = t_lote_postura_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS insert_infertile_eggs");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
