<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveAdminTransactionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_admin_transaction_details`(in t_transaction_id int, in t_motive int, in t_description text, in t_amount int, in t_transaction_detail_id int)
        BEGIN
                if(t_transaction_detail_id is null) then
                insert into admin_transactions_detail (transaction_id, motive, amount, description, created_at, updated_at) values (t_transaction_id, t_motive, t_amount, t_description, now(), now());
            else
                update admin_transactions_detail set motive = t_motive, description = t_description, amount = t_amount where id = t_transaction_detail_id;
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_admin_transaction_details");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
