<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetPosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_posturas`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_status int, in t_placa_specimen varchar(255), in t_category_specimen int)
        BEGIN
        declare count_posturas int;
        declare total_sum_eggs int;
        declare total_sum_eggs_lote int;
        declare total_sum_eggs_infertiles int;
        declare total_sum_eggs_nacidos int;
        declare total_sum_eggs_muertos int;
        select
        count(*),
            ifnull(sum(post.number_eggs_good), 0),
            ifnull(sum(z.suma_total), 0),
            ifnull(sum(z.suma_infertiles), 0),
            ifnull(sum(z.suma_nacidos), 0),
            ifnull(sum(z.suma_muertos), 0)
        into
            count_posturas, total_sum_eggs, total_sum_eggs_lote, total_sum_eggs_infertiles, total_sum_eggs_nacidos, total_sum_eggs_muertos
        from
            posturas post
        join specimens madrilla on
            madrilla.plate = post.placa_madrilla
        join specimens padrillo on
            padrillo.plate = post.placa_padrillo

        left join (
        select
            lp.postura_id,
            ifnull(sum(lp.quantity_eggs), 0) suma_total,
            ifnull(sum(lp.quantity_infertile_eggs), 0) suma_infertiles,
            ifnull(sum(lp.quantity_born_eggs), 0) suma_nacidos,
            ifnull(sum(lp.quantity_dead_eggs), 0) suma_muertos
        from
            lote_posturas lp
        group by
            1) z on
        z.postura_id = post.id
        where
            ( campo is null
            or madrilla.alias like concat('%', campo, '%')
            or padrillo.alias like concat('%', campo, '%')
            or post.placa_madrilla like concat('%', campo, '%')
            or post.placa_padrillo like concat('%', campo, '%')
            or post.code like concat('%', campo, '%') )
            and post.status = t_status
            and if(t_placa_specimen is null,
            true,
            if(t_category_specimen = 1,
            post.placa_padrillo = t_placa_specimen,
            post.placa_madrilla = t_placa_specimen));

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select post.method method,
                    madrilla.photo, madrilla.thumb, post.id, post.postura_date, post.placa_madrilla, post.placa_padrillo, madrilla.alias madrilla_alias,
                    post.deleted_at deleted_at, ud.name deleted_by_name, ud.last_name deleted_by_last_name, uf.name finished_by_name, uf.last_name finished_by_last_name, post.finished_at,
                    padrillo.alias padrillo_alias, u.name, u.last_name, post.status as status, post.number_eggs_good, post.created_at,
                    post.updated_at, padrillo.photo padrillo_photo, padrillo.thumb padrillo_thumb, padrillo.id padrillo_id, \", count_posturas, \" cc,
                    madrilla.id madrilla_id,\", total_sum_eggs ,\" total_sum, \", total_sum_eggs_lote , \" total_sum_eggs_lote, \", total_sum_eggs_infertiles ,
                    \" total_sum_eggs_infertiles, \", total_sum_eggs_nacidos , \" total_sum_eggs_nacidos, \", total_sum_eggs_muertos ,
                    \" total_sum_eggs_muertos, padrillo.is_being_used padrillo_status,
                    madrilla.is_being_used madrilla_status,
                    post.madrilla_status madrilla_status_post, post.padrillo_status padrillo_status_post,
                    x.posturas_quantity posturas_quantity_madrilla,
                    y.posturas_quantity posturas_quantity_padrillo,
                    post.code,
                    z.suma_total, z.suma_infertiles, z.suma_nacidos, z.suma_muertos, (z.suma_total-z.suma_infertiles) resta, post.res_infertility, cpp.status+0 cintillo_postura_number,
                    cpp.status cintillo_postura_status, c2.color cintillo_color, c2.description cintillo_description, ck2.description chickenleg_description, cpp.status+0 cintillo_pata_postura_number

                    from posturas post
                    join specimens madrilla on
                    madrilla.plate = post.placa_madrilla
                    join specimens padrillo on
                    padrillo.plate = post.placa_padrillo
                    join users u on
                    u.id = post.created_by
                    left join users ud on
                    ud.id = post.deleted_by
                    left join users uf on
                    uf.id = post.finished_by
                    left join (
                    select
                        count(*) posturas_quantity,
                        p2.placa_madrilla
                    from
                        posturas p2
                    join specimens s2 on
                        s2.plate = p2.placa_madrilla
                    where
                        year(p2.postura_date) = year(CURDATE())
                    group by
                        p2.placa_madrilla) x on
                    x.placa_madrilla = post.placa_madrilla
                    left join (
                    select
                        count(*) posturas_quantity,
                        p2.placa_padrillo
                    from
                        posturas p2
                    join specimens s2 on
                        s2.plate = p2.placa_padrillo
                    where
                        year(p2.postura_date) = year(CURDATE())
                    group by
                        p2.placa_padrillo) y on
                    y.placa_padrillo = post.placa_padrillo
                    left join (
                    select
                        lp.postura_id,
                        ifnull(sum(lp.quantity_eggs), 0) suma_total,
                        ifnull(sum(lp.quantity_infertile_eggs), 0) suma_infertiles,
                        ifnull(sum(lp.quantity_born_eggs), 0) suma_nacidos,
                        ifnull(sum(lp.quantity_dead_eggs), 0) suma_muertos
                    from
                        lote_posturas lp
                    group by
                        1) z on
                    z.postura_id = post.id
                    left join cintillo_pata_posturas cpp on cpp.postura_id = post.id
                    left join cintillo_patas cp on cp.id = cpp.cintillo_pata_id
                    left join cintillos c2 on c2.id = cp.cintillo_id
                    left join chickenlegs ck2 on ck2.id = cp.chickenleg_id
                    where  post.status = \", t_status, \" and( \", if(t_placa_specimen is null, true, if(t_category_specimen = 1, concat(\"post.placa_padrillo = '\", t_placa_specimen, \"'\"), concat(\"post.placa_madrilla = '\", t_placa_specimen, \"'\"))) , \")  and (\", if(campo is null, true, concat(\" madrilla.alias like '%\", campo, \"%' or padrillo.alias like '%\", campo, \"%' or post.placa_madrilla like '%\", campo, \"%' or post.code like '%\", campo, \"%'\")), \"
                    ) order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;




        END";
        DB::unprepared("DROP procedure IF EXISTS get_posturas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
