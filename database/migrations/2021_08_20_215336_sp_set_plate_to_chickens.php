<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSetPlateToChickens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `set_plate_to_chickens`(in t_chicken_id int, in t_plate varchar(255), in t_user_id int, in t_dob varchar(255), in t_gender int)
        BEGIN
        update chicks set assigned_plate = t_plate where id = t_chicken_id;
        insert
            into
            specimens(created_by,
            origin,
            alias,
            definition,
            plate,
            additional_plate,
            breeder,
            breeder_id,
            currency,
            price,
            color,
            category,
            gender,
            status,
            dob,
            observations,
            created_at,
            updated_at)
        values(t_user_id,
        1,
        null,
        null,
        t_plate,
        null,
        null,
        null,
        null,
        null,
        null,
        (CASE
            WHEN (ADDDATE(date(t_dob), INTERVAL 12 MONTH) <= CURDATE() and t_gender = 1) THEN 3
            WHEN (ADDDATE(date(t_dob), INTERVAL 12 MONTH) <= CURDATE() and t_gender = 2) THEN 4
            WHEN (ADDDATE(date(t_dob), INTERVAL 12 MONTH) > CURDATE() and t_gender = 1) THEN 5
            WHEN (ADDDATE(date(t_dob), INTERVAL 12 MONTH) > CURDATE() and t_gender = 2) THEN 6
        end),
        t_gender,
        1,
        t_dob,
        'Se le puso la placa al ejemplar desde la postura',
        now(),
        now());
        END";
        DB::unprepared("DROP procedure IF EXISTS set_plate_to_chickens");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
