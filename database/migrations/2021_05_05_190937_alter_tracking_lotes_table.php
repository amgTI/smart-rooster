<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTrackingLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE tracking_lote CHANGE COLUMN status status ENUM('CREACION LOTE', 'REVISION INFERTILES', 'REVISION VIVOS', 'NACIMIENTO POLLOS', 'NACEDORA', 'CINTILLO ALA') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
