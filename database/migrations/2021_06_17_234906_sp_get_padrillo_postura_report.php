<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetPadrilloPosturaReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_padrillo_postura_report`(
            in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text
        )
        BEGIN
        declare cc int;

	select
       count(distinct s.id) into cc
	from
		specimens s
	join posturas p on
		p.placa_padrillo = s.plate
        where
            ( campo is null
            or s.alias like concat('%', campo, '%')
            or s.plate like concat('%', campo, '%'));

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select s.*, (select
		json_arrayagg(json_object('anuladas',
		sum(if(p.status = 1, 1, 0)),
		'pendientes',
		sum(if(p.status = 2, 1, 0)),
		'procesos',
		sum(if(p.status = 3 and p.madrilla_status = 1, 1, 0)),
		'finalizadas',
		sum(if(p.status = 4, 1, 0)),
		'stand_by',
		sum(if(p.status = 3 and p.madrilla_status = 0, 1, 0))))) counters, \", cc ,\" cc
                   from specimens s
		join posturas p on p.placa_padrillo = s.plate where
		s.category = 1 and \", if(campo is null, true, concat(\"  s.alias like '%\", campo, \"%' or s.plate like '%\", campo, \"%'\")), \"
		 group by s.id order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_padrillo_postura_report");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
