<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDeleteVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `delete_video`(in idvideo int, in iduser int)
        BEGIN
            update video set deleted_at = now(), deleted_by = iduser where id = idvideo;
            
            select update_countmovies(idvideo);
        END";
        DB::unprepared("DROP procedure IF EXISTS delete_video");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
