<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpUpdateVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `update_video`(in idvideo int,
								in idspe int,
								in type_video int, 
								in namevideo varchar(255),
                                in t_amarrador int,
                                in t_careador int,
								in t_category int,
								in t_rival varchar(255),
								in t_event_date date,
                                in t_event_result int)
            BEGIN
                        declare idspeci int;
                        declare plaquelocal varchar(255);
                        declare idv int default null;
                        
                    START TRANSACTION;
						if(type_video = 2)then
							 update video set type_of_video = type_video,event_date = t_event_date,event_result = t_event_result,rival_id = t_rival,amarrador = t_amarrador,careador = t_careador ,description = namevideo where id = idvideo;
                             
                             select update_countmovies(idvideo);

						end if;
                        if(t_category = 2)then
                            update video set type_of_video = type_video,type_of_rival = t_category,event_date = t_event_date,rival_id = t_rival,description = namevideo where id = idvideo;
                            
                            select update_countmovies(idvideo);
                            
                        end if;
                        if(t_category = 1)then
                        
                            select id into idspeci 
                            from specimens 
                            where plate = t_rival;

                            update video set type_of_video = type_video,type_of_rival = t_category,event_date = t_event_date,rival = idspeci,description = namevideo where id = idvideo;
                            
                            select update_countmovies(idvideo);
                      
                        end if;
                    commit;
            END";
        DB::unprepared("DROP procedure IF EXISTS update_video");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
