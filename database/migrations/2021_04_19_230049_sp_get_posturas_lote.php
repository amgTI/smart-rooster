<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetPosturasLote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_posturas_lote`(in t_lote_id int)
        BEGIN
        select
            lp.*,
            p.placa_madrilla placa_madrilla,
            s.alias madrilla_alias,
            p.placa_padrillo placa_padrillo,
            sp.alias padrillo_alias,
            u.name created_by_name,
            u.last_name created_by_last_name,

            c2.description cintillo_description,
                c2.color cintillo_color,
                cl.description chickenleg_description,
                l.code


        from
            lote_posturas lp
        join posturas p on
            p.id = lp.postura_id
        join lotes l on
        	l.id = lp.lote_id
        join specimens s on
            s.plate = p.placa_madrilla
        join specimens sp on
            sp.plate = p.placa_padrillo
        join users u on u.id = p.created_by
		left join cintillo_pata_posturas cpp on
            	cpp.postura_id = p.id
            left join cintillo_patas cp2 on
             	cp2.id = cpp.cintillo_pata_id
            left join cintillos c2 on
            	c2.id = cp2.cintillo_id
            left join chickenlegs cl on
            	cl.id = cp2.chickenleg_id
        where
            lp.lote_id = t_lote_id and lp.deleted_at is null;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_posturas_lote");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
