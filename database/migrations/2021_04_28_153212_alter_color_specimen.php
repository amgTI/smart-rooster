<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColorSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('color_specimen', 'gender')){
            Schema::table('color_specimen', function (Blueprint $table) {
                $table->integer('gender')->after('color');;
            });
            DB::table('color_specimen')->where('id', 1)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 2)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 3)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 4)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 5)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 6)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 7)->update(
                ['gender' => 1]
            );
            DB::table('color_specimen')->where('id', 8)->update(
                ['gender' => 1]
            );




            DB::table('color_specimen')->insert(
                ['color' => 'NEGRA', 'gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'AMARILLA', 'gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'CENIZA','gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'MORA','gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'AJI SECA','gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'BLANCA','gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'GIRA','gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
            DB::table('color_specimen')->insert(
                ['color' => 'GALLINA','gender' => 2,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
