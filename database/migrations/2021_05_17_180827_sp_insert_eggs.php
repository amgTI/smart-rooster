<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpInsertEggs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `insert_eggs`(in t_description text, in t_egg_postura_date date, in t_created_by int, in t_total_eggs_quantity int)
        BEGIN
                insert
                into
                eggs (description,
                total_eggs_quantity,
                egg_postura_date,
                can_delete_until,
                created_by,
                created_at,
                updated_at)
            values (t_description,
            t_total_eggs_quantity,
            t_egg_postura_date,
            DATE_ADD(now(), interval 24 hour),
            t_created_by,
            now(),
            now());
            select @@identity last_egg_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS insert_eggs");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
