<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetChicksBySpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_chicks_by_specimen`(
            in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_from varchar(255), in t_to varchar(255)
        )
            BEGIN
            declare cc int;

            select
                count(distinct v.placa_padrillo, v.photo, v.thumb, v.madrilla_id, v.padrillo_photo, v.padrillo_thumb, v.padrillo_alias, v.padrillo_id)
            into
                cc
            from
                (select


                        p.id postura_id,
                        ep.plate placa_padrillo,
                        em.alias madrilla_alias,
                        em.photo photo, em.thumb thumb,
                        em.id madrilla_id,
                        ep.photo padrillo_photo, ep.thumb padrillo_thumb,
                        ep.alias padrillo_alias,
                        ep.id padrillo_id,
                        em.plate placa_madrilla,
                        (select
                    count(distinct p.id)
                from
                    posturas p
                    join lote_posturas lp on lp.postura_id = p.id
                    join chicks c on c.lote_postura_id = lp.id
                where
                      p.placa_madrilla = em.plate and p.placa_padrillo = ep.plate) suma
                from
                    chicks ck
                join users u on
                        u.id = ck.created_by
                        join lote_posturas lp on
                        lp.id = ck.lote_postura_id
                        join posturas p on
                        p.id = lp.postura_id
                        join specimens em on
                        em.plate = p.placa_madrilla
                        join specimens ep on
                        ep.plate = p.placa_padrillo where ck.deleted_at is null
                group by 1,2,3,4,5,6,7,8,9,10) v

                join(select
                        x.alita,
                        p.placa_madrilla,
                        p.placa_padrillo,
                        p.postura_date,
                        x.suma_vivos,
                        x.suma_hembra,
                        x.suma_macho,
                        x.suma_sin_asignar,
                        x.suma_muertos,
                        x.suma_total
                    from
                        posturas p
                    join lote_posturas lp on
                        p.id = lp.postura_id
                    join (select
        c.lote_postura_id,
        JSON_ARRAYAGG(JSON_OBJECT('cintillo_ala',
        c.cintillo_ala)) alita,
        sum(if(c.status = 1, 1, 0)) suma_vivos,
        sum(if(c.gender = 2, 1, 0)) suma_hembra,
        sum(if(c.gender = 1, 1, 0)) suma_macho,
        sum(if(c.gender = 3, 1, 0)) suma_sin_asignar,
        sum(if(c.status = 2, 1, 0)) suma_muertos,
        count(*) suma_total
    from
        chicks c where c.deleted_at is null
    group by
        1) x on x.lote_postura_id = lp.id) y on y.placa_madrilla = v.placa_madrilla and y.placa_padrillo = v.placa_padrillo
            where
                (campo is null
                or v.madrilla_alias like concat('%', campo, '%')
                or v.padrillo_alias like concat('%', campo, '%')
                or v.placa_madrilla like concat('%', campo, '%')
                or v.placa_padrillo like concat('%', campo, '%')) and if(t_from is null or t_to is null, true, cast(y.postura_date as date) between cast(t_from as date) and cast(t_to as date));

            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select

                        v.placa_padrillo,
                        v.madrilla_alias,
                        v.photo, v.thumb, v.madrilla_id, v.padrillo_photo, v.padrillo_thumb, v.padrillo_alias, v.padrillo_id, v.placa_madrilla, \", cc ,\" cc, v.suma,
                        FORMAT(sum(y.suma_vivos)/v.suma,0) suma_vivos,
                        FORMAT(sum(y.suma_hembra)/v.suma,0) suma_hembra,
                        FORMAT(sum(y.suma_macho)/v.suma,0)  suma_macho,
                        FORMAT(sum(y.suma_sin_asignar)/v.suma,0)  suma_sin_asignar,
                        FORMAT(sum(y.suma_muertos)/v.suma,0)  suma_muertos,
                        FORMAT(sum(y.suma_total)/v.suma,0)  suma_total


                        from (select


                        p.id postura_id,
                        ep.plate placa_padrillo,
                        em.alias madrilla_alias,
                        em.photo photo, em.thumb thumb,
                        em.id madrilla_id,
                        ep.photo padrillo_photo, ep.thumb padrillo_thumb,
                        ep.alias padrillo_alias,
                        ep.id padrillo_id,
                        em.plate placa_madrilla,
                        (select
                    count(distinct p.id)
                from
                    posturas p
                    join lote_posturas lp on lp.postura_id = p.id
                    join chicks c on c.lote_postura_id = lp.id
                where
                      p.placa_madrilla = em.plate and p.placa_padrillo = ep.plate) suma
                from
                    chicks ck
                join users u on
                        u.id = ck.created_by
                        join lote_posturas lp on
                        lp.id = ck.lote_postura_id
                        join posturas p on
                        p.id = lp.postura_id
                        join specimens em on
                        em.plate = p.placa_madrilla
                        join specimens ep on
                        ep.plate = p.placa_padrillo where ck.deleted_at is null
                group by 1,2,3,4,5,6,7,8,9,10) v

                join(select
                        x.alita,
                        p.placa_madrilla,
                        p.placa_padrillo,
                        p.postura_date,
                        x.suma_vivos,
                        x.suma_hembra,
                        x.suma_macho,
                        x.suma_sin_asignar,
                        x.suma_muertos,
                        x.suma_total
                    from
                        posturas p
                    join lote_posturas lp on
                        p.id = lp.postura_id
                    join (select
        c.lote_postura_id,
        JSON_ARRAYAGG(JSON_OBJECT('cintillo_ala',
        c.cintillo_ala)) alita,
        sum(if(c.status = 1, 1, 0)) suma_vivos,
        sum(if(c.gender = 2, 1, 0)) suma_hembra,
        sum(if(c.gender = 1, 1, 0)) suma_macho,
        sum(if(c.gender = 3, 1, 0)) suma_sin_asignar,
        sum(if(c.status = 2, 1, 0)) suma_muertos,
        count(*) suma_total
    from
        chicks c where c.deleted_at is null
    group by
        1) x on x.lote_postura_id = lp.id) y on y.placa_madrilla = v.placa_madrilla and y.placa_padrillo = v.placa_padrillo

            where (\",
            if(campo is null, true, concat(\" v.madrilla_alias like '%\", campo, \"%'
                                     or v.padrillo_alias like '%\", campo, \"%'
                                    or v.placa_padrillo like '%\", campo, \"%'
                                    or v.placa_madrilla like '%\", campo, \"%'\")), \")\", if(t_from is null or t_to is null, ' ', concat(\" and y.postura_date between '\", t_from,\"' and '\", t_to, \"'\")), \"
             group by 1,3,4,5,6,7,8,9   order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_chicks_by_specimen");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
