<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetCounterLoteRevisionNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_counter_lote_revision_new`(
        )
            BEGIN
            select
            DATE_ADD(l.date_lote, interval 8 day) infertile_revision_date,
            DATE_ADD(l.date_lote, interval 19 day) hatcher_revision_date,
            DATE_ADD(l.date_lote, interval 20 day) alive_revision_date,
            DATE_ADD(l.date_lote, interval 42 day) cintillo_ala_revision_date,
            l.status+0 status_number
        from lotes l
        where
            l.deleted_at is null;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_counter_lote_revision_new");
        DB::unprepared($procedure);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
