<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotePosturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lote_posturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('lote_id');
            $table->foreign('lote_id')->references('id')->on('lotes');
            $table->unsignedBigInteger('postura_id');
            $table->foreign('postura_id')->references('id')->on('posturas');
            $table->integer('quantity_eggs');
            $table->integer('quantity_infertile_eggs')->nullable();
            $table->integer('quantity_born_eggs')->nullable();
            $table->integer('quantity_dead_eggs')->nullable();
            $table->unsignedBigInteger('cintillo_id')->nullable();
            $table->unsignedBigInteger('chickenleg_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lote_posturas');
    }
}
