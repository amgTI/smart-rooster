<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LiberateCintilloFromFinishedPosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $statement = 'update cintillo_pata_posturas set status = 1 where id > 69;';
        $statement_two = 'update cintillo_patas set status = 2 where id in (select cintillo_pata_id from cintillo_pata_posturas where id > 69);';
        DB::select($statement);
        DB::select($statement_two);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { }
}
