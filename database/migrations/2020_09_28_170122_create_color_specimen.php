<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateColorSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_specimen', function (Blueprint $table) {
            $table->id();
            $table->string('color');
            $table->timestamps();
        });
        DB::table('color_specimen')->insert(
            ['color' => 'NEGRO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'AMARILLO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'CENIZO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'MORO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'AJI SECO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'BLANCO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'GIRO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('color_specimen')->insert(
            ['color' => 'GALLINO','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('color_specimen');
    }
}
