<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDecreasaePosturaQuantities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `decreasae_postura_quantities`(in t_postura_id int, in t_quantity int, in t_deleted_by int, in t_eggs_id int)
        BEGIN
            declare t_total_good_eggs int;
            declare remaining int;
            select (number_eggs_good - t_quantity) into t_total_good_eggs from posturas where id = t_postura_id;
            select (t_total_good_eggs - removed_eggs) into remaining from posturas where id = t_postura_id;

            update egg_postura set deleted_by = t_deleted_by, deleted_at = now() where eggs_id = t_eggs_id;
            update posturas set remaining_eggs = remaining, number_eggs_good = t_total_good_eggs where id = t_postura_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS decreasae_postura_quantities");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
