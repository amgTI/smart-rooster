<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetRegisteredEggsMadrillaDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_registered_eggs_madrilla_detail`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_madrilla_plate varchar(255))
        BEGIN
        declare count_registered_eggs_madrilla int;

        select
            count(distinct p.id) into count_registered_eggs_madrilla
        from
            posturas p
        join specimens s on
            s.plate = p.placa_madrilla
        join specimens sp on
            sp.plate = p.placa_padrillo
        join egg_postura ep2 on ep2.postura_id = p.id
        where
            ( campo is null
            or s.alias like concat('%', campo, '%')
            or s.plate like concat('%', campo, '%'))
             and s.plate = t_madrilla_plate;

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select
                        p.*,
                        s.alias madrilla_alias,
                        sp.alias padrillo_alias,
                        \", count_registered_eggs_madrilla ,\" count_registered_eggs_madrilla,
                        (select sum(ep3.quantity) from posturas p2 join specimens s on s.plate = p2.placa_madrilla join egg_postura ep3 on ep3.postura_id = p2.id where ep3.postura_id = p.id and ep3.deleted_at is null) total_quantity,
                        (select
                        json_arrayagg(JSON_OBJECT('date',
                        e.egg_postura_date,
                        'quantity',
                        ep.quantity,
                        'ep_created_at',
                        ep.created_at, 'ep_deleted_at', ep.deleted_at)
                    ) from
                        egg_postura ep
                        join eggs e on e.id = ep.eggs_id
                    where
                        ep.postura_id = p.id) json_data
                    from
                        posturas p
                     join specimens s on
                        s.plate = p.placa_madrilla
                     join specimens sp on
                        sp.plate = p.placa_padrillo
                      join egg_postura ep2 on ep2.postura_id = p.id
                    where
                        s.plate = '\", t_madrilla_plate, \"'\", \"  and \", if(campo is null, true, concat(\" s.alias like '%\", campo, \"%' or s.plate like '%\", campo, \"%'\")), \"
           group by ep2.postura_id, s.alias, sp.alias order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_registered_eggs_madrilla_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
