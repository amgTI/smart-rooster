<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetLote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_lote`(in id_lote int)
        BEGIN
        select
            l.date_lote,
            p.id postura_id,
            lp.quantity_eggs total_eggs,
            lp.quantity_infertile_eggs number_infertile_eggs,
            lp.quantity_born_eggs number_alive_eggs,
            lp.quantity_dead_eggs number_dead_eggs,
            lp.quantity_eggs-lp.quantity_infertile_eggs total_minus_infertile_eggs,
            lp.id lote_postura_id,
            l.placa_madrilla madrilla_incubadora,
            p.placa_madrilla placa_madrilla,
            s.alias madrilla_alias,
            sp.alias padrillo_alias,
            p.placa_padrillo placa_padrillo,
            p.remaining_eggs remaining_eggs,
            p.number_eggs_good number_eggs,
            p.removed_eggs removed_eggs,
            l.status+0 lote_status,
            lp.chickenleg_id chickenleg_id,
            lp.cintillo_id cintillo_id,
            c2.description cintillo_description,
            c2.color cintillo_color,
            cl.description chickenleg_description,
            cpp.status+0 cintillo_pata_postura_status,
            l.id lote_id,
            (select
                (JSON_ARRAYAGG(JSON_OBJECT('cintillo_ala',
                chk.cintillo_ala,
                'gender',
                chk.gender + 0)))
                from chicks chk
                where chk.lote_postura_id = lp.id) cintillos_alas,
                (
                select
                    JSON_ARRAYAGG(JSON_OBJECT('name',
                    dcl.name,
                    'url',
                    dcl.url,
                    'quantity',
                    dcl.quantity))
                from
                    dead_chick_lotes dcl
                where
                    dcl.lote_postura_id = lp.id) evidence

        from
            lotes l
        join lote_posturas lp on
            lp.lote_id = l.id
        join posturas p on
            p.id = lp.postura_id
        left join cintillo_pata_posturas cpp on
            cpp.postura_id = p.id
        left join cintillo_patas cp2 on
            cp2.id = cpp.cintillo_pata_id
        left join cintillos c2 on
            c2.id = cp2.cintillo_id
        left join chickenlegs cl on
            cl.id = cp2.chickenleg_id
        join specimens s on
            s.plate = p.placa_madrilla
        join specimens sp on
            sp.plate = p.placa_padrillo
        where
            l.id = id_lote and lp.deleted_at is null;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_lote");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
