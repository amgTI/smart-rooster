<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePosturasTableAddResInfertility extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posturas', function (Blueprint $table) {
            $table->string('res_infertility')->nullable()->after('turn_off_reason');
            $table->text('infertility_obs')->nullable()->after('res_infertility');
            $table->datetime('res_infertility_at')->nullable()->after('infertility_obs');
            $table->integer('res_infertility_by')->nullable()->after('res_infertility_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
