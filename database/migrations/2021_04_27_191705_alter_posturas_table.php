<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPosturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('posturas', function(Blueprint $table)
        {
            $table->dropColumn('method');
        });

        Schema::table('posturas', function(Blueprint $table)
        {
            $table->enum('method', ['NATURAL', 'ARTIFICIAL'])->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
