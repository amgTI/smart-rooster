<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSearchPosturasByMadrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `search_posturas_by_madrilla`(in madrilla_plate varchar(255))
        BEGIN
            select
            postura.*, madrilla.alias madrilla_alias, padrillo.alias padrillo_alias
            from
                posturas postura
            join
                specimens madrilla
                on madrilla.plate = postura.placa_madrilla
            join specimens padrillo
                on padrillo.plate = postura.placa_padrillo
            where
                postura.placa_madrilla = madrilla_plate
                and year(now()) = year(postura.created_at)
                and postura.status = 'active';
        END";
        DB::unprepared("DROP procedure IF EXISTS search_posturas_by_madrilla");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
