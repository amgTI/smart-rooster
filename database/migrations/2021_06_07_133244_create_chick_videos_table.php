<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChickVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('chick_videos');
        Schema::create('chick_videos', function (Blueprint $table) {
            $table->id();
            $table->integer('chick_id');
            $table->integer('user_id');
            $table->enum('type_of_video',['tope','pelea']);
            $table->enum('type_of_rival',['interno','externo'])->nullable();
            $table->date('event_date')->nullable();
            $table->integer('event_result')->nullable();
            $table->integer('rival')->nullable();
            $table->integer('rival_id')->nullable();
            $table->integer('amarrador')->nullable();
            $table->integer('careador')->nullable();
            $table->string('description');
            $table->text('url');
            $table->string('thumb')->nullable();
            $table->string('duration')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chick_videos');
    }
}
