<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetGroupedPosturasDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_grouped_postura_detail`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_madrilla_plate varchar(255), in t_padrillo_plate varchar(255), in t_status int)
        BEGIN
        declare cc int;

            select
				count(distinct p.id) into cc
			from
				posturas p
            where
                  p.placa_madrilla = t_madrilla_plate and p.placa_padrillo = t_padrillo_plate and p.status = t_status;

            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select
							p.*,
							(
							select
								JSON_ARRAYAGG(JSON_OBJECT('lote_id',
								lp2.id,
								'quantity',
								lp2.quantity_born_eggs))
							from
								lote_posturas lp2
							where
								lp2.postura_id = p.id) json_data,
							(select sum(lp2.quantity_born_eggs) from lote_posturas lp2 where lp2.postura_id = p.id) total_sum, \", cc ,\" cc
						from
							posturas p
						where
							p.placa_madrilla = '\", t_madrilla_plate, \"'\", \" and p.placa_padrillo = '\", t_padrillo_plate, \"'\" ,\" and p.status = \", t_status ,\" order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_grouped_postura_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
