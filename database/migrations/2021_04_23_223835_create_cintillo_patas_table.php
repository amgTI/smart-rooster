<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCintilloPatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cintillo_patas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cintillo_id');
            $table->foreign('cintillo_id')->references('id')->on('cintillos');
            $table->unsignedBigInteger('chickenleg_id');
            $table->foreign('chickenleg_id')->references('id')->on('chickenlegs');
            $table->unsignedBigInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users');
            $table->datetime('deleted_at')->nullable();
            $table->enum('status', ['SIN UTILIZAR', 'UTILIZADA'])->default('SIN UTILIZAR');
            $table->unique(['cintillo_id', 'chickenleg_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cintillo_patas');
    }
}
