<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Alter2Video extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video',function(Blueprint $table){
            $table->enum('type_of_rival',['interno','externo'])->nullable()->after('type_of_video');
        });
        Schema::table('video',function(Blueprint $table){
            $table->date('event_date')->nullable()->after('type_of_rival');
        });
        Schema::table('video',function(Blueprint $table){
            $table->string('rival')->nullable()->after('event_date');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
