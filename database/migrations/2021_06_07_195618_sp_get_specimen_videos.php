<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetSpecimenVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_specimen_videos`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare cc int;
        select count(*) into cc from video v
        			join specimens s on s.id = v.idspecimen
        			where v.deleted_at is null and
                    ( campo is null
                    or s.alias like concat('%', campo, '%')
                   	or s.plate like concat('%', campo, '%'));

                set npage = perpage*(npage-1);

                set
                lc_time_names = 'es_ES';

                set
                @query = concat(\"select v.*, s.alias, s.plate, s.photo, s.thumb specimen_thumb, r.name rival, sp.alias rival_alias, sp.plate rival_plate, v.type_of_rival+0 type_of_rival_number,  s.id specimen_id, u.name first_name, u.last_name last_name,  \", cc, \" cc from video v join specimens s on s.id = v.idspecimen
				left join rivals r on r.id = v.rival_id left join specimens sp on sp.id = v.rival join users u on u.id = v.user_id
				where v.deleted_at is null and \", if(campo is null, true, concat(\" s.alias like '%\", campo, \"%' or s.plate like '%\", campo, \"%'\")), \"
                    order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

                prepare stmt1
                from
                @query;

                execute stmt1;

                deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_specimen_videos");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
