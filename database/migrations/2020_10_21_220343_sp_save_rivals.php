<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveRivals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_rivals`(in t_namerival varchar(255))
        BEGIN
            declare rivalexists varchar(255) default null;
            select name into rivalexists from rivals where name = t_namerival;
            if(rivalexists is null)then
                insert into rivals(name,created_at,updated_at) values(t_namerival,now(),now());
                select rivalexists;
            else
                select rivalexists;
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_rivals");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
