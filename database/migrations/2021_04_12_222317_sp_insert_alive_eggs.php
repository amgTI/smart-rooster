<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpInsertAliveEggs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `insert_alive_eggs`(in t_lote_postura_id int, in t_alive_eggs int, in t_dead_eggs int, in t_cintillo_id int, in t_chickenleg_id int)
        BEGIN
            update
            lote_posturas
        set
            quantity_born_eggs = t_alive_eggs,
            quantity_dead_eggs = t_dead_eggs,
            cintillo_id = t_cintillo_id,
            chickenleg_id = t_chickenleg_id,
            cintillo_ala_status = 1
        where
            id = t_lote_postura_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS insert_alive_eggs");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
