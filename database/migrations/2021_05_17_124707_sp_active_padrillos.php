<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpActivePadrillos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_active_padrillos`()
        BEGIN
        select
                s.*
            from
                specimens s
            where
                s.category = 1 and s.status in (1,4,5);
        END";
        DB::unprepared("DROP procedure IF EXISTS get_active_padrillos");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
