<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('known_as')->nullable();
            $table->enum('doc_type', ['DNI', 'PASSPORT', 'PTP', 'OTHER'])->nullable();
            $table->string('document_number')->nullable();
            $table->date('doe')->nullable();
            $table->string('nationality')->nullable();
            $table->date('dob')->nullable();
            $table->enum('gender', ['MASCULINO', 'FEMENINO'])->nullable();
            $table->enum('martial_status', ['SOLTERO(A)','CASADO(A)', 'SEPARADO(A)', 'DIVORCIADO(A)', 'VIUDO(A)'])->nullable();
            $table->string('photo')->nullable();
            $table->string('mailling_address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('country')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('work_email')->nullable();
            $table->string('private_email')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_phone')->nullable();
            $table->enum('relation', ['ESPOSO(A)', 'SOCIO(A)', 'MADRE', 'PADRE', 'HERMANA', 'HERMANO', 'HIJO', 'HIJA', 'AMIGO(A)', 'OTRO'])->nullable();
            $table->string('contact_name_second')->nullable();
            $table->string('contact_phone_second')->nullable();
            $table->enum('relation_second', ['ESPOSO(A)', 'SOCIO(A)', 'MADRE', 'PADRE', 'HERMANA', 'HERMANO', 'HIJO', 'HIJA', 'AMIGO(A)', 'OTRO'])->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('position')->nullable();
            $table->decimal('salary', 11,2)->nullable();
            $table->enum('status', ['ACTIVO', 'INACTIVO']);
            $table->integer('created_by');
            $table->datetime('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
