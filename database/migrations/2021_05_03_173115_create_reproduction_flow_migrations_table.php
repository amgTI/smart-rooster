<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReproductionFlowMigrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reproduction_flow_migrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('placa_madrilla')->nullable();
            $table->string('placa_padrillo')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->text('observation')->nullable();
            $table->enum('status', ['BORRADOR', 'MIGRADO']);
            $table->integer('created_by');
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reproduction_flow_migrations');
    }
}
