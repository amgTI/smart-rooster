<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveDeadSpecimens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_dead_specimens`(in t_cintillo_pata int,
        in t_cintillo_ala text,
        in t_plate text,
        in t_quantity int,
        in t_type_specimen int,
        in t_created_by int,
        in t_observation text,
        in t_circumstances int,
        in t_breeder_id int)
        BEGIN
                insert
                into
                dead_specimens (cintillo_pata,
                cintillo_ala,
                plate,
                quantity,
                type_specimen,
                created_by,
                created_at,
                updated_at,
                observation,
                circumstances,
                breeder_id)
            values (t_cintillo_pata,
            t_cintillo_ala,
            t_plate,
            t_quantity,
            t_type_specimen,
            t_created_by,
            now(),
            now(),
            t_observation,
            t_circumstances,
            t_breeder_id);

            select @@identity dead_specimen_id;

        END";
        DB::unprepared("DROP procedure IF EXISTS save_dead_specimens");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
