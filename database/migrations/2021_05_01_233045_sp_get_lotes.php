<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetLotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_lotes`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_date varchar(255))
        BEGIN
        declare count_lotes int;
        select count(*) into count_lotes from lotes l where
                    ( campo is null
                    or l.status like concat('%', campo, '%')
                    or l.created_by like concat('%', campo, '%')
                   	or l.code like concat('%', campo, '%')) and l.deleted_at is null and if(t_date is null, true, l.date_lote = t_date);

                set npage = perpage*(npage-1);

                set
                lc_time_names = 'es_ES';

                set
                @query = concat(\"select l.*, l.status+0 status_number, u.name first_name, u.last_name last_name,
								 s.alias madrilla_alias, DATE_ADD(l.date_lote, INTERVAL 8 DAY) infertile_revision_date,
								 DATE_ADD(l.date_lote, INTERVAL 19 DAY) hatcher_revision_date,
								 DATE_ADD(l.date_lote, INTERVAL 20 DAY) alive_revision_date,
									DATE_ADD(l.date_lote, INTERVAL 42 DAY) cintillo_ala_revision_date ,
									 \", count_lotes, \" count_lotes
									from lotes l join users u on u.id = l.created_by
										left join specimens s on s.plate = l.placa_madrilla
									 where \", if(campo is null, true, concat(\" l.date_lote like '%\", campo, \"%' or l.status like '%\", campo, \"%' or l.created_by like '%\", campo, \"%' or l.code like '%\", campo, \"%'\")),
									 if(t_date is null,'',concat(\" and l.date_lote = '\" , t_date, \"'\")),
									 \" and l.deleted_at is null order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

                prepare stmt1
                from
                @query;

                execute stmt1;

                deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_lotes");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
