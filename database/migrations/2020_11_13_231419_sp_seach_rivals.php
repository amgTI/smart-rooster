<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSeachRivals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  PROCEDURE `search_rivals`(in name_rival varchar(255))
        BEGIN
            select id,name
            from rivals
            where name like concat('%',name_rival,'%');
        END";
        DB::unprepared("DROP procedure IF EXISTS search_rivals");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
