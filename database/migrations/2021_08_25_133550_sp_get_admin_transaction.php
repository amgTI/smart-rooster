<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetAdminTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_admin_transaction`(
            in t_transaction_id int
        )
        BEGIN
            select
                at2.*,
                (
                select
                    json_arrayagg(json_object('motive',
                    atd.motive,
                    'motive_text',
                    r.reason,
                    'description',
                    atd.description,
                    'amount',
                    atd.amount))
                from
                    admin_transactions_detail atd
                left join reasons r on
                    r.id = atd.motive
                where
                    atd.transaction_id = at2.id) details, at2.id transaction_id, at2.type+0 type, at2.status+0 status
            from
                admin_transactions at2
            where
                at2.id = t_transaction_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_admin_transaction");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
