<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetRegisteredEggsDateDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_registered_eggs_date_detail`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_register_egg_date date)
        BEGIN
        declare count_registered_eggs_date int;

            select
                count(*)
            into
                count_registered_eggs_date
            from
                egg_postura ep
            join posturas p on
                p.id = ep.postura_id
            join specimens s on
                s.plate = p.placa_madrilla
            join users u on
            	u.id = ep.created_by
            where
                ( campo is null
                or s.alias like concat('%', campo, '%')
                or s.plate like concat('%', campo, '%')
                or concat(u.name, ' ', u.last_name) like concat('%', campo, '%')) and  ep.egg_postura_date = t_register_egg_date;

            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select
                s.plate placa_madrilla,
				s.id madrilla_id,
				p.id postura_id,
				s.photo photo,
				s.thumb thumb,
				ep.can_delete_until can_delete_until,
				s.alias madrilla_alias,
				ep.quantity quantity_eggs,
				u.name created_by_name,
				ep.created_at,
				u.last_name created_by_last_name,\", count_registered_eggs_date, \" count_registered_eggs_date
            from
                egg_postura ep
            join posturas p on
                p.id = ep.postura_id
            join specimens s on
                s.plate = p.placa_madrilla
            join users u on
            	u.id = ep.created_by

            where ep.egg_postura_date = '\",  t_register_egg_date, \"' and \", if(campo is null, true, concat(\" s.alias like '%\", campo, \"%' or s.plate like '%\", campo, \"%' or concat(u.name, ' ', u.last_name) like '%\", campo, \"%'\")), \"
                order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_registered_eggs_date_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
