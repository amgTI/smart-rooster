<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTypesCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types_currency', function (Blueprint $table) {
            $table->id();
            $table->string('currency');
            $table->string('description');
            $table->timestamps();
        });
        DB::table('types_currency')->insert(
            ['currency' => '$','description' => 'USD','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
        DB::table('types_currency')->insert(
            ['currency' => 'S/','description' => 'PEN','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types_currency');
    }
}
