<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_lotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('status');
            $table->text('notification');
            $table->string('link');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('lote_id');
            $table->date('date_lote');
            //$table->enum('type', ['INSTANTLY', 'SCHEDULED']); // 1 - INSTANTLY, 2 - SCHEDULED
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_lotes');
    }
}
