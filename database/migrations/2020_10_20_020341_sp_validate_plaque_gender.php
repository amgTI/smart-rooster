<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpValidatePlaqueGender extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `validate_plaque_by_gender`(in t_plate varchar(255))
        BEGIN
            declare p_plate varchar(255) default null;
            declare ngender int;

                select plate,gender into p_plate, ngender from specimens where plate = t_plate;
                select p_plate,ngender;

        END";
        DB::unprepared("DROP procedure IF EXISTS validate_plaque_by_gender");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
