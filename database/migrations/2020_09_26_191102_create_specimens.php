<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecimens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specimens', function (Blueprint $table) {
            $table->id();
            $table->integer('breeder_id')->nullable();
            $table->integer('origin')->nullable();
            $table->string('alias')->nullable();
            $table->string('plate')->nullable()->unique();
            $table->string('additional_plate')->nullable()->unique();
            $table->string('breeder')->nullable();
            $table->integer('currency')->nullable();
            $table->decimal('price',16,2)->nullable();
            $table->integer('color')->nullable();
            $table->integer('category')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('status')->nullable();
            $table->date('dob')->nullable();
            $table->text('observations')->nullable();
            $table->text('photo')->nullable();
            $table->text('thumb')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('delete_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specimens');
    }
}
