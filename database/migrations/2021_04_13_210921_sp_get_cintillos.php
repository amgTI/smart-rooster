<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetCintillos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_cintillos`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare count_cintillos int;
        select count(*) into count_cintillos from cintillos c where
                    ( campo is null
                    or c.description like concat('%', campo, '%'));

                set npage = perpage*(npage-1);

                set
                lc_time_names = 'es_ES';

                set
                @query = concat(\"select c.id, c.color, c.description, c.created_at, c.updated_at, u.name first_name_created_by, u.last_name last_name_created_by, \", count_cintillos, \" count_cintillos from cintillos c join users u on u.id = c.created_by where \", if(campo is null, true, concat(\" c.description like '%\", campo, \"%'\")), \"
                    order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

                prepare stmt1
                from
                @query;

                execute stmt1;

                deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_cintillos");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
