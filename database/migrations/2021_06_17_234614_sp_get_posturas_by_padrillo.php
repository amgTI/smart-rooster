<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetPosturasByPadrillo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_posturas_by_padrillo`(
            in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_status int, in t_placa_specimen varchar(255), in t_madrilla_status int
        )
        BEGIN
        declare count_posturas int;

        select
            count(*)
        into
            count_posturas
        from
            posturas post
        join specimens madrilla on
            madrilla.plate = post.placa_madrilla
        join specimens padrillo on
            padrillo.plate = post.placa_padrillo
        where
            ( campo is null
            or madrilla.alias like concat('%', campo, '%')
            or padrillo.alias like concat('%', campo, '%')
            or post.placa_madrilla like concat('%', campo, '%')
            or post.placa_padrillo like concat('%', campo, '%') )
            and post.placa_padrillo = t_placa_specimen and (post.status = t_status and if(t_madrilla_status is null, true, post.madrilla_status = t_madrilla_status));

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select post.method method, madrilla.photo, madrilla.thumb, post.id, post.postura_date, post.placa_madrilla, post.placa_padrillo, madrilla.alias madrilla_alias, post.deleted_at deleted_at, ud.name deleted_by_name, ud.last_name deleted_by_last_name,
                    padrillo.alias padrillo_alias, u.name, u.last_name, post.status as status, post.number_eggs_good, post.created_at, post.updated_at, padrillo.photo padrillo_photo, padrillo.thumb padrillo_thumb, padrillo.id padrillo_id, \", count_posturas, \" cc, madrilla.id madrilla_id, post.code from posturas post join specimens madrilla on madrilla.plate = post.placa_madrilla
                    join specimens padrillo on padrillo.plate = post.placa_padrillo join users u on u.id = post.created_by left join users ud on ud.id = post.deleted_by

                    where post.placa_padrillo = '\", t_placa_specimen, \"'\", \" and (\", if(campo is null, true, concat(\" madrilla.alias like '%\", campo, \"%' or padrillo.alias like '%\", campo, \"%' or post.placa_madrilla like '%\", campo, \"%' or post.placa_padrillo like '%\", campo, \"%'\")), \"
                    ) and (post.status = \",t_status,\" and \", if(t_madrilla_status is null, 'true', concat(\" post.madrilla_status = '\", t_madrilla_status, \"'\")),\")  order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_posturas_by_padrillo");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
