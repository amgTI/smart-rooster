<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_employees`(
            in t_campo varchar(255),in t_order_by varchar(255),in t_order varchar(255),in perpage int, in npage int
        )
        BEGIN
            declare cc int;

            select
                ifnull(count(*), 0)
            into
                cc
            from employees e
            join users u on
                u.id = e.created_by
            where
                e.deleted_at is null
                and (t_campo is null or concat(u.name, ' ', ifnull(u.last_name, '')) like concat('%', t_campo, '%')
                or concat(e.first_name, ' ', ifnull(e.middle_name, ''), ' ', ifnull(e.last_name, '')) like concat('%', t_campo, '%'));

            set
            npage = perpage*(npage-1);

            set
            @query = concat(\"select e.*, u.name created_by_name, u.last_name created_by_last_name, e.doc_type+0 doc_type_number, e.gender+0 gender_number, e.martial_status+0 martial_status_number, \", cc , \" cc
            from employees e
            join users u on u.id = e.created_by
            where e.deleted_at is null \",
            if(t_campo is null, '', concat(\" and (concat(u.name,' ', ifnull(u.last_name,'')) like '%\", t_campo, \"%'
            or concat(e.first_name,' ', ifnull(e.middle_name,''), ' ', ifnull(e.last_name,'')) like '%\", t_campo, \"%')\")),\"
            order by \", t_order , \" \", t_order_by , \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_employees");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
