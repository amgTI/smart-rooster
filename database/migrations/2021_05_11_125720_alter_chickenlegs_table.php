<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterChickenlegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Schema::table('chickenlegs', function(Blueprint $table)
        // {
        //     $table->dropColumn('priority');
        // });
        Schema::table('chickenlegs', function (Blueprint $table) {
            $table->integer('priority')->nullable()->unique()->after('description');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
