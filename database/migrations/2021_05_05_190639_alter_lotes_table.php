<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE lotes CHANGE COLUMN status status ENUM('BORRADOR LOTE', 'EN PROCESO LOTE', 'BORRADOR INFERTILE', 'EN PROCESO INFERTILE', 'BORRADOR VIVOS', 'EN PROCESO VIVOS', 'BORRADOR NACIMIENTO', 'EN PROCESO NACIMIENTO', 'FINALIZADO', 'NACEDORA', 'BORRADOR CINTILLO ALA', 'EN PROCESO CINTILLO ALA') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
