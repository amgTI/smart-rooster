<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDeleteLote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `delete_lote`(
            in t_lote_id int
        )
        BEGIN
        update
        lotes
            set
                deleted_at = now()
            where
                id = t_lote_id;

            select
                lp.lote_id,
                p.placa_madrilla,
                lp.quantity_eggs,
                lp.postura_id,
                lp.id
            from
                lote_posturas lp
            join posturas p on
                p.id = lp.postura_id
            where
                lp.deleted_at is null and lp.lote_id = t_lote_id;

        END";
        DB::unprepared("DROP procedure IF EXISTS delete_lote");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
