<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveAmarrador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_amarrador`(in t_amarrador varchar(255))
        BEGIN
            declare amarrador_exists varchar(255) default null;
            select name into amarrador_exists from amarrador where name = t_amarrador;
            if(amarrador_exists is null)then 
                insert into amarrador(name,created_at,updated_at) values(t_amarrador,now(),now());
                select amarrador_exists;
            else 
                select amarrador_exists;
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_amarrador");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
