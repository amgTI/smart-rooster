<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetRegisteredEggsMadrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_registered_eggs_madrilla`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare count_registered_eggs_madrilla int;

        select count(*) into count_registered_eggs_madrilla from (select
            sum(ep.quantity) suma,
            p.placa_madrilla placa_madrilla,
            s.alias madrilla_alias,
            count(*) count_eg
        from
            egg_postura ep
        join posturas p on
            p.id = ep.postura_id
        join specimens s on
            s.plate = p.placa_madrilla
        group by 2,3) v where
            ( campo is null
            or v.placa_madrilla like concat('%', campo, '%')
            or v.madrilla_alias like concat('%', campo, '%'));
        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select v.suma suma, v.placa_madrilla placa_madrilla, v.madrilla_alias madrilla_alias, v.photo, v.thumb , \", count_registered_eggs_madrilla, \" count_registered_eggs_madrilla, v.madrilla_id madrilla_id   from (select
                sum(case
		when ep.deleted_at is null then ep.quantity
		else 0
	end) suma,
				p.placa_madrilla placa_madrilla,
                s.alias madrilla_alias,
				s.photo photo,
				s.thumb thumb,
                count(*) count_eg,
				s.id madrilla_id


            from
                egg_postura ep
            join posturas p on
                p.id = ep.postura_id
            join specimens s on
                s.plate = p.placa_madrilla
            group by 2,3,4,5,7) v

        where \", if(campo is null, true, concat(\" v.placa_madrilla like '%\", campo, \"%' or v.madrilla_alias like '%\", campo, \"%'\")), \"
            order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_registered_eggs_madrilla");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
