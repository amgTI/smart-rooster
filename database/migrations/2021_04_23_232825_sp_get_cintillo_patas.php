<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetCintilloPatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_cintillo_patas`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare count_cintillo_patas int;

            select
                count(*)
            into
                count_cintillo_patas
            from
                cintillo_patas cp
            join users u on
                u.id = cp.created_by
            join cintillos c on
                c.id = cp.cintillo_id
			join chickenlegs cl on
                cl.id = cp.chickenleg_id
            where
                ( campo is null
                or concat(u.name, ' ', u.last_name) like concat('%', campo, '%')
                or c.description like concat('%', campo, '%')
                or cl.description like concat('%', campo, '%'));

            set
            npage = perpage*(npage-1);

            set
            lc_time_names = 'es_ES';

            set
            @query = concat(\"select
                    c.description cintillo_description,
					c.color cintillo_color,
					cl.description chickenleg_description,
					cp.status cintillo_pata_status,
					cp.status+1 cintillo_pata_status_number,
					cp.created_at created_at,
					u.name created_by_name,
					u.last_name created_by_last_name,
                    \", count_cintillo_patas, \" count_cintillo_patas

                from
                    cintillo_patas cp
                join users u on
	                u.id = cp.created_by
	            join cintillos c on
	                c.id = cp.cintillo_id
				join chickenlegs cl on
	                cl.id = cp.chickenleg_id

            where \", if(campo is null, true, concat(\" concat(u.name, ' ', u.last_name) like '%\", campo, \"%' or c.description like '%\", campo, \"%' or cl.description like '%\", campo, \"%'\")), \"
                order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

            prepare stmt1
            from
            @query;

            execute stmt1;

            deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_cintillo_patas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
