<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetEggs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_eggs`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text, in t_status int, in t_date varchar(255))
        BEGIN
        declare count_registered_eggs int;

        select
            count(*)
        into
            count_registered_eggs
        from
            eggs ep
        join users u on
            u.id = ep.created_by
        where
            ( campo is null
            or concat(u.name, ' ', u.last_name) like concat('%', campo, '%')) and if(t_status=1, ep.deleted_at is null, ep.deleted_at is not null) and if(t_date is null, true, ep.egg_postura_date = t_date);

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select
                e.*,
                \", count_registered_eggs, \" count_registered_eggs,
                uc.name created_by_name,
                uc.last_name created_by_last_name,
                ud.name deleted_by_name,
                ud.last_name deleted_by_last_name,
                e.total_eggs_quantity total_eggs_quantity
            from
                eggs e
            join users uc on
                uc.id = e.created_by
            left join users ud on
                ud.id = e.deleted_by

        where \", if(t_status=1,  \"e.deleted_at is null\" , \"e.deleted_at is not null\" ),
        \" and \", if(campo is null, true, concat(\" concat(uc.name, ' ', uc.last_name) like '%\", campo, \"%'\")),
           if(t_date is null,'',concat(\" and e.egg_postura_date = '\" , t_date, \"'\")),
        \" order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_eggs");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
