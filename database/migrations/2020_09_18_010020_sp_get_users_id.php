<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetUsersId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE  PROCEDURE `get_users_id`(in iduser int)
            BEGIN
            select u.id,u.name,u.last_name,u.phone,u.email user_email,u.password_literal password , u.status,
            ifnull((select json_arrayagg(JSON_OBJECT('id', ur.role_id, 'name',r.name))
            from role r
                join user_role ur on ur.role_id = r.id
                join users u1 on u1.id = ur.user_id
                where u1.id = u.id order by u.updated_at desc),'') u_roles,u.avatar
            from users u
                join user_role ur on ur.user_id = u.id
            where u.id = iduser group by u.id;

            END";
        DB::unprepared("DROP procedure IF EXISTS get_users_id");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
