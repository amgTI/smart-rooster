<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpTurnOffMadrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `turn_off_madrilla`(
            in t_placa_madrilla varchar(255), in t_reason_id int
        )
        BEGIN
            update specimens set is_being_used = 0 where plate = t_placa_madrilla;
            update posturas set madrilla_status = 0, turn_off_reason = t_reason_id where placa_madrilla = t_placa_madrilla and status = 3;

        END";
        DB::unprepared("DROP procedure IF EXISTS turn_off_madrilla");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
