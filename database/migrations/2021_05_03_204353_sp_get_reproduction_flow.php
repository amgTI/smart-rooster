<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetReproductionFlow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_reproduction_flow`(in t_reproduction_flow_migration_id int)
        BEGIN
                select
                rfm.placa_madrilla,
                rfm.placa_padrillo,
                rfm.date_of_birth,
                rfm.observation,
                rfm.status,
                rfm.status + 0 status_number,
                (
                select
                    JSON_ARRAYAGG(JSON_OBJECT('cintillo_ala',
                    rfmd.cintillo_ala,
                    'gender',
                    rfmd.gender+0,
                    'migration_id',
                    rfmd.id))
                from
                    reproduction_flow_migrations_detail rfmd
                join reproduction_flow_migrations r on
                    r.id = rfmd.flow_migration_id
                where
                    rfmd.flow_migration_id = t_reproduction_flow_migration_id) detail
            from
                reproduction_flow_migrations rfm
            where
                id = t_reproduction_flow_migration_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_reproduction_flow");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
