<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_user`(in userid int, in u_name varchar(256),in u_last_name varchar(256),in u_phone varchar(256),useremail varchar(256),in literal_password varchar(255) ,in u_password varchar(256), in u_role json, in user_status int,in t_update int, in url_avatar text)
        BEGIN
		declare x int default 0;
                declare new_user int;
                declare v_password varchar(255);
                declare l_password varchar(255);
                START TRANSACTION;
                    if(t_update is null) then
                        insert into users(name,last_name,phone,email,password_literal,password,status,create_by,created_at,updated_at)
                        values(u_name,u_last_name,u_phone,useremail,literal_password,u_password,user_status,userid,now(),now());

                        select @@identity into new_user;
                        if(url_avatar is not null) then

                            update users set avatar = concat('users/',new_user,'/',url_avatar) where id = new_user;
                        end if;
                        repeat

                        if(JSON_EXTRACT(u_role, concat('$[',x,'].id')) <> 0)then
                        insert into user_role(user_id,role_id,created_at, updated_at) values(new_user,JSON_EXTRACT(u_role, concat('$[',x,'].id')),now(),now());
                        end if;
                        set x = x + 1;
                        until x >= JSON_LENGTH(u_role)
                    end repeat;
                    select new_user as id;
                    else
                    delete from user_role where user_id = t_update;
					repeat
						if(JSON_EXTRACT(u_role, concat('$[',x,'].id')) <> 0)then
							insert into user_role(user_id,role_id,created_at,updated_at) values(t_update,JSON_EXTRACT(u_role, concat('$[',x,'].id')),now(),now());
							end if;
							set x = x + 1;
							until x >= JSON_LENGTH(u_role)
							end repeat;

                            if(u_password = (select u.password_literal from users u where u.id = t_update)) then

                                update users set name = u_name, last_name = u_last_name,phone = u_phone, email = useremail,status = user_status,updated_at = now() where id = t_update;

								else

									update users set name = u_name, last_name = u_last_name,phone = u_phone, email = useremail,password_literal = literal_password,password = u_password,status = user_status,updated_at = now() where id = t_update;

							end if;

							if(url_avatar is not null) then
								update users set avatar = concat('users/',t_update,'/',url_avatar) where id = t_update;
							end if;
							select t_update as id;
					end if;

             commit;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_user");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
