<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetReproductionFlowMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_reproduction_flow_migrations`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare cc int;

        select
            count(*)
        into
            cc
        from
            reproduction_flow_migrations rfm
        join users u on
            u.id = rfm.created_by
        join specimens madrilla on
            madrilla.plate = rfm.placa_madrilla
        join specimens padrillo on
            padrillo.plate = rfm.placa_padrillo
        where
            (( campo is null
            or madrilla.alias like concat('%', campo, '%')
            or padrillo.alias like concat('%', campo, '%')
            or rfm.placa_madrilla like concat('%', campo, '%')
            or rfm.placa_padrillo like concat('%', campo, '%') ) and rfm.deleted_at is null);

        set
        npage = perpage*(npage-1);

        set
        lc_time_names = 'es_ES';

        set
        @query = concat(\"select
                rfm.id id,
            em.plate placa_madrilla, \", cc , \" cc,
                ep.plate placa_padrillo,
                em.alias madrilla_alias,
                em.photo photo, em.thumb thumb,
                ep.alias padrillo_alias,
                rfm.date_of_birth date_of_birth,
                rfm.observation observation,
                rfm.status status_text,
                rfm.status+0 status,
                uc.name created_by_name,
                ud.name deleted_by_name,
                ud.last_name deleted_by_last_name,
                uc.last_name created_by_last_name,
                rfm.created_at created_at,
                rfm.deleted_at deleted_at,
                ep.photo padrillo_photo, ep.thumb padrillo_thumb,
                ep.id padrillo_id
            from
                reproduction_flow_migrations rfm
            join specimens em on
                em.plate = rfm.placa_madrilla
            join specimens ep on
                ep.plate = rfm.placa_padrillo
            join users uc on
                uc.id = rfm.created_by
            left join users ud on
                ud.id = rfm.deleted_by

        where rfm.deleted_at is null and (\", if(campo is null, true, concat(\" em.alias like '%\", campo, \"%' or ep.alias like '%\", campo, \"%' or rfm.placa_madrilla like '%\", campo, \"%' or rfm.placa_padrillo like '%\", campo, \"%'\")), \")
            order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_reproduction_flow_migrations");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
