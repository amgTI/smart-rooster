<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDeleteSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `detele_specimen`(in idspec int, in iduser int)
        BEGIN
            update specimens set deleted_at = now(), delete_by = iduser where id = idspec;
        END";
        DB::unprepared("DROP procedure IF EXISTS detele_specimen");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
