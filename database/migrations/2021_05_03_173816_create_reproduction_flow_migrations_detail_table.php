<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReproductionFlowMigrationsDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reproduction_flow_migrations_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('flow_migration_id');
            $table->foreign('flow_migration_id')->references('id')->on('reproduction_flow_migrations');
            $table->string('cintillo_ala');
            $table->enum('gender', ['MACHO', 'HEMBRA', 'SIN ASIGNAR']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reproduction_flow_migrations_detail');
    }
}
