<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveCombination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_combination`(in t_cintillo_id int, in t_chickenleg_id int, in t_created_by int)
        BEGIN
            if(!exists (select * from cintillo_patas where cintillo_id = t_cintillo_id and chickenleg_id = t_chickenleg_id)) then
                insert into cintillo_patas (cintillo_id, chickenleg_id, created_by, created_at, updated_at) values(t_cintillo_id,t_chickenleg_id,t_created_by,now(),now());
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_combination");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
