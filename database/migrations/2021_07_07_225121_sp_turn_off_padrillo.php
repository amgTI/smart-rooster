<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpTurnOffPadrillo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `turn_off_padrillo`(
            in t_placa_padrillo varchar(255), in t_reason_id int, in t_postura_id int
        )
        BEGIN
        update
        specimens s
    set
        s.is_being_used = 0
    where
        s.plate = t_placa_padrillo;

    update
        specimens s
    set
        s.is_being_used = 0
    where
        s.plate in (
        select
            p.placa_madrilla
        from
            posturas p
        where
            p.placa_padrillo = t_placa_padrillo
            and p.status = 3);

    update
        posturas p
    set
        p.padrillo_status = 0,
        p.madrilla_status = 0
    where
        p.placa_padrillo = t_placa_padrillo
        and p.status = 3;

    update
        posturas p
    set
        p.turn_off_reason = t_reason_id
    where
        p.id = t_postura_id;


        END";
        DB::unprepared("DROP procedure IF EXISTS turn_off_padrillo");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
