<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpInsertLote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `insert_lote`(in t_placa_madrilla varchar(255), in t_total_eggs int, in t_date_lote date, in t_created_by int, in t_status int, in t_lote_id int)
        BEGIN
        if(t_lote_id is null) then
	            insert
					into
					lotes (placa_madrilla,
					total_eggs,
					date_lote,
					created_by,
					status,
					created_at,
					updated_at)
				values(t_placa_madrilla,
				t_total_eggs,
				t_date_lote,
				t_created_by,
				t_status,
				now(),
				now());

				set
				@last_lote_id = @@identity;



				update lotes set code = CONCAT('L', right(year(date_lote), 2), LPAD(@last_lote_id, 5, '0')) where id = @last_lote_id;
			else
				update lotes set placa_madrilla = t_placa_madrilla,
								total_eggs = t_total_eggs,
								date_lote = t_date_lote,
								status = t_status,
								updated_at = now()
							where id = t_lote_id;
					set @last_lote_id = t_lote_id;

			end if;

			if(t_status = 2) then
				insert
					into
					notifications (status,
					notification,
					link,
					role_id,
					delivery_notificaction_date,
					module,
					type,
					created_at,
					updated_at)
				values (0,
				concat('El lote', ' ', @last_lote_id, ' ', 'requiere revisión de huevos infértiles'),
				concat('/main/reproduccion/sacar-crias/revision-infertiles/', @last_lote_id),
				6,
				DATE_ADD(t_date_lote, INTERVAL 8 DAY),
				'Lote',
				2,
				now(),
				now());
				insert
						into
						notifications (status,
						notification,
						link,
						role_id,
						delivery_notificaction_date,
						module,
						type,
						created_at,
						updated_at)
					values (0,
					concat('El lote', ' ', @last_lote_id, ' ', 'requiere revisión de pollos vivos'),
					concat('/main/reproduccion/sacar-crias/revision-vivos/', @last_lote_id),
					6,
					DATE_ADD(t_date_lote, INTERVAL 21 DAY),
					'Lote',
					2,
					now(),
					now());
				end if;
			select
				@last_lote_id last_lote_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS insert_lote");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
