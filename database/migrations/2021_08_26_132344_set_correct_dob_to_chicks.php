<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetCorrectDobToChicks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $statement = 'update chicks ck
       inner join lote_posturas lp on
           lp.id = ck.lote_postura_id
       inner join lotes l on
           l.id = lp.lote_id
       inner join tracking_lote tl on
           tl.lote_id = l.id
       set ck.date_of_birth = date(tl.created_at)
       where tl.status = 3;';
       DB::select($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
