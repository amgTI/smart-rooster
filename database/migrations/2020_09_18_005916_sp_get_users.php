<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_users`(in iduser int,in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
BEGIN
declare cc int;
			select count(*) into cc from users u
				join users u2 on u2.id = u.create_by
            where u.create_by = iduser
             and ( campo is null  or  concat(u.name,' ', ifnull(u.last_name,''))  like concat('%',campo,'%')  or  u.email like concat('%',campo, '%') );

            set npage=perpage*(npage-1);

            SET lc_time_names = 'es_ES';

	  set @query = concat(\"
				select concat(u.name,' ',ifnull(u.last_name,'')) name_user, u.email,u.updated_at,\",cc,\" cc,
				concat(u2.name,' ',ifnull(u2.last_name,''),' <br> ',DATE_FORMAT(u.created_at,'%d  %M  %Y') ,' ',DATE_FORMAT(u.created_at,'%r'))  name_create,
					if(u.status = 1,'Activo','Inactivo') status,
					concat(date_format(last_access_log(u.id),'%d/%m/%Y'),' ',date_format(last_access_log(u.id),'%r')) t_access_log ,
					u.id,(select json_arrayagg(JSON_OBJECT('id_role', ur.role_id, 'name_role',r.name))
							from role r
							join user_role ur on ur.role_id = r.id
							where ur.user_id = u.id) u_roles,u.avatar
				from users u
					join users u2 on u2.id = u.create_by
				where u.create_by = \", iduser,
			if(campo is null,'',concat(\" and concat(u.name,' ', ifnull(u.last_name,'')) like '%\",campo,\"%' or u.email like '%\",campo, \"%'\")),\"
			order by \",orden,' ',orderby,\" limit \",perpage,\" offset \",npage,\";\");
			PREPARE stmt1 FROM @query;
			EXECUTE stmt1;
			DEALLOCATE PREPARE stmt1;
	END";
        DB::unprepared("DROP procedure IF EXISTS get_users");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
