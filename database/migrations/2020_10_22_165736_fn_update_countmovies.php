<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FnUpdateCountmovies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE FUNCTION `update_countmovies`( ide int) RETURNS int(11)
        BEGIN
		declare c int;
		declare specid int;
        declare idrival int;
        
            select idspecimen, rival into specid,idrival
            from video 
            where id = ide;
            
            select count(*) into c
            from video 
            where idspecimen = specid  and deleted_at is null ;
            

            if(c > 0 )then
                update specimens set countmovies = 1 where id = specid;
            else 
                update specimens set countmovies = 0 where id = specid; 
            end if;
            
            if(idrival is not null)then
                if(c > 0 )then
                    update specimens set countmovies = 1 where id = idrival;
                else 
                    update specimens set countmovies = 0 where id = idrival; 
                end if;
            end if;
                    
        RETURN 1;
        END";
        DB::unprepared("DROP FUNCTION IF EXISTS update_countmovies");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
