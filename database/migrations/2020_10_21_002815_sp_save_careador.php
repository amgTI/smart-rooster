<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveCareador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_careador`(in t_careador varchar(255))
        BEGIN
            declare careador_exists varchar(255) default null;
            select name into careador_exists from careador where name = t_careador;
            if(careador_exists is null)then 
                insert into careador(name,created_at,updated_at) values(t_careador,now(),now());
                select careador_exists;
            else 
                select careador_exists;
            end if;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_careador");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
