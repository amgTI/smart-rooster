<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDeadSpecimenAddObservation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dead_specimens', function (Blueprint $table) {
            $table->text('observation')->nullable()->after('quantity');
            $table->enum('circumstances', ['PELEA', 'NATURAL'])->nullable()->after('type_specimen');
            $table->unsignedBigInteger('breeder_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
