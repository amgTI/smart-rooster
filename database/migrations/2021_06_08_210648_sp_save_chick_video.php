<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveChickVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_chick_video`(
            in idchick int,
                        in iduser int,
                        in type_video int,
                        in namevideo varchar(255),
                        in t_category int,
                        in t_cintillorival varchar(255),
                        in t_event_date date,
                        in t_event_result int,
                        in t_amarrador int,
                        in t_careador int,
                        in t_duration varchar(20),
                        in video_url text,
                        in v_thumb varchar(255)
        )
        BEGIN
        declare idchk int;
            declare idvideo int;
            START TRANSACTION;
                if(type_video = 2)then
                    insert into chick_videos (chick_id,user_id,type_of_video,description,amarrador,careador,event_date,event_result,rival_id, url,thumb,duration,created_at,updated_at)
                    values(idchick,iduser,type_video,namevideo,t_amarrador,t_careador,t_event_date,t_event_result,t_cintillorival,video_url,v_thumb,t_duration,now(),now());

                    select @@identity into idvideo;

                    select update_countmovies(idvideo);

                end if;
                if(t_category = 2)then

                    insert into chick_videos (chick_id,user_id,type_of_video,description,type_of_rival,event_date,rival_id, url,thumb,duration,created_at,updated_at)
                    values(idchick,iduser,type_video,namevideo,t_category,t_event_date,t_cintillorival,video_url,v_thumb,t_duration,now(),now());

                    select @@identity into idvideo;

                    select update_countmovies(idvideo);

                end if;
                if(t_category = 1)then

                    select id into idchk
                    from chicks
                    where cintillo_ala = t_cintillorival;

                    insert into chick_videos (chick_id,user_id,type_of_video,description,type_of_rival,event_date,rival ,url,thumb,duration,created_at,updated_at)
                    values(idchick,iduser,type_video,namevideo,t_category,t_event_date,idchk,video_url,v_thumb,t_duration,now(),now());

                    select @@identity into idvideo;

                    select update_countmovies(idvideo);

                end if;
            commit;

        END";
        DB::unprepared("DROP procedure IF EXISTS save_chick_video");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
