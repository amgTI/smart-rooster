<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeadSpecimensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dead_specimens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cintillo_pata')->nullable();
            $table->string('cintillo_ala')->nullable();
            $table->string('plate')->nullable();
            $table->integer('quantity');
            $table->enum('type_specimen', ['CINTILLO PATA', 'CINTILLO ALA', 'PLACA']);
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dead_specimens');
    }
}
