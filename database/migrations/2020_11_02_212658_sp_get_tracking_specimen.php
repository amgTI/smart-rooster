<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetTrackingSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_tracking_specimen`(in idspec int)
        BEGIN
            SET lc_time_names = 'es_ES';
            select concat(u.name, ' ' , u.last_name) name_user, ts.description, ts.url_evidence, ts.url_evidence_thumb,ts.created_at,DATE_FORMAT(ts.death_date,'%d  %M  %Y') death_date,
            concat(DATE_FORMAT(ts.created_at,'%d  %M  %Y') ,' ',DATE_FORMAT(ts.created_at,'%r')) fecha,
            ss.description description_tracking
            from tracking_status_specimen ts 
                join users u on u.id = ts.user_id
                join status_specimens ss on ss.id = ts.status
            where ts.idspecimen = idspec;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_tracking_specimen");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
