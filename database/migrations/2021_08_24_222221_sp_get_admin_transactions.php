<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetAdminTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_admin_transactions`(
            in t_campo varchar(255),in t_order_by varchar(255),in t_order varchar(255),in perpage int, in npage int, in date_from date,in date_to date, in t_type int
        )
        BEGIN
        declare cc int;

        select
            ifnull(count(*), 0)
        into
            cc
        from admin_transactions mc
        join users u on
            u.id = mc.created_by
        where
            mc.deleted_by is null
            and (t_campo is null or concat(u.name, ' ', ifnull(u.last_name, '')) like concat('%', t_campo, '%'))
           	and ((date_from is null or date_to is null) or date(mc.transaction_date)>=date_from and date(mc.transaction_date)<=date_to)
           	and mc.type = t_type;

        set
        npage = perpage*(npage-1);

        set
        @query = concat(\"select mc.*, u.name, u.last_name, mc.type+0 type_number, \", cc , \" cc
		from admin_transactions mc
		join users u on u.id = mc.created_by
		where mc.deleted_by is null \",
		if(t_type is not null, concat(\" and mc.type = \", t_type),''),
		if((date_from is null or date_to is null),'',concat(\" and date(mc.transaction_date)>='\",date_from,\"' and date(mc.transaction_date)<='\",date_to,\"'\")),
		if(t_campo is null, '', concat(\" and concat(u.name,' ', ifnull(u.last_name,'')) like '%\", t_campo, \"%'\")), \"
		order by \", t_order , \" \", t_order_by , \" limit \", perpage, \" offset \", npage, \";\");

        prepare stmt1
        from
        @query;

        execute stmt1;

        deallocate prepare stmt1;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_admin_transactions");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
