<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetPosturaDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_postura_detail`(in postura_id int)
        BEGIN
            select
            p.id postura_id,
            ep.quantity,
            e.created_by,
            e.created_at,
            e.deleted_at,
            e.egg_postura_date egg_postura_date,
            madrilla.alias madrilla_alias,
            madrilla.plate placa_madrilla,
            padrillo.alias padrillo_alias,
            padrillo.plate placa_padrillo,
            p.number_eggs_good number_eggs_good,
            u.name created_by_first_name,
            u.last_name created_by_last_name,
            cint.description cintillo_description,
            cint.color cintillo_color,
            cll.description chickenleg_description,
            cpp.status+0 cintillo_pata_postura_number,
            p.code,
            (
            select
                JSON_ARRAYAGG(JSON_OBJECT('cintillo_ala',
                chk.cintillo_ala,
                'gender',
                chk.gender,
                'date_of_birth',
                chk.date_of_birth, 'new_dob', '', 'is_editing_dob', 0, 'chick_id', chk.id, 'lote_code', l5.code, 'assigned_plate', chk.assigned_plate, 'gender_number', chk.gender+0, 'is_assigned_plate', chk.assigned_plate is not null, 'status_number', chk.status+0, 'is_correct_plate', 0) )
            from
                chicks chk
                join lote_posturas lp5 on lp5.id = chk.lote_postura_id
                join lotes l5 on l5.id = lp5.lote_id
            where
                lp5.postura_id = postura_id and lp5.deleted_at is null) postura_pollitos
        from
            posturas p
        left join egg_postura ep on
            p.id = ep.postura_id
        left join specimens madrilla on
            madrilla.plate = p.placa_madrilla
        left join specimens padrillo on
            padrillo.plate = p.placa_padrillo
        left join eggs e on
            e.id = ep.eggs_id
        left join users u on
            u.id = e.created_by
        left join cintillo_pata_posturas cpp on
            cpp.postura_id = p.id
        left join cintillo_patas cp on
            cp.id = cpp.cintillo_pata_id
        left join cintillos cint on
            cint.id = cp.cintillo_id
        left join chickenlegs cll on
            cll.id = cp.chickenleg_id

        where
            p.id = postura_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_postura_detail");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
