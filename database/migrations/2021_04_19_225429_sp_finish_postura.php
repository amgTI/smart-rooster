<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpFinishPostura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `finish_postura`(in t_postura_id int, in t_comment text, in t_finished_by int)
        BEGIN
        declare combination_id int;
        declare t_placa_padrillo varchar(255);
        declare t_placa_madrilla varchar(255);
        start transaction;
            select placa_madrilla, placa_padrillo into t_placa_madrilla, t_placa_padrillo from posturas where id = t_postura_id;
            update posturas set status = 4, finished_comment = t_comment, seen_status = 1, finished_at = now(), finished_by = t_finished_by where id = t_postura_id;
        update specimens set is_being_used = 0 where plate = t_placa_padrillo;
            update specimens set is_being_used = 0 where plate = t_placa_madrilla;
        commit;
        END";
        DB::unprepared("DROP procedure IF EXISTS finish_postura");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
