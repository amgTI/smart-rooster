<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetVideoSpecimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_videos_specimen`(in idspe int)
        BEGIN
            SET lc_time_names = 'es_ES';

            select v.id, v.idspecimen, v.type_of_video, v.description, v.url, v.duration, v.thumb, v.type_of_video, v.type_of_rival,
                DATE_FORMAT(v.event_date,'%d  %M  %Y') event_date,v.event_result, if(idspe = v.idspecimen ,1,0) permission,
                concat(concat(u.name,' ',ifnull(u.last_name,'')),' <br> ',DATE_FORMAT(v.created_at,'%d  %M  %Y') ,' ',DATE_FORMAT(v.created_at,'%r') ) created_at,
                case
                    when v.type_of_video + 0  = 2 or v.type_of_rival + 0 = 2 then vl.name
                    when v.idspecimen = idspe then sr.plate
                    else s.plate
                end rival
            from video v
                join users u on u.id = v.user_id
                join specimens s on s.id = v.idspecimen
                left join specimens sr on sr.id = v.rival
                left join rivals vl on vl.id = v.rival_id
            where if(v.idspecimen = idspe, v.idspecimen , v.rival) = idspe
            and v.deleted_at is null
            order by v.event_date desc;

		END";
        DB::unprepared("DROP procedure IF EXISTS get_videos_specimen");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
