<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpActivatePostura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `activate_postura`(in postura_id int, in t_comment text, in t_method int)
        BEGIN
            update posturas set status = 3, activation_comment = t_comment, seen_status = 1, method = t_method where id = postura_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS activate_postura");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
