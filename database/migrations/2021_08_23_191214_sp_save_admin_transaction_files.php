<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSaveAdminTransactionFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_admin_transaction_files`(in t_transaction_id int,
        in t_name varchar(255),
        in t_url varchar(255),
        in t_size varchar(255),
        in t_ext varchar(255),
        in t_created_by int)
        BEGIN
            insert
                into
                admin_transactions_files (transaction_id,
                name,
                url,
                size,
                ext,
                created_by, created_at, updated_at)
            values (t_transaction_id,
            t_name,
            t_url,
            t_size,
            t_ext,
            t_created_by, now(), now());
        END";
        DB::unprepared("DROP procedure IF EXISTS save_admin_transaction_files");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
