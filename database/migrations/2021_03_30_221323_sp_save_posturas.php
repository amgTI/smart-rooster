<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpSavePosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `save_postura`(in p_madrilla varchar(255), in p_padrillo varchar(255), in t_observation longtext, in t_created_by int, in t_postura_date date, in t_method int, in t_status int)
        BEGIN
        declare postura_id int;
        insert
            into
            posturas(placa_madrilla,
            placa_padrillo,
            observation,
            postura_date,
            created_by,
            created_at,
            updated_at,
            method,
            status)
        values(p_madrilla,
        p_padrillo,
        t_observation,
        t_postura_date,
        t_created_by,
        now(),
        now(),
        t_method,
        t_status
        );

    update specimens set is_being_used = 1 where plate = p_padrillo;
        update specimens set is_being_used = 1 where plate = p_madrilla;
            select @@identity into postura_id;

       update posturas set code = CONCAT('P', right(year(postura_date), 2), LPAD(postura_id, 5, '0')) where id = postura_id;

       select postura_id posturaId;
        END";
        DB::unprepared("DROP procedure IF EXISTS save_postura");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
