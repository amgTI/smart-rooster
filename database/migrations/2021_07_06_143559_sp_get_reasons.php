<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_reasons`(in npage int,in perpage int,in orderby varchar(4),in orden int,in campo text)
        BEGIN
        declare cc int;
        select count(*) into cc from reasons r where
                    ( campo is null
                    or r.reason like concat('%', campo, '%')) and r.deleted_at is null;

                set npage = perpage*(npage-1);

                set
                lc_time_names = 'es_ES';

                set
                @query = concat(\"select r.*, u.name first_name_created_by, u.last_name last_name_created_by, \", cc, \" cc, r2.reason parent_reason, m.name module_name from reasons r join users u on u.id = r.created_by left join reasons r2 on r2.id = r.parent_id join modules m on m.id = r.module_id where r.deleted_at is null and \", if(campo is null, true, concat(\" r.reason like '%\", campo, \"%'\")), \"
                    order by \", orden, ' ', orderby, \" limit \", perpage, \" offset \", npage, \";\");

                prepare stmt1
                from
                @query;

                execute stmt1;

                deallocate prepare stmt1;

        END";
        DB::unprepared("DROP procedure IF EXISTS get_reasons");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
