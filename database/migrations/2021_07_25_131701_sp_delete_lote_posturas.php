<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpDeleteLotePosturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `delete_lote_posturas`(
            in t_lote_id int, in t_placa_madrilla varchar(255), in t_quantity_eggs int, in t_postura_id int, in t_lote_postura_id int, in t_status int
        )
        BEGIN
        declare total_removed_eggs int;

            declare total_remaining_eggs int;
    if(t_status > 1) then
		select
			p.removed_eggs - t_quantity_eggs
		into
			total_removed_eggs
		from
			posturas p
		where
			p.id = t_postura_id;

		select
			p.remaining_eggs+t_quantity_eggs
		into
			total_remaining_eggs
		from
			posturas p
		where
			p.id = t_postura_id;

		update
			posturas p
		set
			p.removed_eggs = total_removed_eggs,
			p.remaining_eggs = total_remaining_eggs
		where
			p.id = t_postura_id;


	end if;
	update lote_posturas set deleted_at = now() where id = t_lote_postura_id;

        END";
        DB::unprepared("DROP procedure IF EXISTS delete_lote_posturas");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
