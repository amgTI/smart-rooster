<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetCountByPosturaStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
            CREATE PROCEDURE `get_count_by_postura_status`()
            BEGIN
            declare count_anulado int;
            declare count_pendiente int;
            declare count_proceso int;
            declare count_finalizado int;
            select count(*) into count_anulado from posturas p where status = 1 and seen_status = 1;
            select count(*) into count_pendiente from posturas p where status = 2 and seen_status = 1;
            select count(*) into count_proceso from posturas p where status = 3 and seen_status = 1;
            select count(*) into count_finalizado from posturas p where status = 4 and seen_status = 1;


            select count_anulado, count_pendiente, count_proceso, count_finalizado;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_count_by_postura_status");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
