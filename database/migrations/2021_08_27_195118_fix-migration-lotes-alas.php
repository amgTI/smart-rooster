<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixMigrationLotesAlas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $statement_first = "update lotes set status = 12 where id in (
                select id from (select l.id from lotes l join lote_posturas lp on lp.lote_id = l.id join posturas p on p.id = lp.postura_id where p.observation = 'Migracion de data') updateds
        );";
        DB::select($statement_first);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
