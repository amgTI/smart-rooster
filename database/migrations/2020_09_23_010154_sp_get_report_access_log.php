<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetReportAccessLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_report_access_log`(in iduser int)
            BEGIN
            SET lc_time_names = 'es_ES';
                select concat(DATE_FORMAT(created_at,'%d  %M  %Y') ,' ',DATE_FORMAT(created_at,'%r'))  created_at, concat(DATE_FORMAT(updated_at,'%d  %M  %Y') ,' ',DATE_FORMAT(updated_at,'%r'))  ending_session
                from access_log 
                where user_id = iduser ;
            END";
        DB::unprepared("DROP procedure IF EXISTS get_report_access_log");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
