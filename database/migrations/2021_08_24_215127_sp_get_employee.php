<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SpGetEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `get_employee`(
            in t_employee_id int
        )
        BEGIN
            select
                e.*,
                e.doc_type + 0 doc_type,
                e.gender + 0 gender,
                e.martial_status + 0 martial_status,
                e.relation + 0 relation,
                e.relation_second relation_second,
                null created_by,
                null photo_name,
                e.id employee_id
            from
                employees e
            where
                e.id = t_employee_id;
        END";
        DB::unprepared("DROP procedure IF EXISTS get_employee");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
