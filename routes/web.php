<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','UserController@showlogin');
Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::group([
        'namespace' => '\Haruncpi\LaravelLogReader\Controllers',
        'middleware' => config('laravel-log-reader.middleware')
        ], function () {
        Route::get(config('laravel-log-reader.view_route_path'), 'LogReaderController@getIndex');
        Route::post(config('laravel-log-reader.view_route_path'), 'LogReaderController@postDelete');
        Route::get(config('laravel-log-reader.api_route_path'), 'LogReaderController@getLogs');
    });
    Route::get('/home', 'HomeController@index')->name('home');
    //users
    Route::get('user/getCurrentUser', 'UserController@getCurrentUser');
    Route::get('/users','UserController@index')->name('users.index');
    Route::get('/users/create','UserController@create')->name('users.create');
    Route::get('/users/edit/{id}','UserController@edit')->name('users.edit');
    //specimens
    Route::get('/specimens','Specimens\SpecimensController@index')->name('specimens.index');
    Route::get('/specimens/create','Specimens\SpecimensController@create')->name('specimens.create');
    Route::get('/specimens/edit/{id}','Specimens\SpecimensController@edit')->name('specimens.edit');
    Route::get('{any}', function () {
        return view('layouts.app');
    })->where('any', '.*');
});

