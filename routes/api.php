<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', 'UserController@showApi');
//Users
Route::get('users/edit/{id}', 'Api\ApiUserController@edit');
Route::post('save-update-user', 'Api\ApiUserController@saveuser');
Route::post('get-role', 'Api\ApiUserController@getrole');
Route::post('get-users', 'Api\ApiUserController@getusers');
Route::post('get-edit-users', 'Api\ApiUserController@edituser');
Route::post('get-sessions-user', 'Api\ApiUserController@getreportaccess');
Route::post('valid-email', 'Api\ApiUserController@validemail');
Route::post('get-current-user', 'Api\ApiUserController@getCurrentUser');
//Specimens
Route::get('specimens/edit/{id}', 'Api\Specimens\ApiSpecimensController@edit');
Route::post('get-data-select-specimens', 'Api\Specimens\ApiSpecimensController@getdataselects');
Route::post('save-specimens', 'Api\Specimens\ApiSpecimensController@save_specimens');
Route::post('get-specimens', 'Api\Specimens\ApiSpecimensController@get_specimens');
Route::post('get-specimens-id', 'Api\Specimens\ApiSpecimensController@get_specimens_id');
Route::post('validate-plate', 'Api\Specimens\ApiSpecimensController@validateplate');
Route::post('validate-plate-gender', 'Api\Specimens\ApiSpecimensController@validateplategender');
Route::post('delete-specimen', 'Api\Specimens\ApiSpecimensController@delete_specimen');
Route::post('get-color-specimen', 'Api\Specimens\ApiSpecimensController@getcolorspecimen');
Route::post('save-video', 'Api\Specimens\ApiSpecimensController@savevideo');
Route::post('update-video', 'Api\Specimens\ApiSpecimensController@updatevideo');
Route::post('get-videos', 'Api\Specimens\ApiSpecimensController@getvideospecimen');
Route::post('get-video-id', 'Api\Specimens\ApiSpecimensController@getvideoid');
Route::post('delete-video', 'Api\Specimens\ApiSpecimensController@deletevideo');
Route::post('save-color', 'Api\Specimens\ApiSpecimensController@savecolorspecimen');
Route::post('save-amarrador', 'Api\Specimens\ApiSpecimensController@saveamarrador');
Route::post('save-careador', 'Api\Specimens\ApiSpecimensController@savecareador');
Route::post('get-amarrador', 'Api\Specimens\ApiSpecimensController@getamarrador');
Route::post('get-careador', 'Api\Specimens\ApiSpecimensController@getcareador');
Route::post('save-rival', 'Api\Specimens\ApiSpecimensController@saverival');
Route::post('get-rival', 'Api\Specimens\ApiSpecimensController@getrival');
Route::post('search-rival', 'Api\Specimens\ApiSpecimensController@searchRivals');
Route::post('search-tracking', 'Api\Specimens\ApiSpecimensController@searchtracking');
Route::post('specimen/validate', 'Api\Specimens\ApiSpecimensController@validatePlateForDeadSpecimens');
Route::post('specimen/get-videos', 'Api\Specimens\ApiSpecimensController@getSpecimensGrid');
Route::get('specimen/get-gallos', 'Api\Specimens\ApiSpecimensController@getGallos');
Route::get('specimens/get-counter', 'Api\Specimens\ApiSpecimensController@getCounterSpecimens');
Route::post('specimens/update-plate', 'Api\Specimens\ApiSpecimensController@updateBorrowedPlateToPosturaAndMigration');
Route::get('specimen/counter', 'Api\Specimens\ApiSpecimensController@getSpecimenCounters');
//Rutas de posturas

Route::post('postura/save', 'Api\posturas\ApiPosturaController@savePostura');
Route::get('postura/validate-pradrilla-or-madrilla/{madrilla_id}', 'Api\posturas\ApiPosturaController@validateMadrillaOrPadrillo');
Route::post('postura', 'Api\posturas\ApiPosturaController@getPosturas');
Route::get('postura/search/{madrilla_plate}', 'Api\posturas\ApiPosturaController@getPosturasByMadrilla');
Route::post('postura/register-eggs', 'Api\posturas\ApiPosturaController@registerEggsToPostura');
Route::post('postura/edit-eggs', 'Api\posturas\ApiPosturaController@editEggsToPostura');
Route::get('postura/active-madrillas', 'Api\posturas\ApiPosturaController@getActiveMadrillasByActivePosturas');
Route::get('postura/processing-madrillas', 'Api\posturas\ApiPosturaController@getActiveMadrillasByProcessingPosturas');
Route::get('postura/active-padrillos', 'Api\posturas\ApiPosturaController@getActivePadrillos');
Route::post('postura/cancel', 'Api\posturas\ApiPosturaController@cancelPostura');
Route::post('postura/activate', 'Api\posturas\ApiPosturaController@activatePostura');
Route::post('postura/finish', 'Api\posturas\ApiPosturaController@finishPostura');
Route::get('postura/nodrizas', 'Api\posturas\ApiPosturaController@getNodrizas');
Route::post('postura/registered-eggs', 'Api\posturas\ApiPosturaController@getRegisteredEggs');
Route::post('postura/registered-eggs-madrilla', 'Api\posturas\ApiPosturaController@getRegisteredEggsByMadrilla');
Route::post('postura/registered-eggs-madrilla-detail', 'Api\posturas\ApiPosturaController@getRegisteredEggsByMadrillaDetail');
Route::post('postura/registered-eggs-date', 'Api\posturas\ApiPosturaController@getRegisteredEggsByDate');
Route::post('postura/registered-eggs-date-detail', 'Api\posturas\ApiPosturaController@getRegisteredEggsByDateDetail');
Route::post('postura/detail', 'Api\posturas\ApiPosturaController@getPosturaDetail');
Route::get('postura/counters', 'Api\posturas\ApiPosturaController@getCounterByStatus');
Route::post('postura/seenstatus', 'Api\posturas\ApiPosturaController@setStatusSeen');
Route::post('postura/get-grouped-posturas', 'Api\posturas\ApiPosturaController@getGroupedPosturas');
Route::post('postura/get-grouped-postura-detail', 'Api\posturas\ApiPosturaController@getGroupedPosturaDetail');
Route::post('postura/get-grouped-finished-postura-detail', 'Api\posturas\ApiPosturaController@getGroupedFinishedPosturasDetail');
Route::post('postura/get-grouped-postura-chicks-detail', 'Api\posturas\ApiPosturaController@getGroupedPosturaChicksDetail');
Route::post('postura/get-padrillo-report', 'Api\posturas\ApiPosturaController@getPadrilloPosturasReport');
Route::post('postura/get-padrillo-report-detail', 'Api\posturas\ApiPosturaController@getPadrilloPosturasReportDetail');
Route::get('postura/get-madrillas-being-used', 'Api\posturas\ApiPosturaController@getActiveMadrillasBeingUsed');
Route::post('postura/turn-off-madrilla', 'Api\posturas\ApiPosturaController@turnOffMadrillaFromPostura');
Route::post('postura/turn-off-padrillo', 'Api\posturas\ApiPosturaController@turnOffPadrilloFromPostura');
Route::get('postura/get-madrillas-processing', 'Api\posturas\ApiPosturaController@getPosturasProcessingByMadrilla');
Route::get('postura/get-madrillas-for-register-eggs', 'Api\posturas\ApiPosturaController@getPosturasProcessingByMadrillaToRegisterEggs');
Route::post('postura/asign-infertility-responsable', 'Api\posturas\ApiPosturaController@asignInfertileResponsableToPostura');
Route::post('postura/get-by-responsable', 'Api\posturas\ApiPosturaController@getPosturasByResponsable');
Route::post('postura/drop-chickenleg', 'Api\posturas\ApiPosturaController@dropChickenleg');

//Rutas de lote
Route::get('lote/counter', 'Api\lotes\ApiLoteController@getCounterLoteRevision');
Route::post('lote/crear', 'Api\lotes\ApiLoteController@insertLote');
Route::post('lote/get', 'Api\lotes\ApiLoteController@getLotes');
Route::post('lote/get-detail', 'Api\lotes\ApiLoteController@getLoteDetail');
Route::get('lote/get-nacimiento/{lote_id}', 'Api\lotes\ApiLoteController@getCintillosByLotePostura');
Route::get('lote/cintillos', 'Api\lotes\ApiLoteController@getCintillos');
Route::get('lote/chickenlegs', 'Api\lotes\ApiLoteController@getChickenLegs');
Route::get('lote/{lote_id}', 'Api\lotes\ApiLoteController@getLote');
Route::get('lote/get-lote-vivos/{lote_id}', 'Api\lotes\ApiLoteController@getLoteRevisionVivos');
Route::post('lote/infertiles', 'Api\lotes\ApiLoteController@insertInfertileEggs');
Route::post('lote/vivos', 'Api\lotes\ApiLoteController@insertAliveEggs');
Route::post('lote/tracking', 'Api\lotes\ApiLoteController@getTracking');
Route::post('lote/finished-lotes', 'Api\lotes\ApiLoteController@getFinishedLotes');
Route::get('lote/detail-finished-lotes/{lote_id}', 'Api\lotes\ApiLoteController@getDetailFinishedLotes');
Route::post('lote/nacimiento', 'Api\lotes\ApiLoteController@insertCintilloToLotePostura');
Route::post('lote/hatcher', 'Api\lotes\ApiLoteController@confirmHatcherReview');
Route::post('lote/alas', 'Api\lotes\ApiLoteController@insertCintillosAlaChicks');
Route::post('lote/dead-eggs', 'Api\lotes\ApiLoteController@saveDeadChicksEvidence');
Route::post('lote/get-lote-evidence', 'Api\lotes\ApiLoteController@getLoteEvidence');
Route::post('lote/calculate-dead-chicks', 'Api\lotes\ApiLoteController@calculateDeadChicks');
Route::post('lote/delete', 'Api\lotes\ApiLoteController@deleteLote');


//Rutas de notificaciones

Route::post('notification', 'Api\notifications\ApiNotificationController@getNotifications');
Route::post('notification/confirm', 'Api\notifications\ApiNotificationController@confirmNotification');

//Ruta de cintillos

Route::post('cintillo', 'Api\cintillos\ApiCintilloController@getCintillos');
Route::post('cintillo/insert', 'Api\cintillos\ApiCintilloController@insertCintillo');
Route::put('cintillo/update/{cintillo_id}', 'Api\cintillos\ApiCintilloController@updateCintillo');


//Ruta de combinaciones (cintillo patas)

Route::post('cintillo-pata/get', 'Api\cintillos\ApiCintilloPataController@getCintilloPatas');
Route::get('cintillo-pata/get-colour-chickenleg', 'Api\cintillos\ApiCintilloPataController@getCintillosAndChickenlegs');
Route::post('cintillo-pata/save', 'Api\cintillos\ApiCintilloPataController@saveCombination');
Route::get('cintillos-pata/free-combinations', 'Api\cintillos\ApiCintilloPataController@getFreeCombinations');
Route::post('cintillos-pata/free-combinations-postura', 'Api\cintillos\ApiCintilloPataController@getFreeCombinationsByPosturas');
Route::post('cintillo-pata/save-combinations', 'Api\cintillos\ApiCintilloPataController@setCombinationsToPostura');
Route::post('cintillo-pata/delete', 'Api\cintillos\ApiCintilloPataController@deleteCombination');
Route::get('cintillo-pata/get-combinations-used', 'Api\cintillos\ApiCintilloPataController@getCombinationsUsed');

//Ruta de patas

Route::post('chickenleg', 'Api\cintillos\ApiChickenLegController@getChickenLeg');
Route::post('chickenleg/insert', 'Api\cintillos\ApiChickenLegController@insertChickenLeg');
Route::put('chickenleg/update/{chickenleg_id}', 'Api\cintillos\ApiChickenLegController@updateChickenLeg');
Route::get('chickenleg/current-priorities', 'Api\cintillos\ApiChickenLegController@getCurrentPriorities');

//Ruta de huevos

Route::post('egg', 'Api\eggs\ApiEggController@getEggs');
Route::post('egg/detail', 'Api\eggs\ApiEggController@getEggsPostura');
Route::post('egg/detail-madrilla', 'Api\eggs\ApiEggController@getEggsDetail');
Route::post('egg/delete', 'Api\eggs\ApiEggController@deleteEggs');
Route::post('egg/get-egg', 'Api\eggs\ApiEggController@getEgg');

//Ruta de migraciones

Route::post('reproduction/migrate', 'Api\migrations\ApiReproductionMigrationController@save_migration');
Route::post('reproduction/get-migrations', 'Api\migrations\ApiReproductionMigrationController@get_migrations');
Route::post('reproduction/get-migration', 'Api\migrations\ApiReproductionMigrationController@get_migration');
Route::post('reproduction/delete-migration', 'Api\migrations\ApiReproductionMigrationController@delete_migration');
Route::post('reproduction/update-migration', 'Api\migrations\ApiReproductionMigrationController@update_migration');

//Ruta de pollitos

Route::post('chick/save', 'Api\chicks\ApiChickController@saveChicks');
Route::post('chick/validate', 'Api\chicks\ApiChickController@validateCintilloAla');
Route::post('chick/validate-dead-specimen', 'Api\chicks\ApiChickController@validateCintilloAlaForDeadSpecimens');
Route::post('chick/get', 'Api\chicks\ApiChickController@getChicks');
Route::post('chick/save-from-lote', 'Api\chicks\ApiChickController@saveChicksFromRevision');
Route::post('chick/save-dead', 'Api\chicks\ApiChickController@registerDeadEggs');
Route::post('chick/update', 'Api\chicks\ApiChickController@editChick');
Route::post('chick/save-video', 'Api\chicks\ApiChickController@saveVideo');
Route::get('chick/get-machos', 'Api\chicks\ApiChickController@getPollitosMachos');
Route::post('chick/get-videos', 'Api\chicks\ApiChickController@getChicksGrid');
Route::post('chick/validate-chick-upload', 'Api\chicks\ApiChickController@validateCintilloAlaForUpload');
Route::get('gettbl', 'Api\eggs\ApiEggController@getTblProjects');
Route::post('chick/delete-video', 'Api\chicks\ApiChickController@deleteChickVideo');
Route::post('chick/delete', 'Api\chicks\ApiChickController@deleteChick');
Route::post('chick/save-new-dob', 'Api\chicks\ApiChickController@saveNewDobToChick');
Route::post('chick/set-placa', 'Api\chicks\ApiChickController@setPlacaToChickenWhoHasCintillo');
//Ruta de tareas

Route::post('task/save', 'Api\tasks\ApiTaskController@saveTask');
Route::post('task/update', 'Api\tasks\ApiTaskController@updateTask');
Route::post('task/delete', 'Api\tasks\ApiTaskController@deleteTask');
Route::post('task-lote/get', 'Api\tasks\ApiTaskController@getTaskCalendar');
Route::post('task/done-task', 'Api\tasks\ApiTaskController@doneTask');
Route::post('task/get-more-tasks', 'Api\tasks\ApiTaskController@getMoreTasks');
Route::post('task/get', 'Api\tasks\ApiTaskController@getTasks');

//Ruta de muertos

Route::post('deads/get', 'Api\deads\ApiDeadSpecimenController@getDeadSpecimens');
Route::post('deads/save', 'Api\deads\ApiDeadSpecimenController@saveDeadSpecimen');
Route::post('deads/evidence', 'Api\deads\ApiDeadSpecimenController@getDeadSpecimenEvidence');
Route::post('deads/delete', 'Api\deads\ApiDeadSpecimenController@deleteDeadSpecimens');
Route::post('deads/get-lotes-by-combination', 'Api\deads\ApiDeadSpecimenController@getLotesByCombination');

//Ruta de árbol genealogico

Route::post('tree/get', 'Api\tree\ApiTreeController@getSpecimenTree');
Route::post('tree/save', 'Api\tree\ApiTreeController@saveSpecimenParents');
Route::post('tree/delete', 'Api\tree\ApiTreeController@deleteSpecimenTree');
Route::post('tree/validate-specimen', 'Api\tree\ApiTreeController@validateIfSpecimenHasFamilyTree');
Route::post('tree/update', 'Api\tree\ApiTreeController@updateSpecimenParent');

//Ruta de razones

Route::post('reason/get', 'Api\reasons\ApiReasonController@searchReason');
Route::post('reason/save', 'Api\reasons\ApiReasonController@saveReason');
Route::get('modules/get', 'Api\reasons\ApiReasonController@getModules');
Route::post('reason/get-by-module', 'Api\reasons\ApiReasonController@getReasons');
Route::post('reason/delete', 'Api\reasons\ApiReasonController@deleteReason');
Route::post('reason/update', 'Api\reasons\ApiReasonController@updateReason');
Route::post('reason/by-module', 'Api\reasons\ApiReasonController@getReasonsByModule');


//Ruta de administrativo

Route::post('administrative/save', 'Api\administrative\ApiAdministrativeController@saveTransaction');
Route::post('administrative/get-transactions', 'Api\administrative\ApiAdministrativeController@getTransactions');
Route::get('administrative/get-dashboard-data', 'Api\administrative\ApiAdministrativeController@getDashboardData');
Route::post('administrative/delete-transaction', 'Api\administrative\ApiAdministrativeController@deleteTransaction');
Route::post('administrative/get-selected-one', 'Api\administrative\ApiAdministrativeController@getTransaction');


//Ruta de empleados

Route::post('employee/save-employeee', 'Api\administrative\ApiEmployeeController@saveEmployee');
Route::post('employee/get-employees', 'Api\administrative\ApiEmployeeController@getEmployees');
Route::post('employee/get-employee', 'Api\administrative\ApiEmployeeController@getEmployee');
Route::post('employee/delete', 'Api\administrative\ApiEmployeeController@deleteEmployee');

