(function($) {

	"use strict";

	var fullHeight = function() {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
	  $('.navbar').toggleClass('retract');
	  $('.main').toggleClass('join');
      $('#sidebar').toggleClass('active');
	  $('.spanname').toggleClass('programselect')
	  $('.image-list').toggleClass('float-left')


  });

})(jQuery);

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
  })
