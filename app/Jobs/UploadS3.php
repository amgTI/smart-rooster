<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use Illuminate\Support\Facades\Storage;

class UploadS3 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data = [];
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $hour = date('Y-m-d-h-i-s');
            $namevideo = $hour . '.' . $this->data['extension'];
            $namethumb = $hour . '.png';
            $file_path = 'ejemplares/imagesEjemplares/'.$this->data['idspecimen'].'/videos'.'/'. $namevideo; 
            $file_path_thumb = 'ejemplares/imagesEjemplares/'.$this->data['idspecimen'].'/videos'.'/'.$namethumb; 

            Storage::disk('s3')->put($file_path, file_get_contents($this->data['video']));
            Storage::disk('s3')->put($file_path_thumb, file_get_contents($this->data['capturevideo']));

            $statement = 'call save_video(?,?,?,?,?,?,?)';
            $parameter = [
                $this->data['idspecimen'],
                $this->data['iduser'],
                $this->data['type'],
                $this->data['name_video'],
                $this->data['duration'],
                $file_path,
                $file_path_thumb
            ];
                DB::select($statement,$parameter);

        }catch(Exception $e){
            //$error = 'Error de conexion: ' .  $e->getMessage() .  "\n";
            \log::info('fallo el job s3');
            //return  ['status' => 500,'msg' => 'Error de conexion'];
        }
    }
}
