<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;


class RequestUploadVideo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_date' => 'required',
            'type' => 'required',
            'event_result' => 'required_if:type,2'
        ];
    }

//override this method in your FormRequest
    protected function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors(), 201));
    }
}
