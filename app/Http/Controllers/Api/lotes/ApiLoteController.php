<?php

namespace App\Http\Controllers\Api\lotes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiLoteController extends Controller
{
    public function insertLote(Request $request)
    {
        $pl_id = [];
        //dd($request);

        $statement = 'call insert_lote(?,?,?,?,?,?)';
        $parameters = [
            $request->lote['placa_madrilla'],
            $request->lote['total_eggs'],
            $request->lote['date'],
            $request->lote['created_by'],
            $request->lote['status'],
            isset($request->lote['lote_id'])?$request->lote['lote_id']:null
        ];
        $last_id = DB::select($statement, $parameters);
        if ($request->lote['status'] == 2) {
            $statement_tracking = 'insert into tracking_lote (lote_id, user_id, status, description, created_at, updated_at) values(?,?,?,?,?,?)';
            $parameters_tracking = [
                $last_id[0]->last_lote_id,
                $request->lote['created_by'],
                1,
                'Se creó el lote',
                $request->lote['fromMigration'] ? $request->lote['date'] : now(),
                $request->lote['fromMigration'] ? $request->lote['date'] : now()
            ];
            DB::select($statement_tracking, $parameters_tracking);
        }
        $statement_pivote = 'call insert_postura_lote(?,?,?,?,?,?)';
        foreach ($request->posturas as $postura) {
            if ($postura['postura_id']) {
                $parameters_pivote = [
                    $last_id[0]->last_lote_id,
                    $postura['placa_madrilla'],
                    $postura['total_eggs'],
                    $postura['postura_id']['id'],
                    $request->lote['status'],
                    isset($postura['lote_postura_id'])?$postura['lote_postura_id']:null
                ];
                $pl_id = DB::select($statement_pivote, $parameters_pivote);
            }
        }
        $statement_to_delete = 'update lote_posturas set deleted_at = now() where id = ?';

        foreach ((array) $request->posturas_to_delete as $postura_to_delete) {
            $parameter_to_delete = [
                $postura_to_delete
            ];
            DB::select($statement_to_delete, $parameter_to_delete);
        }
        //dd($pl_id);
        return array($pl_id, $last_id);
    }
    public function deleteLote(Request $request)
    {
        $statement = 'call delete_lote(?)';
        $parameter = [
            $request->lote_id
        ];
        $lote_postura_statement = 'call delete_lote_posturas(?,?,?,?,?,?)';
        $data = DB::select($statement, $parameter);
        // dd($data[0]->id);
        foreach ($data as $entry) {
            $lote_postura_paremeters = [
                $entry->lote_id,
                $entry->placa_madrilla,
                $entry->quantity_eggs,
                $entry->postura_id,
                $entry->id,
                $request->status
            ];
            DB::select($lote_postura_statement, $lote_postura_paremeters);
        }
    }
    public function getLotes(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_lotes(?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            8,
            $request->campo,
            $request->date
        ];
        $data =  DB::select($statement, $parameter);
        if (count($data)) {
            $totalCount = $data[0]->count_lotes;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getLoteDetail(Request $request)
    {
        $statement = 'call get_posturas_lote(?)';
        $parameter = [
            $request->lote_id
        ];
        $data =  DB::select($statement, $parameter);
        return $data;
    }
    public function getLote($lote_id)
    {
        $statement = 'call get_lote(?)';
        $parameter = [
            $lote_id
        ];
        $data = DB::select($statement, $parameter);
        foreach ($data as $entry) {
            $entry->cintillos_alas = json_decode($entry->cintillos_alas);

            if ($entry->evidence) {
                $entry->evidence = json_decode($entry->evidence);
                foreach ($entry->evidence as $evidence) {
                    if ($evidence->url != null) {
                        $url = Storage::temporaryUrl(
                            $evidence->url,
                            now()->addMinutes(120)
                        );
                        $evidence->url = $url;
                    }
                }
            }
        }
        return $data;
    }
    public function getLoteRevisionVivos($lote_id)
    {
        $statement = 'call get_lote(?)';
        $parameter = [
            $lote_id
        ];
        $data = DB::select($statement, $parameter);
        $array = [];
        foreach ($data as $entry) {
            $entry->cintillos_alas = json_decode($entry->cintillos_alas);

            if ($entry->evidence) {
                $entry->evidence = json_decode($entry->evidence);
                foreach ($entry->evidence as $evidence) {
                    if ($evidence->url != null) {
                        $url = Storage::temporaryUrl(
                            $evidence->url,
                            now()->addMinutes(120)
                        );
                        $evidence->url = $url;
                    }
                }
            }
            if ($entry->total_minus_infertile_eggs > 0) {
                $array[] = $entry;
            }
        }
        $data = $array;

        return $data;
    }
    public function insertInfertileEggs(Request $request)
    {
        DB::transaction(function () use ($request) {

            $statement = 'update lotes set total_infertile_eggs = ?, status = ? where id = ?';
            $parameters = [
                $request->lote['total_eggs'],
                $request->status,
                $request->lote['lote_id']
            ];
            DB::select($statement, $parameters);
            if ($request->status == 10) {
                $statement_tracking = 'insert into tracking_lote (lote_id, user_id, status, description, created_at, updated_at) values(?,?,?,?,?,?)';
                $parameters_tracking = [
                    $request->lote['lote_id'],
                    $request->lote['created_by'],
                    2,
                    'Se hizo la revisión de infértiles',
                    $request->lote['infertile_revision_date'] ? $request->lote['infertile_revision_date'] : now(),
                    $request->lote['infertile_revision_date'] ? $request->lote['infertile_revision_date'] : now()
                ];
                DB::select($statement_tracking, $parameters_tracking);
            }
            $statement_pivote = 'call insert_infertile_eggs(?,?)';
            foreach ($request->posturas as $postura) {
                $parameters_pivote = [
                    $postura['lote_postura_id'],
                    $postura['number_infertile_eggs']?$postura['number_infertile_eggs']:0
                ];
                DB::select($statement_pivote, $parameters_pivote);
            }
        });
    }
    public function confirmHatcherReview(Request $request)
    {
        $statement = 'update lotes set status = ? where id = ?';
        $parameters = [
            4,
            $request->lote['lote_id']
        ];
        DB::select($statement, $parameters);
        $statement_tracking = 'insert into tracking_lote (lote_id, user_id, status, description, created_at, updated_at) values(?,?,?,?,?,?)';
        $parameters_tracking = [
            $request->lote['lote_id'],
            $request->lote['created_by'],
            5,
            'Nacedora completada',
            $request->lote['hatcher_revision_date'] ? $request->lote['hatcher_revision_date'] : now(),
            $request->lote['hatcher_revision_date'] ? $request->lote['hatcher_revision_date'] : now()
        ];
        DB::select($statement_tracking, $parameters_tracking);
    }
    public function insertAliveEggs(Request $request)
    {
        // dd($request->all());
        DB::transaction(function () use ($request) {

            $statement = 'update lotes set total_born_eggs = ?, total_dead_eggs = ?, status = ? where id = ?';
            $parameters = [
                $request->lote['total_alive_eggs'],
                $request->lote['total_dead_eggs'],
                $request->status,
                $request->lote['lote_id']
            ];
            DB::select($statement, $parameters);

            if ($request->status == 11) {
                $statement_tracking = 'insert into tracking_lote (lote_id, user_id, status, description, created_at, updated_at) values(?,?,?,?,?,?)';
                $parameters_tracking = [
                    $request->lote['lote_id'],
                    $request->lote['created_by'],
                    3,
                    'Se hizo la revisión de pollos vivos',
                    $request->lote['born_date'] ? $request->lote['born_date'] : now(),
                    $request->lote['born_date'] ? $request->lote['born_date'] : now()
                ];
                DB::select($statement_tracking, $parameters_tracking);
                $statement_dos = 'select * from tasks where module = 1 and deleted_at is null';
                $tasks = DB::select($statement_dos);
                $statement_insert_task = 'call save_lote_task(?,?,?,?,?)';
                foreach ($tasks as $task) {
                    $parameters_insert = [
                        $task->id,
                        $request->lote['lote_id'],
                        1,
                        $request->lote['born_date'] ? $request->lote['born_date'] : now(),
                        $task->day
                    ];
                    DB::select($statement_insert_task, $parameters_insert);
                }
            }

            $statement_pivote = 'call insert_alive_eggs(?,?,?,?,?)';
            foreach ($request->posturas as $postura) {
                $parameters_pivote = [
                    $postura['lote_postura_id'],
                    $postura['number_alive_eggs']?$postura['number_alive_eggs']:0,
                    $postura['number_dead_eggs']?$postura['number_dead_eggs']:0,
                    null,
                    null
                ];
                DB::select($statement_pivote, $parameters_pivote);
            }
        });
    }
    public function insertCintillosAlaChicks(Request $request)
    {
        $statement = 'update lotes set status = ? where id = ?';
        $parameters = [
            $request->status,
            $request->lote['lote_id']
        ];
        DB::select($statement, $parameters);

        if ($request->status == 12) {
            $statement_tracking = 'insert into tracking_lote (lote_id, user_id, status, description, created_at, updated_at) values(?,?,?,?,?,?)';
            $parameters_tracking = [
                $request->lote['lote_id'],
                $request->lote['created_by'],
                6,
                'Se le ha puesto cintillo en el ala a todos los pollos',
                now(),
                now()
                // $request->lote['born_date']?$request->lote['born_date']:now(),
                // $request->lote['born_date']?$request->lote['born_date']:now()
            ];
            DB::select($statement_tracking, $parameters_tracking);
        }
        $statement = 'insert into chicks (cintillo_ala, lote_postura_id, gender, status, created_by, created_at, updated_at, date_of_birth) values (?,?,?,?,?,now(),now(),?)';
        $statement_lotes = 'update lote_posturas set cintillo_ala_status = 2 where id = ?';
        $statement_dob = 'select date(created_at) correct_date from tracking_lote tl where tl.lote_id = ? and tl.status = 3;';
        $parameters_dob = [
            $request->lote['lote_id']
        ];
        $dob = DB::select($statement_dob, $parameters_dob);
        foreach ($request->posturas as $postura) {
            foreach ($postura['cintillos_alas'] as $chick) {
                if ($chick['cintillo_ala'] != null && $chick['gender'] != null) {
                    $parameters = [
                        $chick['cintillo_ala'],
                        $postura['lote_postura_id'],
                        $chick['gender'],
                        1,
                        $request->lote['created_by'],
                        $dob[0]->correct_date
                    ];
                    DB::select($statement, $parameters);
                    $parameter_lote = [
                        $postura['lote_postura_id']
                    ];
                    DB::select($statement_lotes, $parameter_lote);
                }
            }
        }
    }
    public function getTracking(Request $request)
    {
        $statement = 'select tl.*, tl.status+0 status_number, u.name u_name, u.last_name u_last_name from tracking_lote tl join users u on u.id = tl.user_id where tl.lote_id = ? order by tl.created_at desc';
        $parameters = [
            $request->lote_id
        ];
        return DB::select($statement, $parameters);
    }
    public function getCounterLoteRevision()
    {
        $statement = 'call get_counter_lote_revision_new()';
        $data = DB::select($statement);
        $cont = 0;
        foreach ($data as $lote) {
            if(($lote->status_number == 2 || $lote->status_number == 3) && Carbon::now()->format('Y-m-d') >= $lote->infertile_revision_date){
                $cont++;
            }else if($lote->status_number == 10 && Carbon::now()->format('Y-m-d') >= $lote->hatcher_revision_date){
                $cont++;
            }else if($lote->status_number <6 && $lote->status_number > 3 && Carbon::now()->format('Y-m-d') >= $lote->alive_revision_date){
                $cont++;
            }else if($lote->status_number <12 && $lote->status_number > 6 && Carbon::now()->format('Y-m-d') >= $lote->cintillo_ala_revision_date){
                $cont++;
            }
        }
        return $cont;
    }
    public function getFinishedLotes(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_finished_lotes(?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement, $parameter);
        if (count($data)) {
            $totalCount = $data[0]->count_lotes;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getDetailFinishedLotes($lote_id)
    {
        $statement = 'select
        lp.id, lp.lote_id, lp.postura_id, lp.quantity_eggs, lp.quantity_infertile_eggs, lp.quantity_born_eggs, lp.quantity_dead_eggs, lp.cintillo_id cintillo,
                    p.placa_madrilla,
                    s.alias madrilla_alias
                from
                    lote_posturas lp
                join posturas p on
                    p.id = lp.postura_id
                join specimens s on
                    s.plate = p.placa_madrilla
                where
                    lp.lote_id = ?';
        $parameter = [
            $lote_id
        ];
        return DB::select($statement, $parameter);
    }
    public function getCintillos()
    {
        $statement = 'select * from cintillos';
        return DB::select($statement);
    }
    public function getChickenLegs()
    {
        $statement = 'select * from chickenlegs';
        return DB::select($statement);
    }
    public function insertCintilloToLotePostura(Request $request)
    {
        $statement_status = 'update lotes set status = ? where id = ?';
        $parameters_status = [
            $request->status,
            $request->lote_id
        ];
        DB::select($statement_status, $parameters_status);
        $statement = 'update lote_posturas set cintillo_id = ? where id = ?';
        foreach ($request->lotes as $lote) {
            $parameters = [
                $lote['cintillo'],
                $lote['id']
            ];
            DB::select($statement, $parameters);
        }
        $statement_notification = "insert
                                into
                                notifications (status,
                                notification,
                                link,
                                role_id,
                                delivery_notificaction_date,
                                module,
                                type,
                                created_at,
                                updated_at)
                            values (0,
                            concat('Los pollos del lote', ' ', ? , ' ',  'han nacido'),
                            '/main/reproduccion/nacimiento-pollos',
                            1,
                            now(),
                            'Lote',
                            1,
                            now(),
                            now())";
        $parameters_notificaction = [
            $request->lote_id
        ];
        DB::select($statement_notification, $parameters_notificaction);

        if ($request->status == 8) {
            $statement_tracking = 'insert into tracking_lote (lote_id, user_id, status, description, created_at, updated_at) values(?,?,?,?,?,?)';
            $parameters_tracking = [
                $request->lote_id,
                $request->created_by,
                4,
                'Se le puso cintillos a los pollos nacidos',
                now(),
                now()
            ];
            DB::select($statement_tracking, $parameters_tracking);
        }
    }
    public function getCintillosByLotePostura($lote_id)
    {
        $statement = 'select id, lote_id, postura_id, quantity_eggs, quantity_infertile_eggs, quantity_born_eggs, quantity_dead_eggs, cintillo_id cintillo from lote_posturas where lote_id = ?';
        $parameter = [
            $lote_id
        ];
        return DB::select($statement, $parameter);
    }
    public function saveDeadChicksEvidence(Request $request)
    {
        if ($request->file != null || $request->file != '') {
            $ext = $request->file->getClientOriginalExtension();
            $original_name = $request->file->getClientOriginalName();
            // $cont = explode(".",$request->file);
            // $name = 'avatar'.$ext;
            // $name_thumb = 'thumb.'.$cont[1];
        }
        $name_folder = "lotes";
        $folder_lote = "deadChicks";
        $name_folder_lotes_id = $request->lote_postura_id;
        if ($request->file != '' && $request->file != null) {
            $file_path = $name_folder . '/' . $folder_lote . '/' . $name_folder_lotes_id . '/' . $original_name;
            // $file_path_thumb = $name_folder.'/'.$folder_lote.'/'.$name_folder_lotes_id.'/'.$name_thumb;

            Storage::disk('s3')->put($file_path, file_get_contents($request->file));
            // Storage::disk('s3')->put($file_path_thumb, file_get_contents($request->thumb));
        }
        $statement = 'insert into dead_chick_lotes (name, url, quantity, lote_postura_id, created_at, updated_at) values (?,?,?,?,?,?)';
        $parameters = [
            $original_name,
            $file_path,
            $request->quantity,
            $request->lote_postura_id,
            now(),
            now()
        ];
        DB::select($statement, $parameters);
    }
    public function calculateDeadChicks(Request $request)
    {
        $statement = 'call calculate_dead_chicks(?,?)';
        foreach ($request->lote_posturas as $lote_postura) {
            if ($lote_postura['quantity']) {
                $parameters = [
                    $lote_postura['lote_postura_id'],
                    $lote_postura['quantity']
                ];
                DB::select($statement, $parameters);
            }
        }
    }
    public function getLoteEvidence(Request $request)
    {
        $statement = 'call get_lote_dead_eggs_evidence(?)';
        $parameter = [
            $request->lote_id
        ];
        $data = DB::select($statement, $parameter);
        foreach ($data as $entry) {
            $entry->evidence = json_decode($entry->evidence);
            foreach ($entry->evidence as $evidence) {
                if ($evidence->url != null) {
                    $url = Storage::temporaryUrl(
                        $evidence->url,
                        now()->addMinutes(120)
                    );
                    $evidence->url = $url;
                }
            }
        }
        return $data;
    }
}
