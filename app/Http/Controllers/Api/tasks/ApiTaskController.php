<?php

namespace App\Http\Controllers\Api\tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiTaskController extends Controller
{
    public function saveTask(Request $request){
        $statement = 'insert into tasks (task, day, module, created_by,created_at,updated_at) values (?,?,?,?,now(),now())';
        $parameters = [
            $request->task,
            $request->day,
            $request->module,
            $request->created_by
        ];
        DB::select($statement, $parameters);
    }
    public function updateTask(Request $request)
    {
        $statement = 'update tasks set task = ?, day = ? where id = ?';
        $parameters = [
            $request->task,
            $request->day,
            $request->task_id
        ];
        DB::select($statement,$parameters);
    }
    public function deleteTask(Request $request)
    {
        $statement = 'update tasks set deleted_at = now(), deleted_by = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->task_id
        ];
        DB::select($statement, $parameters);
    }
    public function getTaskCalendar(Request $request)
    {
        $statement = 'select
        t.task task,
        lt.id `key`,
        l.id lote_id,
        lt.status + 0 status_text,
        lt.delivery_date dates,
        (
        select
            json_object("title", t.task, "status", lt2.status+0, "id", lt2.id, "year", year(lt2.delivery_date), "month", month(lt2.delivery_date), "lote_id", lt2.lote_id
            )
        from
            lote_tasks lt2
            join tasks t on t.id = lt2.task_id
        where lt2.id = lt.id
            ) customData
    from
        lote_tasks lt
    join tasks t on
        t.id = lt.task_id
    join lotes l on
        l.id = lt.lote_id where YEAR(lt.delivery_date) = ? and MONTH(lt.delivery_date) = ? and t.deleted_at is null;';
            $parameters = [
                $request->year,
                $request->month
            ];
        $data = DB::select($statement, $parameters);
        foreach($data as $entry)
        {
            $entry->customData = json_decode($entry->customData);
        }
        return $data;
    }
    public function doneTask(Request $request)
    {
        $statement = 'update lote_tasks set status = 2, done_at = now(), done_by = ? where id = ?';
        $parameter = [
            $request->done_by,
            $request->lote_task_id
        ];
        DB::select($statement, $parameter);
    }
    public function getMoreTasks(Request $request)
    {
        $statement = 'select lt.*,lt.status+0 status_number, t.task task from lote_tasks lt join tasks t on t.id = lt.task_id  where delivery_date = ? and t.deleted_at is null;';
        $parameter = [
            $request->delivery_date
        ];
        $lote_tasks = DB::select($statement, $parameter);
        return $lote_tasks;
    }
    public function getTasks(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_tasks_table(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            'desc',
            2,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
}
