<?php

namespace App\Http\Controllers\Api\notifications;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiNotificationController extends Controller
{
    public function getNotifications(Request $request)
    {
        $statement = 'select * from notifications where delivery_notificaction_date <= ? and role_id = ? order by created_at desc limit 20';
        $parameters = [
            now(),
            $request->role_id
        ];
        return DB::select($statement, $parameters);
    }
    public function confirmNotification(Request $request)
    {
        $statement = 'update notifications set status = 1 where id = ?';
        $parameters = [
            $request->notification_id
        ];
        DB::select($statement, $parameters);
    }
}
