<?php

namespace App\Http\Controllers\Api\posturas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiPosturaController extends Controller
{

    public function savePostura(Request $request)
    {
        $statement = 'call save_postura(?,?,?,?,?,?,?)';
        $parameter = [
            $request->placa_madrilla,
            $request->placa_padrillo,
            $request->observation,
            $request->created_by,
            $request->postura_date,
            $request->method,
            $request->method ? 3 : 2
        ];
        $postura_id = DB::select($statement, $parameter);
        return $postura_id;
    }
    public function validateMadrillaOrPadrillo($placa_madrilla)
    {
        $statement = 'call validate_plaque_padrillo_madrilla(?)';
        $parameter = [
            $placa_madrilla
        ];
        $data =  DB::select($statement, $parameter);
        return $data;
    }
    public function getPosturas(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_posturas(?,?,?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            5,
            $request->campo,
            $request->status,
            $request->specimen,
            $request->category_specimen
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
            if ($photo->padrillo_photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->padrillo_photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->padrillo_thumb,
                    now()->addMinutes(120)
                );
                $photo->padrillo_photo = $url;
                $photo->padrillo_thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getPosturasByResponsable(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_posturas_by_responsable(?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            5,
            $request->campo,
            $request->specimen
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
            if ($photo->padrillo_photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->padrillo_photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->padrillo_thumb,
                    now()->addMinutes(120)
                );
                $photo->padrillo_photo = $url;
                $photo->padrillo_thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getPadrilloPosturasReportDetail(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 10;
        $statement = 'call get_posturas_by_padrillo(?,?,?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'asc',
            1,
            $request->campo,
            $request->status,
            $request->specimen,
            $request->madrilla_status
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getPadrilloPosturasReport(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 10;
        $statement = 'call get_padrillo_postura_report(?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'asc',
            1,
            $request->campo
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            $photo->counters = json_decode($photo->counters);
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getGroupedPosturas(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 10;
        $statement = 'call get_grouped_posturas(?,?,?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'asc',
            1,
            $request->campo,
            $request->status,
            $request->specimen,
            $request->category_specimen
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getGroupedFinishedPosturasDetail(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_grouped_finished_postura_detail(?,?,?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            3,
            $request->campo,
            $request->madrilla_plate,
            $request->padrillo_plate,
            $request->status
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $one) {
            $one->json_data = json_decode($one->json_data);
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getGroupedPosturaDetail(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_grouped_postura_detail(?,?,?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            3,
            $request->campo,
            $request->madrilla_plate,
            $request->padrillo_plate,
            $request->status
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $one) {
            $one->json_data = json_decode($one->json_data);
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getGroupedPosturaChicksDetail(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_grouped_postura_chicks_detail(?,?,?,?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            3,
            $request->campo,
            $request->madrilla_plate,
            $request->padrillo_plate,
            $request->status,
            $request->gender
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $one) {
            $one->json_data = json_decode($one->json_data);
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getPosturasByMadrilla($plate_madrilla)
    {
        $statement = 'call search_posturas_by_madrilla(?)';
        $parameter = [
            $plate_madrilla
        ];
        $data =  DB::select($statement, $parameter);
        return $data;
    }
    public function registerEggsToPostura(Request $request)
    {

        $statement_header = 'call insert_eggs(?,?,?,?)';
        $parameters_header = [
            $request->register_eggs_comment,
            $request->register_eggs_date,
            $request->created_by,
            $request->total_register_eggs
        ];
        $last_egg_id = DB::select($statement_header, $parameters_header);
        $statement = 'call register_eggs_to_postura(?,?,?,?,?)';
        foreach ($request->posturas as $postura) {
            if ($postura['postura_id']) {
                $parameter = [
                    $last_egg_id[0]->last_egg_id,
                    $postura['placa_madrilla'],
                    $postura['number_eggs'],
                    $postura['postura_id']['id'],
                    isset($postura['id'])?$postura['id']:null
                ];
                DB::select($statement, $parameter);
            }
        }
    }
    public function editEggsToPostura(Request $request)
    {
        // $statement_header = 'call insert_eggs(?,?,?,?)';
        // $parameters_header = [
        //     $request->register_eggs_comment,
        //     $request->register_eggs_date,
        //     $request->created_by,
        //     $request->total_register_eggs
        // ];
        // $last_egg_id = DB::select($statement_header, $parameters_header);
        $statement_update = 'update eggs set description = ?, egg_postura_date = ?, total_eggs_quantity = ? where id = ?';
        $parameters_update = [
            $request->register_eggs_comment,
            $request->register_eggs_date,
            $request->total_register_eggs,
            $request->egg_id
        ];
        DB::select($statement_update, $parameters_update);
        $statement = 'call register_eggs_to_postura(?,?,?,?,?)';
        foreach ($request->posturas as $postura) {
            if ($postura['postura_id']) {
                $parameter = [
                    null,
                    $postura['placa_madrilla'],
                    $postura['number_eggs'],
                    $postura['postura_id']['id'],
                    $postura['id']
                ];
                DB::select($statement, $parameter);
            }
        }
    }
    public function getActiveMadrillasByActivePosturas()
    {
        $statement = 'call get_active_madrillas_by_active_posturas()';
        $madrillas = DB::select($statement);
        return $madrillas;
    }
    public function getActiveMadrillasByProcessingPosturas()
    {
        $statement = 'call get_madrillas_by_processing_posturas()';
        $madrillas = DB::select($statement);
        return $madrillas;
    }
    public function getActivePadrillos()
    {
        $statement = 'call get_active_padrillos()';
        $madrillas = DB::select($statement);
        return $madrillas;
    }
    public function activatePostura(Request $request)
    {
        $statement = 'call activate_postura(?,?,?)';
        $parameters = [
            $request->postura_id,
            $request->postura_comment,
            $request->method
        ];
        DB::select($statement, $parameters);
        $statement_notification = "insert
                                into
                                notifications (status,
                                notification,
                                link,
                                role_id,
                                delivery_notificaction_date,
                                module,
                                type,
                                created_at,
                                updated_at)
                            values (0,
                            concat('La postura', ' ', ? , ' ', 'ha sido activada'),
                            '/main/reproduccion/planificacion-camadas',
                            6,
                            now(),
                            'Postura',
                            1,
                            now(),
                            now())";
        $parameters_notificaction = [
            $request->postura_id
        ];
        DB::select($statement_notification, $parameters_notificaction);
    }
    public function getPosturasProcessingByMadrilla()
    {
        $statement = 'call get_posturas_processing_madrillas()';
        $data = DB::select($statement);
        return $data;
    }
    public function getPosturasProcessingByMadrillaToRegisterEggs()
    {
        $statement = 'select
		p.*,
		s.alias madrilla_alias
	from
		posturas p
	join specimens s on
		s.plate = p.placa_madrilla
	where
		p.status = 3 and p.madrilla_status = 1 order by s.plate*1 asc';
        $data = DB::select($statement);
        return $data;
    }
    public function cancelPostura(Request $request)
    {
        $statement = 'update posturas set status = 1, cancel_comment = ?, deleted_by = ?, deleted_at = now(), seen_status = 1 where id = ?;';
        $statement_padrillo = 'update specimens set is_being_used = 0 where plate = ?';
        $parameter_padrillo = [
            $request->placa_padrillo
        ];
        $statement_madrilla = 'update specimens set is_being_used = 0 where plate = ?';
        $parameter_madrilla = [
            $request->placa_madrilla
        ];
        $parameters = [
            $request->postura_comment,
            $request->user_id,
            $request->postura_id
        ];
        DB::select($statement, $parameters);
        DB::select($statement_padrillo, $parameter_padrillo);
        DB::select($statement_madrilla, $parameter_madrilla);
        $statement_notification = "insert
                                into
                                notifications (status,
                                notification,
                                link,
                                role_id,
                                delivery_notificaction_date,
                                module,
                                type,
                                created_at,
                                updated_at)
                            values (0,
                            concat('La postura', ' ', ? , ' ', 'ha sido anulada'),
                            '/main/reproduccion/planificacion-camadas',
                            1,
                            now(),
                            'Postura',
                            1,
                            now(),
                            now())";
        $parameters_notificaction = [
            $request->postura_id
        ];
        DB::select($statement_notification, $parameters_notificaction);
    }
    public function finishPostura(Request $request)
    {
        $statement = 'call finish_postura(?,?,?)';
        $parameters = [
            $request->postura_id,
            $request->postura_comment,
            $request->finished_by
        ];
        DB::select($statement, $parameters);
        // $cintillo_pata_id_header = 'select cintillo_pata_id from cintillo_pata_posturas where postura_id = ?';
        // $cintillo_pata_id_param = [
        //     $request->postura_id
        // ];
        // $cintillo_pata_id = DB::select($cintillo_pata_id_header, $cintillo_pata_id_param);



        // if (sizeof($cintillo_pata_id) > 0) {
        //     $liberar_cintillo_sql = 'update cintillo_patas set status = 1 where id = ?';
        //     $liberar_cintillo_sql_param = [
        //         $cintillo_pata_id[0]->cintillo_pata_id
        //     ];
        //     DB::select($liberar_cintillo_sql, $liberar_cintillo_sql_param);
        // }
    }
    public function getNodrizas()
    {
        $statement = 'select * from specimens where status = 1 and category = 2';
        return DB::select($statement);
    }
    public function getRegisteredEggs(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_registered_eggs(?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement, $parameter);
        if (count($data)) {
            $totalCount = $data[0]->count_registered_eggs;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getRegisteredEggsByMadrilla(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_registered_eggs_madrilla(?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'asc',
            3,
            $request->campo
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->count_registered_eggs_madrilla;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getRegisteredEggsByDate(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_registered_eggs_date(?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            2,
            $request->campo
        ];
        $data =  DB::select($statement, $parameter);
        if (count($data)) {
            $totalCount = $data[0]->count_registered_eggs_date;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getRegisteredEggsByMadrillaDetail(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_registered_eggs_madrilla_detail(?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            3,
            $request->campo,
            $request->madrilla_plate
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $one) {
            $one->json_data = json_decode($one->json_data);
        }
        if (count($data)) {
            $totalCount = $data[0]->count_registered_eggs_madrilla;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getRegisteredEggsByDateDetail(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_registered_eggs_date_detail(?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'asc',
            2,
            $request->campo,
            $request->register_egg_date
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->count_registered_eggs_date;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getPosturaDetail(Request $request)
    {
        $first_statement = 'call get_postura_detail(?)';
        $first_parameters = [
            $request->postura_id
        ];
        $postura_eggs_detail = DB::select($first_statement, $first_parameters);
        $postura_eggs_detail[0]->postura_pollitos = json_decode($postura_eggs_detail[0]->postura_pollitos);
        $second_statement = 'select lp.*, l.code, lp.cintillo_ala_status+0 c_ala_status from lote_posturas lp join lotes l on l.id = lp.lote_id where lp.postura_id = ? and lp.deleted_at is null';
        $second_parameters = [
            $request->postura_id
        ];
        $postura_lote_detail = DB::select($second_statement, $second_parameters);

        return array($postura_eggs_detail, $postura_lote_detail);
    }
    public function getCounterByStatus()
    {
        $statement = 'call get_count_by_postura_status()';
        return DB::select($statement);
    }
    public function setStatusSeen(Request $request)
    {
        $statement = 'update posturas set seen_status = 2 where status = ?';
        $parameter = [
            $request->status_postura
        ];
        DB::select($statement, $parameter);
    }
    public function getActiveMadrillasBeingUsed()
    {
        $statement = 'select
                        s.id,
                        s.alias,
                        s.plate
                    from
                        specimens s
                    where
                        s.category = 2
                        and ( s.status in (1, 4, 5)
                        and s.is_being_used = 0 )
                    order by
                        s.plate*1 asc';
        $data = DB::select($statement);
        return $data;
    }
    public function turnOffMadrillaFromPostura(Request $request)
    {
        $statement = 'call turn_off_madrilla(?,?)';
        $parameter = [
            $request->plate,
            $request->turn_off_reason
        ];
        DB::select($statement, $parameter);
    }
    public function turnOffPadrilloFromPostura(Request $request)
    {
        $statement = 'call turn_off_padrillo(?,?,?)';
        $parameter = [
            $request->plate,
            $request->turn_off_reason,
            $request->postura_id
        ];
        DB::select($statement, $parameter);
    }
    public function asignInfertileResponsableToPostura(Request $request)
    {
        $statement = 'update posturas set res_infertility = ?, infertility_obs = ?, res_infertility_at = now(), res_infertility_by = ? where id = ?';
        $parameters = [
            $request->responsable,
            $request->infertility_observation,
            $request->user_id,
            $request->postura_id
        ];
        DB::select($statement, $parameters);
    }
    public function dropChickenleg(Request $request)
    {
        $statement = 'call drop_chicken_leg_from_postura(?)';
        $parameter = [
            $request->postura_id
        ];
        DB::select($statement, $parameter);
    }
}
