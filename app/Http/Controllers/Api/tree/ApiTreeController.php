<?php

namespace App\Http\Controllers\Api\tree;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiTreeController extends Controller
{
    public function saveSpecimenParents(Request $request)
    {
        if ($request->parent_id == null) {
            $statement = 'call save_specimen_tree(?,?,?,?,?,?,?)';
            $parameters = [
                $request->parent_id,
                $request->specimen_id,
                $request->specimen_alias,
                $request->specimen_plate,
                $request->specimen_dob,
                1,
                $request->created_by
            ];
            $specimen = DB::select($statement, $parameters);
        }
        $statement = 'insert into specimen_family_tree (parent_id, specimen_id, alias, plate, dob, type, created_by,created_at,updated_at, observation) values (?,?,?,?,?,?,?,?,?,?)';
        $parameters = [
            $request->parent_id?$request->parent_id:$specimen[0]->first_specimen,
            $request->specimen_id,
            $request->mom_alias,
            $request->mom_plate,
            $request->mom_dob,
            2,
            $request->created_by,
            now(),
            now(),
            $request->mom_description
        ];
        DB::select($statement, $parameters);
        $statement = 'insert into specimen_family_tree (parent_id, specimen_id, alias, plate, dob, type, created_by,created_at,updated_at, observation) values (?,?,?,?,?,?,?,?,?,?)';
        $parameters = [
            $request->parent_id?$request->parent_id:$specimen[0]->first_specimen,
            $request->specimen_id,
            $request->dad_alias,
            $request->dad_plate,
            $request->dad_dob,
            3,
            $request->created_by,
            now(),
            now(),
            $request->dad_description
        ];
        DB::select($statement, $parameters);
    }
    public function updateSpecimenParent(Request $request)
    {
        $statement = 'update specimen_family_tree set alias = ?, plate = ?, dob = ?, observation = ? where id = ?';
        $parameters = [
            $request->alias,
            $request->plate,
            $request->dob,
            $request->observation,
            $request->specimen_id
        ];
        DB::select($statement, $parameters);
    }
    public function getSpecimenTree(Request $request)
    {
        $statement = 'select * from specimen_family_tree where specimen_id = ? and deleted_at is null order by type desc';
        $parameter = [
            $request->specimen_id
        ];
        $data = DB::select($statement, $parameter);
        $tree = $this->buildTree($data);
        // dd($tree);
        return $tree;
    }
    public function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            // dd($element);
            if ($element->parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
    public function deleteSpecimenTree(Request $request)
    {
        $statement = 'update specimen_family_tree set deleted_by = ?, deleted_at = now() where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->tree_item_id
        ];
        DB::select($statement, $parameters);
    }
    public function validateIfSpecimenHasFamilyTree(Request $request)
    {
        $statement = 'select * from specimen_family_tree where specimen_id = ? and parent_id = 0';
        $parameter = [
            $request->specimen_id
        ];
        $specimen = DB::select($statement, $parameter);
        return $specimen;
    }
}
