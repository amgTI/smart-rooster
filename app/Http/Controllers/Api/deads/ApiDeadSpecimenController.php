<?php

namespace App\Http\Controllers\Api\deads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ApiDeadSpecimenController extends Controller
{
    public function saveDeadSpecimen(Request $request)
    {

        $request->header = json_decode($request->header);
        foreach ($request->header as $dead) {
            if ($dead->id) {
                $statement = 'call save_dead_specimens(?,?,?,?,?,?,?,?,?)';
                if ($dead->type == 1) {
                    $parameters = [
                        $dead->id->id,
                        null,
                        null,
                        1,
                        $dead->type,
                        $request->created_by,
                        $dead->observation->id,
                        $dead->circumstances,
                        $dead->breeder_id
                    ];
                } else if ($dead->type == 2) {
                    $parameters = [
                        null,
                        $dead->id,
                        null,
                        1,
                        $dead->type,
                        $request->created_by,
                        $dead->observation->id,
                        $dead->circumstances,
                        $dead->breeder_id
                    ];
                } else {
                    $parameters = [
                        null,
                        null,
                        $dead->id,
                        1,
                        $dead->type,
                        $request->created_by,
                        $dead->observation->id,
                        $dead->circumstances,
                        $dead->breeder_id
                    ];
                }
                $dead_specimen = DB::select($statement, $parameters);
                // dd(json_encode($dead->customFormData));



                if ($dead->customFormData != null) {

                    foreach ($dead->customFormData as $file) {
                        $image_64 = $file->base;
                        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
                        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                        $image = str_replace($replace, '', $image_64);
                        $image = str_replace(' ', '+', $image);
                        $filePath = 'deads/' . $dead->type . '/' . $file->name;
                        Storage::disk('s3')->put($filePath, base64_decode($image));
                        Storage::disk('public')->put($filePath, base64_decode($image));
                        $statement = 'call save_dead_specimen_files(?,?,?,?,?,?)';
                        $parameter = [
                            $dead_specimen[0]->dead_specimen_id,
                            $file->name,
                            $filePath,
                            $file->size,
                            $extension,
                            $request->created_by
                        ];
                        DB::select($statement, $parameter);
                    }
                }
                // dd($dead->id->id);
                if ($dead->type == 1) {
                    $statement_calculate = 'call save_dead_cintillo_pata(?,?,?,?,?,?)';
                    $parameters = [
                        $dead->id->id,
                        1,
                        1,
                        $dead_specimen[0]->dead_specimen_id,
                        $dead->postura_id,
                        $dead->lote_id
                    ];
                    DB::select($statement_calculate, $parameters);
                } else if ($dead->type == 2) {
                    $statement_calculate = 'call save_dead_cintillo_ala(?,?,?,?)';
                    $parameters = [
                        $dead->id,
                        1,
                        1,
                        $dead_specimen[0]->dead_specimen_id
                    ];
                    DB::select($statement_calculate, $parameters);
                } else if ($dead->type == 3) {
                    $statement_specimen_muerto = 'update specimens set status = 3 where plate = ?';
                    $parameter = [
                        $dead->id
                    ];
                    DB::select($statement_specimen_muerto, $parameter);
                }
            }
        }
    }
    public function getDeadSpecimens(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 10;
        $statement = 'call get_dead_specimens(?,?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            1,
            $request->campo,
            $request->type_specimen
        ];
        $data =  DB::select($statement, $parameter);
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        foreach ($data as $photo) {
            if ($photo) {
                if ($photo->photo != null) {
                    $url = Storage::temporaryUrl(
                        $photo->photo,
                        now()->addMinutes(120)
                    );
                    $url_thumb = Storage::temporaryUrl(
                        $photo->thumb,
                        now()->addMinutes(120)
                    );
                    $photo->photo = $url;
                    $photo->thumb = $url_thumb;
                }
            }
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function getDeadSpecimenEvidence(Request $request)
    {
        $statement = 'select * from dead_specimen_evidences dse where dead_specimen_id = ?';
        $parameter = [
            $request->dead_specimen_id
        ];
        $evidences = DB::select($statement, $parameter);
        foreach ($evidences as $evidence) {
            if ($evidence->url != null) {
                $url = Storage::temporaryUrl(
                    $evidence->url,
                    now()->addMinutes(120)
                );
                $evidence->url = $url;
            }
        }
        return $evidences;
    }
    public function deleteDeadSpecimens(Request $request)
    {

        $statement_main = 'update dead_specimens set deleted_at = ?, deleted_by = ? where id = ?';
        $parameter_main = [
            now(),
            $request->deleted_by,
            $request->dead_specimen_id
        ];
        DB::select($statement_main, $parameter_main);
        if ($request->type_specimen == 1) {
            $statement = 'call save_dead_cintillo_pata(?,?,?)';
            $parameters = [
                $request->cintillo_pata,
                $request->quantity,
                2
            ];
            DB::select($statement, $parameters);
        } else if ($request->type_specimen == 2) {
            $statement = 'call save_dead_cintillo_ala(?,?,?)';
            $parameters = [
                $request->cintillo_ala,
                $request->quantity,
                2
            ];
            DB::select($statement, $parameters);
        } else if ($request->type_specimen == 3) {
            $statement_specimen_muerto = 'update specimens set status = 2 where plate = ?';
            $parameter = [
                $request->plate
            ];
            DB::select($statement_specimen_muerto, $parameter);
        }
    }
    public function getLotesByCombination(Request $request)
    {
        $statement = 'select * from cintillo_pata_posturas cpp join lote_posturas lp on lp.postura_id = cpp.postura_id join lotes l on l.id = lp.lote_id where cpp.cintillo_pata_id = ? and cpp.status = 1;';
        $parameters = [
            $request->cintillo_pata_id
        ];
        $data = DB::select($statement, $parameters);
        return $data;
    }
}
