<?php

namespace App\Http\Controllers\Api\chicks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use FFMpeg;
use Intervention\Image\Facades\Image;

class ApiChickController extends Controller
{
    public function saveChicks(Request $request)
    {
        $statement_lote = 'update lotes set status = ? where id = ?';
        $parameters_lote = [
            $request->status,
            $request->lote_id
        ];
        DB::select($statement_lote, $parameters_lote);
        $statement = 'insert into chicks (cintillo_ala, lote_postura_id, postura_id, gender, status, created_by, created_at, updated_at, date_of_birth) values (?,?,?,?,?,?,now(),now(),?)';
        $statement_lotes = 'update lote_posturas set cintillo_ala_status = 2 where id = ?';
        foreach ($request->chicks as $chick) {
            $parameters = [
                $chick['cintillo_ala'],
                $request->lote_postura_id,
                $request->postura_id,
                $chick['gender'],
                1,
                $request->created_by,
                $request->date_of_birth
            ];
            DB::select($statement, $parameters);
        }
        $parameter_lote = [
            $request->lote_postura_id
        ];
        DB::select($statement_lotes, $parameter_lote);
        // $statement_j = 'update cintillo_pata_posturas set status = 2 where postura_id = ?;';
        // $parameter_j = [
        //     $request->postura_id
        // ];
        // DB::select($statement_j, $parameter_j);
        // $cintillo_pata_id_header = 'select cintillo_pata_id from cintillo_pata_posturas where postura_id = ?';
        // $cintillo_pata_id_param = [
        //     $request->postura_id
        // ];
        // $cintillo_pata_id = DB::select($cintillo_pata_id_header, $cintillo_pata_id_param);



        // if (sizeof($cintillo_pata_id) > 0) {
        //     $liberar_cintillo_sql = 'update cintillo_patas set status = 1 where id = ?';
        //     $liberar_cintillo_sql_param = [
        //         $cintillo_pata_id[0]->cintillo_pata_id
        //     ];
        //     DB::select($liberar_cintillo_sql, $liberar_cintillo_sql_param);
        // }
    }
    public function saveChicksFromRevision(Request $request)
    {
        $statement = 'insert into chicks (cintillo_ala, postura_id, gender, status, created_by, created_at, updated_at, date_of_birth) values (?,?,?,?,?,now(),now(),?)';
        foreach ($request->posturas as $postura) {
            foreach ($postura['cintillos_alas'] as $chick) {
                $parameters = [
                    $chick['cintillo_ala'],
                    $postura['postura_id'],
                    $chick['gender'],
                    1,
                    $request->created_by,
                    now()
                ];
                DB::select($statement, $parameters);
            }
        }
    }
    public function validateCintilloAla(Request $request)
    {
        $statement = 'select ck.cintillo_ala from chicks ck join lote_posturas lp on lp.id = ck.lote_postura_id where ck.cintillo_ala = ? and lp.deleted_at is null';
        $parameter = [
            $request->cintillo_ala
        ];
        $data = DB::select($statement, $parameter);
        return $data;
    }
    public function validateCintilloAlaForUpload(Request $request)
    {
        $statement = 'select cintillo_ala from chicks where cintillo_ala = ? and gender+0 = 1 and status+0 = 1';
        $parameter = [
            $request->t_plate
        ];
        $data = DB::select($statement, $parameter);
        return $data;
    }
    public function validateCintilloAlaForDeadSpecimens(Request $request)
    {
        $statement = 'select ck.cintillo_ala from chicks ck join lote_posturas lp on lp.id = ck.lote_postura_id where ck.cintillo_ala = ? and lp.deleted_at is null and ck.status <> 2 and ck.assigned_plate is null';
        $parameter = [
            $request->cintillo_ala
        ];
        $data = DB::select($statement, $parameter);
        return $data;
    }
    public function getChicks(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        if ($request->option == 1) {
            $statement = 'call get_chicks(?,?,?,?,?,?,?)';
            $parameter = [
                $page,
                $perpage,
                'desc',
                6,
                $request->campo,
                $request->start_date,
                $request->final_date
            ];
        } else {
            $statement = 'call get_chicks_by_specimen(?,?,?,?,?,?,?)';
            $parameter = [
                $page,
                $perpage,
                'desc',
                6,
                $request->campo,
                $request->start_date,
                $request->final_date
            ];
        }

        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
            if ($photo->padrillo_photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->padrillo_photo,
                    now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->padrillo_thumb,
                    now()->addMinutes(120)
                );
                $photo->padrillo_photo = $url;
                $photo->padrillo_thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function saveNewDobToChick(Request $request)
    {
        $statement = 'update chicks set date_of_birth = ? where id = ?';
        $parameters = [
            $request->new_dob,
            $request->chick_id
        ];
        DB::select($statement, $parameters);
    }
    public function registerDeadEggs(Request $request)
    {

        $statement = 'call save_dead_eggs(?,?,?,?)';
        $parameters = [
            $request->quantity,
            $request->description,
            $request->lote_id,
            $request->created_by
        ];
        $dead_chick_id = DB::select($statement, $parameters);
        return $dead_chick_id;
    }
    public function editChick(Request $request)
    {
        $statement = 'update chicks set gender = ?, status = ? where id = ?';
        $parameters = [
            $request->gender,
            $request->status,
            $request->chick_id
        ];
        DB::select($statement, $parameters);
    }
    public function saveVideo(Request $request)
    {
        try {
            $hour = date('Y-m-d-h-i-s');
            $namevideo = $hour . '.' . $request->extension;
            $namethumb = $hour . '.png';
            $file_path = 'chicks/imagesChicks/' . $request->chick_id . '/videos' . '/' . $namevideo;
            $file_path_thumb = 'chicks/imagesChicks/' . $request->chick_id . '/videos' . '/' . $namethumb;
            Storage::disk('s3')->put($file_path, file_get_contents($request->video));

            $thumbnail =  FFMpeg::fromDisk('s3')
                ->open($file_path)
                ->addFilter(function ($filters) {
                    $filters->resize(new \FFMpeg\Coordinate\Dimension(5, 5));
                })
                ->getFrameFromSeconds(3)
                ->export()
                ->toDisk('s3')
                ->save($file_path_thumb);
            $url_thumb = Storage::temporaryUrl(
                $file_path_thumb,
                now()->addMinutes(120)
            );
            $img = Image::make(file_get_contents($url_thumb));
            $img->resize(200, 200)->encode('jpg');
            Storage::disk('s3')->put($file_path_thumb, $img->__toString());
            $statement = 'call save_chick_video(?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $parameter = [
                $request->chick_id,
                $request->iduser,
                $request->type,
                $request->name_video,
                $request->type_category,
                $request->plaquerival,
                date_format(date_create($request->event_date), 'Y-m-d'),
                $request->event_result,
                $request->amarrador,
                $request->careador,
                $request->duration,
                $file_path,
                $file_path_thumb
            ];
            DB::select($statement, $parameter);
        } catch (Exception $e) {
            return  ['status' => 500, 'msg' => 'Error de conexion'];
        }
    }
    public function getPollitosMachos()
    {
        $statement = 'select * from chicks where gender = 1';
        $pollitos = DB::select($statement);
        return $pollitos;
    }
    public function getChicksGrid(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = 100;
        $statement = 'call get_chick_videos(?,?,?,?,?)';
        $parameter = [
            $page,
            $perpage,
            'desc',
            6,
            $request->campo
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            $url_video = Storage::temporaryUrl(
                $photo->url,
                now()->addMinutes(120),
                ['ResponseContentDisposition' => 'attachment']
            );
            if ($photo->thumb != null) {
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,
                    now()->addMinutes(120)
                );
                $photo->thumb = $url_thumb;
            }
            $photo->url = $url_video;
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }
        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }

    public function deleteChickVideo(Request $request)
    {
        $statement = 'update chick_videos set deleted_at = now(), deleted_by = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->chick_video_id
        ];
        DB::select($statement, $parameters);
    }
    public function deleteChick(Request $request)
    {
        $statement = 'update chicks set deleted_at = now(), deleted_by = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->chick_id
        ];
        DB::select($statement, $parameters);
    }
    public function setPlacaToChickenWhoHasCintillo(Request $request)
    {
        $statement = 'call set_plate_to_chickens(?,?,?,?,?)';
        $parameters = [
            $request->chicken_id,
            $request->assigned_plate,
            $request->user_id,
            $request->dob,
            $request->gender
        ];
        DB::select($statement, $parameters);
    }
}
