<?php

namespace App\Http\Controllers\Api\eggs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiEggController extends Controller
{
    public function getEggs(Request $request){
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_eggs(?,?,?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            'desc',
            4,
            $request->campo,
            $request->status,
            $request->date
        ];
        $data =  DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->count_registered_eggs;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function getEggsPostura(Request $request){
        $statement = 'select
                    ep.id,
                    ep.postura_id,
                    s.alias madrilla_alias,
                    s.id madrilla_id,
                    s.photo photo,
	                s.thumb thumb,
                    s.plate placa_madrilla,
                    sp.alias padrillo_alias,
                    sp.plate placa_padrillo,
                    ep.quantity
                from
                    egg_postura ep
                join posturas p on
                    p.id = ep.postura_id
                join specimens s on
                    s.plate = p.placa_madrilla
                join specimens sp on
                    sp.plate = p.placa_padrillo
                where
                    eggs_id = ?';
        $parameters = [
            $request->eggs_id
        ];
        $data = DB::select($statement, $parameters);
        foreach($data as $photo){
            if($photo->photo != null){
                $url = Storage::temporaryUrl(
                    $photo->photo,now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        return $data;
    }
    public function getEggsDetail(Request $request)
    {
        $statement = 'select
                e.*,
                ep.quantity
            from
                eggs e
            join egg_postura ep on
                ep.eggs_id = e.id
            join posturas p on
                p.id = ep.postura_id
            join specimens s on
                s.plate = p.placa_madrilla where s.plate = ?';
        $parameter = [
            $request->madrilla_plate
        ];
        return DB::select($statement, $parameter);
    }
    public function deleteEggs(Request $request){

        $statement_header = 'call delete_register_eggs(?,?,?)';
        $parameters_header = [
            $request->eggs_id,
            $request->elimination_comment,
            $request->deleted_by
        ];
        DB::select($statement_header, $parameters_header);
        $statement_get_detail = 'select * from egg_postura where eggs_id = ?';
        $parameter_get_detail = [
            $request->eggs_id
        ];
        $detail = DB::select($statement_get_detail, $parameter_get_detail);

        $statement_detail = 'call decreasae_postura_quantities(?,?,?,?)';
        foreach($detail as $det){
            $parameters_detail = [
                $det->postura_id,
                $det->quantity,
                $request->deleted_by,
                $request->eggs_id
            ];
            DB::select($statement_detail, $parameters_detail);
        }
    }
    public function getTblProjects()
    {
        $statement = 'select id value, parent_id, project_name from tbl_projects';
        $data = DB::select($statement);
        $tree = $this->buildTree($data);
        return $tree;
    }
    public function buildTree($elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            // dd($element);
            if ($element->parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->value);
                if ($children) {
                    $element->children = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
    public function getEgg(Request $request)
    {
        $statement = 'call get_egg(?)';
        $parameter = [
            $request->egg_id
        ];
        $statement_header = 'select * from eggs where id = ?';
        $parameter_header = [
            $request->egg_id
        ];
        $data = DB::select($statement, $parameter);
        $data_header = DB::select($statement_header, $parameter_header);
        return array($data,$data_header);
    }
}
