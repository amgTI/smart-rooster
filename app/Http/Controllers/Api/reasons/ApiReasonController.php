<?php

namespace App\Http\Controllers\Api\reasons;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiReasonController extends Controller
{
    public function saveReason(Request $request)
    {
        $statement = 'insert into reasons (module_id, parent_id, reason, created_by, created_at, updated_at) values (?,?,?,?,?,?)';
        $parameters = [
            $request->module_id,
            $request->parent_id,
            $request->reason,
            $request->created_by,
            now(),
            now()
        ];
        DB::statement($statement, $parameters);
    }
    public function searchReason(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_reasons(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function getModules()
    {
        $statement = 'select * from modules where deleted_at is null';
        $modules = DB::select($statement);
        return $modules;
    }
    public function getReasons(Request $request)
    {
        $statement = 'select r.* from reasons r where r.module_id = ? and r.deleted_at is null';
        $parameter = [
            $request->module_id
        ];
        $reasons = DB::select($statement, $parameter);
        return $reasons;
    }
    public function deleteReason(Request $request)
    {
        $statement = 'update reasons set deleted_by = ?, deleted_at = now() where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->reason_id
        ];
        DB::select($statement, $parameters);
    }
    public function updateReason(Request $request)
    {
        $statement = 'update reasons set reason = ? where id = ?';
        $parameters = [
            $request->reason,
            $request->reason_id
        ];
        DB::select($statement, $parameters);
    }
    public function getReasonsByModule(Request $request)
    {
        $statement = 'select r.id, r.module_id, r.parent_id, r.reason, r2.reason parent_reason from reasons r left join reasons r2 on r2.id = r.parent_id where r.module_id = ?';
        $parameter = [
            $request->module_id
        ];
        $data = DB::select($statement, $parameter);
        return $data;
    }
}
