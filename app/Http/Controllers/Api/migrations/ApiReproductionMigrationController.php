<?php

namespace App\Http\Controllers\Api\migrations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiReproductionMigrationController extends Controller
{
    public function save_migration(Request $request){
        $statement = 'call sp_save_migration(?,?,?,?,?,?)';
        $parameters = [
            $request->migration_header['placa_madrilla'],
            $request->migration_header['placa_padrillo'],
            $request->migration_header['date_of_birth'],
            $request->migration_header['observation'],
            $request->migration_header['status'],
            $request->migration_header['created_by']
        ];
        $migration = DB::select($statement, $parameters);
        $statement_body = 'insert into reproduction_flow_migrations_detail (flow_migration_id, cintillo_ala, gender, created_at, updated_at) values (?,?,?,now(),now())';
        foreach($request->migration_body as $entry)
        {
            $parameters_body = [
                $migration[0]->migration_id,
                $entry['cintillo_ala'],
                $entry['gender']
            ];
            DB::select($statement_body, $parameters_body);
        }
    }
    public function get_migrations(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_reproduction_flow_migrations(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            'desc',
            6,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        foreach($data as $photo){
            if($photo->photo != null){
                $url = Storage::temporaryUrl(
                    $photo->photo,now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
            if($photo->padrillo_photo != null){
                $url = Storage::temporaryUrl(
                    $photo->padrillo_photo,now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->padrillo_thumb,now()->addMinutes(120)
                );
                $photo->padrillo_photo = $url;
                $photo->padrillo_thumb = $url_thumb;
            }
        }
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function get_migration(Request $request)
    {
        $statement = 'call get_reproduction_flow(?)';
        $parameter = [
            $request->migration_id
        ];
        $data = DB::select($statement, $parameter);
        foreach($data as $entry)
        {
            $entry->detail = json_decode($entry->detail);
        }
        return $data;
    }
    public function delete_migration(Request $request)
    {
        $statement = 'update reproduction_flow_migrations set deleted_by = ?, deleted_at = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            now(),
            $request->migration_id
        ];
        DB::select($statement, $parameters);
    }
    public function update_migration(Request $request)
    {
        $statement = 'update reproduction_flow_migrations set placa_madrilla = ?, placa_padrillo = ?, date_of_birth = ?, observation = ?, status = ? where id = ?';
        $parameter = [
            $request->migration_header['placa_madrilla'],
            $request->migration_header['placa_padrillo'],
            $request->migration_header['date_of_birth'],
            $request->migration_header['observation'],
            $request->migration_header['status'],
            $request->migration_header['migration_flow_id']
        ];
        DB::select($statement, $parameter);
        $statement_detail = 'call update_migration_flow_detail(?,?,?,?)';
        foreach($request->migration_body as $detail){
            $parameter_detail = [
                    $detail['migration_id'],
                    $request->migration_header['migration_flow_id'],
                    $detail['cintillo_ala'],
                    $detail['gender']
            ];
            DB::select($statement_detail, $parameter_detail);
        }
        $statement_delete = 'delete from reproduction_flow_migrations_detail where id = ?';
        foreach($request->to_delete as $deleted){
            $parameter_delete = [
                $deleted
            ];
            DB::select($statement_delete, $parameter_delete);
        }
    }
}
