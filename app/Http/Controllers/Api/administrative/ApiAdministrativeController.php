<?php

namespace App\Http\Controllers\Api\administrative;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiAdministrativeController extends Controller
{
    public function saveTransaction(Request $request)
    {
        $request->header = json_decode($request->header);
        $statement_header = 'call save_admin_transactions(?,?,?,?,?,?,?)';
        $parameters_header = [
            $request->header->transaction_id,
            $request->header->type,
            $request->header->transaction_date,
            $request->header->memo,
            $request->total,
            $request->status,
            $request->created_by
        ];
        $transaction_id = DB::select($statement_header, $parameters_header);
        $statement_detail = 'call save_admin_transaction_details(?,?,?,?,?)';
        foreach($request->header->details as $detail)
        {
            $parameters_detail = [
                sizeof($transaction_id)>0?$transaction_id[0]->transaction_id:$request->header->transaction_id,
                $detail->reason?$detail->reason->id:null,
                $detail->description,
                $detail->amount,
                $detail->id
            ];
            if($detail->reason){
                DB::select($statement_detail, $parameters_detail);
            }
        }
        if ($request->file('images') != null) {
            foreach ($request->file('images') as $file) {
                $textname = $file->getClientOriginalName();
                $filePath = 'administrative/transactions/files'. $textname;
                $ext = $file->getClientOriginalExtension();
                Storage::disk('s3')->put($filePath, file_get_contents($file));
                $statement='call save_admin_transaction_files(?,?,?,?,?,?)';
                $parameter=[
                    sizeof($transaction_id)>0?$transaction_id[0]->transaction_id:$request->header->transaction_id,
                    $textname,
                    $filePath,
                    filesize($file),
                    $ext,
                    $request->created_by
                ];
                DB::select($statement, $parameter);
            }
        }
    }
    public function getTransactions(Request $request)
    {
        // dd($request);
        $page = $request->page ? $request->page : 1;
        $perpage = $request->perpage;
        $statement='call get_admin_transactions(?,?,?,?,?,?,?,?)';
        $parameter=[
            $request->campo,
            $request->orderby == 1 ? 'desc' : 'asc',
            $request->order,
            $perpage,
            $page,
            $request->from,
            $request->to,
            $request->type
        ];
        $data = DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }

        $results = collect( $data);

        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function getDashboardData()
    {
        $statement = 'select sum(at2.total) totals from admin_transactions at2 where at2.deleted_at is null group by type;';
        $data = DB::select($statement);
        return $data;
    }
    public function deleteTransaction(Request $request)
    {
        $statement = 'update admin_transactions set deleted_at = now(), deleted_by = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->transaction_id
        ];
        DB::select($statement, $parameters);
    }
    public function getTransaction(Request $request)
    {
        $statement = 'call get_admin_transaction(?);';
        $parameters = [
            $request->transaction_id
        ];

        $data = DB::select($statement, $parameters);
        $data[0]->details = json_decode($data[0]->details);
        return $data;
    }
}
