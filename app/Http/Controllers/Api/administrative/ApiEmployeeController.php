<?php

namespace App\Http\Controllers\Api\administrative;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiEmployeeController extends Controller
{
    public function saveEmployee(Request $request)
    {
        $statement = 'call save_employee(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
        $parameters = [
            $request->employee_id,
            $request->first_name,
            $request->middle_name,
            $request->last_name,
            $request->known_as,
            $request->doc_type,
            $request->document_number,
            $request->doe,
            $request->nationality,
            $request->dob,
            $request->gender,
            $request->martial_status,
            $request->mailling_address,
            $request->city,
            $request->state,
            $request->zip_code,
            $request->country,
            $request->home_phone,
            $request->mobile_phone,
            $request->work_phone,
            $request->work_email,
            $request->private_email,
            $request->contact_name,
            $request->contact_phone,
            $request->relation,
            $request->contact_name_second,
            $request->contact_phone_second,
            $request->relation_second,
            $request->start_date,
            $request->end_date,
            $request->position,
            $request->salary,
            $request->created_by
        ];
        $create = DB::select($statement, $parameters);
        $statement_update_photo = 'update employees set photo = ? where id = ?';
        if($request->photo != '' && $request->photo != null && $request->photo_name){
            $cont = explode(".",$request->photo_name);
            $name_avatar = 'avatar.'.$cont[1];
            $name_folder = "employees";
            $name_folder_user = $create[0]->employee_id;
            $file_path = $name_folder.'/'.$name_folder_user.'/'.$name_avatar;
            $parameters_update = [
                $file_path,
                $create[0]->employee_id
            ];
            DB::select($statement_update_photo, $parameters_update);
            Storage::disk('s3')->put($file_path, file_get_contents($request->photo));
        }
    }
    public function getEmployees(Request $request)
    {
        $page = $request->page ? $request->page : 1;
        $perpage = $request->perpage;
        $statement = 'call get_employees(?,?,?,?,?)';
        $parameter = [
            $request->campo,
            $request->orderby == 1 ? 'desc' : 'asc',
            $request->order,
            $perpage,
            $page
        ];
        $data =  DB::select($statement, $parameter);
        foreach ($data as $photo) {
            if ($photo->photo != null) {
                $url = Storage::temporaryUrl(
                    $photo->photo,
                    now()->addMinutes(120)
                );
                $photo->photo = $url;
                // $photo->thumb = $url_thumb;
            }
        }
        if (count($data)) {
            $totalCount = $data[0]->cc;
        } else {
            $totalCount = count($data);
        }

        $results = collect($data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount, $perpage, $page);
        return $paginator;
    }
    public function deleteEmployee(Request $request)
    {
        $statement = 'update employees set deleted_at = now(), deleted_by = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            $request->employee_id
        ];
        DB::select($statement, $parameters);
    }
    public function getEmployee(Request $request)
    {
        $statement = 'call get_employee(?)';
        $parameters = [
            $request->employee_id
        ];
        $data = DB::select($statement, $parameters);
        if ($data[0]->photo != null) {
            $url = Storage::temporaryUrl(
                $data[0]->photo,
                now()->addMinutes(120)
            );
            $data[0]->photo = $url;
            // $photo->thumb = $url_thumb;
        }
        return $data;
    }
}
