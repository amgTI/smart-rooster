<?php

namespace App\Http\Controllers\Api\cintillos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiChickenLegController extends Controller
{
    public function getChickenLeg(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_chickenlegs(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->count_cintillos;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function insertChickenLeg(Request $request)
    {
        $statement = 'insert into chickenlegs (description, priority, created_by, created_at, updated_at) values (?,?,?,?,?)';
        $parameters = [
            $request->description,
            $request->priority,
            $request->created_by,
            now(),
            now()
        ];
        try{
            DB::select($statement, $parameters);
        }catch (\Illuminate\Database\QueryException $e){
            return response()->json(['errors' => 'Prioridad duplicada', 'text' => 'Por favor elige otra prioridad'], 422);
        }
    }
    public function updateChickenLeg(Request $request, $cintillo_id)
    {
        $statement = 'update chickenlegs set description = ?, set priority = ? where id = ?';
        $parameters = [
            $request->description,
            $request->priority,
            $cintillo_id
        ];
        try{
            DB::select($statement, $parameters);
        }catch (\Illuminate\Database\QueryException $e){
            return response()->json(['errors' => 'Prioridad duplicada', 'text' => 'Por favor elige otra prioridad'], 422);
        }
    }
    public function getCurrentPriorities()
    {
        $statement = 'select priority from chickenlegs';
        $priorities = DB::select($statement);
        return $priorities;
    }
}
