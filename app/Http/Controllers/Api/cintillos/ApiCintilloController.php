<?php

namespace App\Http\Controllers\Api\cintillos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiCintilloController extends Controller
{
    public function getCintillos(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_cintillos(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->count_cintillos;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function insertCintillo(Request $request)
    {
        $statement = 'insert into cintillos (color, description, created_by, created_at, updated_at) values (?,?,?,?,?)';
        $parameters = [
            $request->color,
            $request->description,
            $request->created_by,
            now(),
            now()
        ];
        DB::select($statement, $parameters);
    }
    public function updateCintillo(Request $request, $cintillo_id)
    {
        $statement = 'update cintillos set description = ?, color = ? where id = ?';
        $parameters = [
            $request->description,
            $request->color,
            $cintillo_id
        ];
        DB::select($statement,$parameters);
    }
}
