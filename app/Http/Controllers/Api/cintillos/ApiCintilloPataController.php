<?php

namespace App\Http\Controllers\Api\cintillos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiCintilloPataController extends Controller
{
    public function getCintilloPatas(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_cintillo_patas(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        if (count($data)){
            $totalCount = $data[0]->count_cintillo_patas;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function getCintillosAndChickenlegs()
    {
        $statement_colours = 'select * from cintillos';
        $statement_chickenlegs = 'select * from chickenlegs order by priority asc';
        $colours = DB::select($statement_colours);
        $chickenlegs = DB::select($statement_chickenlegs);
        return array($colours, $chickenlegs);
    }
    public function saveCombination(Request $request)
    {
        $statement = 'call save_combination(?,?,?)';
        foreach($request->chickenlegs as $chickenleg)
        {
            foreach($request->colours as $colour)
            {
                $parameters = [
                    $colour['id'],
                    $chickenleg['id'],
                    $request->created_by
                ];
                DB::select($statement, $parameters);
            }
        }
        // try{
        //     DB::select($statement, $parameters);
        // }catch (\Illuminate\Database\QueryException $e){
        //     return response()->json(['errors' => 'Combinación duplicada', 'text' => 'Por favor elige otra combinación'], 422);
        // }

    }
    public function getFreeCombinations()
    {
        $statement = 'select
                c.description color_description,
                c.color color_color,
                c.id color_id,
                cl.id chickenleg_id,
                cl.description chickenleg_description,
                cp.id cintillo_pata_id,
                cl.priority chickenleg_priority
            from
                cintillo_patas cp
            join cintillos c on
                c.id = cp.cintillo_id
            join chickenlegs cl on
                cl.id = cp.chickenleg_id
            where
                cp.status = 1 order by cl.priority asc';
        return DB::select($statement);
    }
    public function getFreeCombinationsByPosturas(Request $request)
    {
        $post = '('.implode(',', $request->posturas_id).')';
        $statement = "select
                json_arrayagg(JSON_OBJECT('id',
                p.id,
                'cintillo_pata_id',
                cpp.cintillo_pata_id, 'cintillo_id', cp.cintillo_id, 'chickenleg_id', cp.chickenleg_id, 'chickenleg_description', cl.description, 'cintillo_description', c.description, 'cintillo_color', c.color, 'chickenleg_priority', cl.priority)) arreglos
            from
                posturas p
            left join cintillo_pata_posturas cpp on
                cpp.postura_id = p.id
            left join cintillo_patas cp on
                cp.id = cpp.cintillo_pata_id
            left join cintillos c on
                c.id = cp.cintillo_id
            left join chickenlegs cl on
                cl.id = cp.chickenleg_id
            where
               p.id in ".$post;

        $combinations_by_postura = DB::select($statement);

        return json_decode($combinations_by_postura[0]->arreglos);
    }
    public function setCombinationsToPostura(Request $request)
    {
        $statement_cintillo_pata = 'update cintillo_patas set status = 2 where id = ?';
        $statement_cintillo_pata_postura = 'insert into cintillo_pata_posturas (cintillo_pata_id, postura_id, status, created_at, updated_at) values (?,?,1,now(),now())';
        foreach($request->combinations as $combination)
        {
            $parameters_cintillo_pata = [
                $combination['cintillo_pata_id']
            ];
            DB::select($statement_cintillo_pata, $parameters_cintillo_pata);
            $parameters_cintillo_pata_postura = [
                $combination['cintillo_pata_id'],
                $combination['id']
            ];
            DB::select($statement_cintillo_pata_postura, $parameters_cintillo_pata_postura);
        }
    }
    public function deleteCombination(Request $request)
    {
        $statement = 'update cintillo_patas set deleted_by = ?, deleted_at = ? where id = ?';
        $parameters = [
            $request->deleted_by,
            now(),
            $request->combination_id
        ];
        DB::select($statement, $parameters);
    }
    public function getCombinationsUsed()
    {
        $statement = 'select
                    cp.*,
                    c.color cintillo_color,
                    c.description cintillo_description,
                    c2.description chickenleg_description
                from
                    cintillo_patas cp
                join cintillos c on
                    c.id = cp.cintillo_id
                join chickenlegs c2 on
                    c2.id = cp.chickenleg_id
                where
                    cp.status = 2';
        $combinations = DB::select($statement);
        return $combinations;
    }
}
