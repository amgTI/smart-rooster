<?php

namespace App\Http\Controllers\Api\Specimens;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RequestUploadVideo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use FFMpeg;
use FFMpeg\Filters\Video\VideoFilters;
use Illuminate\Support\Facades\Auth;
use Spatie\Glide\GlideImageFacade;
use Intervention\Image\Facades\Image;
use Spatie\MediaLibrary\UrlGenerator;
class ApiSpecimensController extends Controller
{

    public function getdataselects(Request $request){
        $statement = 'call data_select_specimens()';
        return DB::select($statement);
    }
    public function edit($id){
        $statement = 'call get_specimens_id(?)';
        $parameter =[
            $id
        ];
        $specimen = DB::select($statement,$parameter);
        foreach($specimen as $photo){
            if($photo->photo != null){

                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->thumb = $url_thumb;
            }
            if($photo->url_evidence != null){

                $url_evidence_thumb = Storage::temporaryUrl(
                    $photo->url_evidence_thumb,now()->addMinutes(120)
                );
                $photo->url_evidence_thumb = $url_evidence_thumb;
            }
        }
        return $specimen;
    }
    public function getcolorspecimen(Request $request){
        $statement = 'call get_color_specimen(?)';
        $parameter = [
            $request->gender_specimen
        ];
        return DB::select($statement, $parameter);
    }
    public function save_specimens(Request $request){
        if($request->file_name != null || $request->file_name != ''){
            $cont = explode(".",$request->file_name);
            $name = 'avatar.'.$cont[1];
            $name_thumb = 'thumb.'.$cont[1];
        }
        if($request->file_name_evidence != null || $request->file_name_evidence != ''){
            $cont = explode(".",$request->file_name_evidence);
            $name_evidence = 'evidence.'.$cont[1];
            $name_thumb_evidence = 'thumb_evidence.'.$cont[1];
        }
        $name_folder = "ejemplares";
        $folder_specimens = "imagesEjemplares";

        $statement = 'call save_specimens(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        $parameter = [
            $request->iduser,
            $request->origin,
            $request->alias,
            $request->placa,
            $request->p_adicional,
            $request->breeder,
            $request->breeder_id,
            str_replace(',','',$request->price ? : 0),
            $request->ejem_color,
            $request->category,
            $request->gender,
            $request->status,
            date_format(date_create($request->dob), 'Y-m-d'),
            $request->obs,
            $request->t_update,
            $request->file_name == '' ? null : $name,
            $request->file_name == '' ? null : $name_thumb,
            $request->currency,
            $request->file_name_evidence == '' ? null : $name_evidence,
            $request->file_name_evidence == '' ? null : $name_thumb_evidence,
            $request->description_evidence == '' ? null : $request->description_evidence,
            $request->death_date == '' ? null : date_format(date_create($request->death_date), 'Y-m-d'),
            $request->definition == '' ? null : $request->definition
        ];

        $create =  DB::select($statement,$parameter);
        $name_folder_specimens_id = $create[0]->id_specimen;
        if($request->file_image != '' && $request->file_image != null){
            $file_path = $name_folder.'/'.$folder_specimens.'/'.$name_folder_specimens_id.'/'.$name;
            $file_path_thumb = $name_folder.'/'.$folder_specimens.'/'.$name_folder_specimens_id.'/'.$name_thumb;

            Storage::disk('s3')->put($file_path, file_get_contents($request->file_image));
            Storage::disk('s3')->put($file_path_thumb, file_get_contents($request->thumb));
        }
        if($request->file_image_evidence != '' && $request->file_image_evidence != null){

            $file_path_evidence = $name_folder.'/'.$folder_specimens.'/'.$name_folder_specimens_id.'/'.$name_evidence;
            $file_path_thumb_evidence = $name_folder.'/'.$folder_specimens.'/'.$name_folder_specimens_id.'/'.$name_thumb_evidence;

            Storage::disk('s3')->put($file_path_evidence, file_get_contents($request->file_image_evidence));
            Storage::disk('s3')->put($file_path_thumb_evidence, file_get_contents($request->thum_evidence));
        }
        return $create;
    }
    public function get_specimens(Request $request){
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_specimens(?,?,?,?,?,?,?,?,?,?,?)';
        $parameter = [
            $request->iduser,
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo,
            $request->filter_origin,
            $request->filter_color,
            $request->filter_category,
            $request->filter_status,
            $request->filter_rival
        ];
        $data =  DB::select($statement,$parameter);
        foreach($data as $photo){
            if($photo->photo != null){
                $url = Storage::temporaryUrl(
                    $photo->photo,now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }

        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function delete_specimen(Request $request){
        $statement = 'call detele_specimen(?,?)';
        $parameter = [
            $request->id,
            $request->iduser
        ];
       DB::statement($statement,$parameter);
    }

    public function validateplate(Request $request){
        $statement = 'call validate_plate(?,?)';
        $parameter = [
            $request->t_plate,
            $request->idspecimen
        ];
        $validate = DB::select($statement,$parameter);
        if($request->idspecimen == null ){//entro en el insert y encontro la placa existente
            if($validate[0]->p_plate != null){
                return 0 ;
            }else{
                return 1;
            }
        }else if($request->t_plate != null || $request->t_plate != '' && $request->idspecimen  != null || $request->idspecimen  != '' ){//entro en el update
            if($validate[0]->p_plate == null &&  $validate[0]->vid == null){
                return 1 ;
            }
            if($validate[0]->p_plate === $request->t_plate && $validate[0]->vid == $request->idspecimen ){// la placa existe pero es igual a la placa actual y el id es el mismo
                return 1 ;
            }
            if($validate[0]->p_plate != null && $validate[0]->vid != $request->idspecimen ){// placa diferente y id diferente
                return 0 ;
            }
        }else{//la placa no existe para ambos casos
            return 1 ;
        }
    }

    public function savevideo(Request $request){
        // dd($request);
        // print_r($request);
        // die();
        try{
            $hour = date('Y-m-d-h-i-s');
            $namevideo = $hour . '.' . $request->extension;
            $namethumb = $hour . '.png';
            $file_path = 'ejemplares/imagesEjemplares/'.$request->idspecimen.'/videos'.'/'. $namevideo;
            $file_path_thumb = 'ejemplares/imagesEjemplares/'.$request->idspecimen.'/videos'.'/'.$namethumb;
            Storage::disk('s3')->put($file_path, file_get_contents($request->video));

            $thumbnail =  FFMpeg::fromDisk('s3')
                                    ->open($file_path )
                                    ->addFilter(function ($filters) {
                                        $filters->resize(new \FFMpeg\Coordinate\Dimension(5, 5));
                                    })
                                    ->getFrameFromSeconds(3)
                                    ->export()
                                    ->toDisk('s3')
                                    ->save($file_path_thumb);
            $url_thumb = Storage::temporaryUrl(
                $file_path_thumb,now()->addMinutes(120)
            );
            $img = Image::make(file_get_contents($url_thumb));
            $img->resize(200, 200)->encode('jpg');
            Storage::disk('s3')->put($file_path_thumb, $img->__toString());
            $statement = 'call save_video(?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $parameter = [
                $request->idspecimen,
                $request->iduser,
                $request->type,
                $request->name_video,
                $request->type_category,
                $request->plaquerival,
                date_format(date_create($request->event_date), 'Y-m-d'),
                $request->event_result,
                $request->amarrador,
                $request->careador,
                $request->duration,
                $file_path,
                $file_path_thumb
            ];
                DB::select($statement,$parameter);

        }catch(Exception $e){
           // $error = 'Error de conexion: ' .  $e->getMessage() .  "\n";

            //return response()->json(['data' => $e->getMessage()],201) ;

            return  ['status' => 500,'msg' => 'Error de conexion'];
        }
    }
    public function updatevideo(Request $request){




        $statement = 'call update_video(?,?,?,?,?,?,?,?,?,?)';
        $parameter = [
            $request->id,
            $request->idspecimen,
            $request->category,
            $request->description,
            $request->amarrador,
            $request->careador,
            $request->type_category,
            $request->rival,
            date_format(date_create($request->event_date), 'Y-m-d'),
            $request->event_result
        ];
        DB::select($statement,$parameter);
    }
    public function getvideospecimen(Request $request){
        $statement = 'call get_videos_specimen(?)';
        $parameter = [
            $request->idspecimen
        ];
        $data = DB::select($statement,$parameter);
        foreach($data as $url){
            $url_video = Storage::temporaryUrl(
                $url->url,now()->addMinutes(120),
                ['ResponseContentDisposition' => 'attachment']
            );
            if($url->thumb != null){
                $url_thumb = Storage::temporaryUrl(
                    $url->thumb,now()->addMinutes(120)
                );
                $url->thumb = $url_thumb;
            }
            $url->url = $url_video;
        }
        return $data;
    }
    public function getvideoid(Request $request){
        $statement = 'call get_video_id(?)';
        $parameter = [
            $request->id
        ];
        return DB::select($statement,$parameter);
    }
    public function deletevideo(Request $request){
        $statement = 'call delete_video(?,?)';
        $parameter = [
            $request->id,
            $request->iduser
        ];
        DB::select($statement,$parameter);
    }
    public function savecolorspecimen(Request $request){
        $statement = 'call save_color_specimen(?,?)';
        $parameter = [
            $request->namecolor,
            $request->gender_specimen
        ];
        return DB::select($statement,$parameter);
    }
    public function saveamarrador(Request $request){
        $statement = 'call save_amarrador(?)';
        $parameter = [
            $request->nameamarrador
        ];
        return DB::select($statement,$parameter);
    }
    public function savecareador(Request $request){
        $statement = 'call save_careador(?)';
        $parameter = [
            $request->namecareador
        ];
        return DB::select($statement,$parameter);
    }
    public function getamarrador(Request $request){
        $statement = 'call get_amarrador()';
       return  DB::select($statement);
    }
    public function getcareador(Request $request){
        $statement = 'call get_careador()';
       return  DB::select($statement);
    }
    public function saverival(Request $request){
        $statement = 'call save_rivals(?)';
        $parameter = [
            $request->namerival
        ];
        return DB::select($statement,$parameter);
    }
    public function getrival(Request $request){
        $statement = 'call get_rivals()';
        return DB::select($statement);
    }
    public function validateplategender(Request $request){
        $statement = 'call validate_plaque_by_gender(?)';
        $parameter = [
            $request->t_plate
        ];
        $validate = DB::select($statement,$parameter);
        if($request->idspecimen == null ){//entro en el insert y encontro la placa existente
                if($validate[0]->p_plate != null){
                    if($validate[0]->ngender == 1){
                        return 0 ;
                    }else{
                        return 2;
                    }
                }else{
                    return 1;
                }

        }else if($request->t_plate != null || $request->t_plate != '' && $request->idspecimen  != null || $request->idspecimen  != '' ){//entro en el update

                if($validate[0]->p_plate == null ){
                    return 1 ;
                }
                if($validate[0]->p_plate != null ){// placa diferente
                    if($validate[0]->ngender == 1){
                        return 0 ;
                    }else{
                        return 2;
                    }
                }

        }else{//la placa no existe para ambos casos
            if($validate[0]->ngender == 1){
                return 1 ;
            }else{
                return 2;
            }
        }
    }
    public function searchtracking(Request $request){
        $statement = 'call get_tracking_specimen(?)';
        $parameter = [
            $request->idspecimen
        ];
        $data =  DB::select($statement,$parameter);
        foreach($data as $photo){
            if($photo->url_evidence != null){
                $url = Storage::temporaryUrl(
                    $photo->url_evidence,now()->addMinutes(120)
                );
                $url_evidence_thumb = Storage::temporaryUrl(
                    $photo->url_evidence_thumb,now()->addMinutes(120)
                );
                $photo->url_evidence = $url;
                $photo->url_evidence_thumb = $url_evidence_thumb;
            }
        }
        return $data;
    }
    public function searchRivals(Request $request){
        if($request['q'] != ""){
            $statement='call search_rivals("'.$request['q'].'")';
            $rivals =  DB::select($statement);
        }else{
            $rivals = [];
        }
        return $rivals;
    }
    public function validatePlateForDeadSpecimens(Request $request)
    {
        $statement = 'select plate from specimens where plate = ? and status <> 3';
        $parameter = [
            $request->plate
        ];
        $data = DB::select($statement, $parameter);
        return $data;
    }
    public function getSpecimensGrid(Request $request)
    {
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_specimen_videos(?,?,?,?,?)';
        $parameter=[
            $page,
            $perpage,
            'desc',
            6,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        // dd($data);
        foreach($data as $photo){
            if($photo->photo != null){
                $url = Storage::temporaryUrl(
                    $photo->photo,now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->specimen_thumb,now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->specimen_thumb = $url_thumb;
            }
            $url_video = Storage::temporaryUrl(
                $photo->url,now()->addMinutes(120),
                ['ResponseContentDisposition' => 'attachment']
            );
            if($photo->thumb != null){
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->thumb = $url_thumb;
            }
            $photo->url = $url_video;
        }
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }
        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function getGallos()
    {
        $statement = 'select * from specimens where category = 3';
        $gallos = DB::select($statement);
        foreach($gallos as $photo){
            if($photo->photo != null){
                $url = Storage::temporaryUrl(
                    $photo->photo,now()->addMinutes(120)
                );
                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->photo = $url;
                $photo->thumb = $url_thumb;
            }
        }
        return $gallos;
    }
    public function getCounterSpecimens()
    {
        $statement = 'select count(*) counter from specimens where deleted_at is null and category is not null';
        $specimens = DB::select($statement);
        return $specimens;
    }
    public function updateBorrowedPlateToPosturaAndMigration(Request $request)
    {
        if($request->gender == 1){
            $statement = 'update posturas set placa_padrillo = ? where placa_padrillo = ?';
            $statement_es = 'update reproduction_flow_migrations set placa_padrillo = ? where placa_padrillo = ?';
        }else{
            $statement = 'update posturas set placa_madrilla = ? where placa_madrilla = ?';
            $statement_es = 'update reproduction_flow_migrations set placa_madrilla = ? where placa_madrilla = ?';
        }
        $statement_tree = 'update specimen_family_tree set plate = ? where specimen_id = ? and plate = ?';
        $parameters_tree = [
            $request->current_plate,
            $request->specimen_id,
            $request->previous_plate
        ];
        $parameters = [
            $request->current_plate,
            $request->previous_plate
        ];
        $parameters_es = [
            $request->current_plate,
            $request->previous_plate
        ];
        DB::select($statement, $parameters);
        DB::select($statement_es, $parameters_es);
        DB::select($statement_tree, $parameters_tree);
    }
    public function getSpecimenCounters()
    {
        $statement = 'select count(*) counter, s.category category from specimens s where s.deleted_at is null and s.category is not null group by s.category';
        $counters = DB::select($statement);
        return $counters;

    }
}
