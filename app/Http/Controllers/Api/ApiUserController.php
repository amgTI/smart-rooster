<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ApiUserController extends Controller
{
    //
    public function getrole(Request $request){
        $statement = 'call get_roles()';
        return DB::select($statement);
    }
    public function edit($id){
        $statement = 'call get_users_id(?)';
        $parameter=[
            $id
        ];
        $users =  DB::select($statement,$parameter);
        foreach($users as $avatar){
            if($avatar->avatar != null){
                $url = Storage::temporaryUrl(
                    $avatar->avatar,now()->addMinutes(120)
                );
                $avatar->avatar = $url;
            }
        }
        $users[0]->u_roles = json_decode($users[0]->u_roles);
        return $users;
    }
    public function validemail(Request $request){
        $statement = 'call valid_email_user (?,?)';
        $parameter = [
            $request->useremail,
            $request->iduser
        ];
        $validate= DB::select($statement,$parameter);
        //dd($validate[0]);
        if($request->iduser == null ){//entro en el insert y encontro el correo existente
            if($validate[0]->u_email != null){
                return 0;
            }else{
                return 1;
            }
        }else if($request->useremail != null || $request->useremail != '' && $request->iduser  != null || $request->iduser  != '' ){//entro en el update
            if($validate[0]->u_email == null &&  $validate[0]->u_id == null){
                return 1;
            }
            if($validate[0]->u_email === $request->useremail && $validate[0]->u_id == $request->iduser ){// el correo existe pero es igual al correo actual y el id es el mismo
                return 1;
            }
            if($validate[0]->u_email != null && $validate[0]->u_id != $request->iduser ){// correo diferente y id diferente
                return 0;
            }

        }else{//el correo no existe para ambos casos
            return 1;
        }

    }
    public function saveuser(Request $request){

        if(!empty($consult[0]->useremail) && $request->t_update == null  ){
            return 0;
        }else{
            if($request->file_name != null || $request->file_name != '' ){
                $cont = explode(".",$request->file_name);
                $name = 'avatar.'.$cont[1];
            }else{
                $name = null;
            }

            $role_d = [
                'id' => $request->rol
            ];
            $statement = 'call save_user(?,?,?,?,?,?,?,?,?,?,?)';
            $parameter=[
                $request->iduser,
                $request->name,
                $request->last_name,
                $request->phone,
                $request->useremail,
                $request->password,
                Hash::make($request->password),
                $request->rol == 1 ? json_encode($role_d) : json_encode($request->rol),
                $request->status,
                $request->t_update,
                $request->file_name == null || $request->file_name == '' ? null : $name

            ];
            $create =   DB::select($statement,$parameter);
            if($request->file_image != '' && $request->file_image != null){
                $cont = explode(".",$request->file_name);
                $name_avatar = 'avatar.'.$cont[1];
                $name_folder = "users";
                $name_folder_user = $create[0]->id;
                $file_path = $name_folder.'/'.$name_folder_user.'/'.$name_avatar;
                Storage::disk('s3')->put($file_path, file_get_contents($request->file_image));

            }
            return 1;
        }

    }
    public function getCurrentUser(Request $request){
        $role = DB::select("select  ur.role_id,r.name
        from role r
        join user_role ur on ur.role_id = r.id
        join users u on u.id = ur.user_id
        where ur.user_id = ". $request->user_id);

        $query = DB::select("select r.id role_id, r.name role_name, u.id user_id,u.name,u.last_name,
        UPPER(concat(u.name,' ', ifnull(u.last_name,''))) name_user, u.avatar from users u
            join user_role ur on ur.user_id = u.id
            join role r on r.id = ur.role_id
            where u.id = ". $request->user_id);
            $query[0]->role = $role;
            foreach($query as $avatar){
                if($avatar->avatar != null){
                    $url = Storage::temporaryUrl(
                        $avatar->avatar,now()->addMinutes(120)
                    );
                    $avatar->avatar = $url;
                }
            }
            $role_name = '';
            foreach($role as $item){
                $role_name = $role_name.'('.$item->name.'), ';
            }

            $query[0]->u_role = substr($role_name,0,-2);
            // dd($query);
            return $query;
    }
    public function getusers(Request $request){
        //return $request;
        $page = $request->page?$request->page:1;
        $perpage = 100;
        $statement = 'call get_users(?,?,?,?,?,?)';
        $parameter=[
            $request->userid,
            $page,
            $perpage,
            $request->orderby,
            $request->order,
            $request->campo
        ];
        $data =  DB::select($statement,$parameter);
        foreach($data as $avatar){
            if($avatar->avatar != null){
                $url = Storage::temporaryUrl(
                    $avatar->avatar,now()->addMinutes(120)
                );
                $avatar->avatar = $url;
            }
        }
        if (count($data)){
            $totalCount = $data[0]->cc;
        }else{
            $totalCount = count($data);
        }

        $results = collect( $data);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($results, $totalCount,$perpage,$page);
        return $paginator;
    }
    public function getreportaccess(Request $request){
        $statement = 'call get_report_access_log(?)';
        $parameter =[
            $request->iduser
        ];
        return DB::select($statement,$parameter);
    }
}
