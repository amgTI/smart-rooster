<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('guest')->except('logout');
    }
    public function logout () {
        $statement = 'call access_log_report(?,?)';
        $parameter =[
            2,
            Auth::user()->id
        ];
        DB::select($statement,$parameter);
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/login');
    }
    public function authenticated(Request $request, $user) {
        $statement = 'call access_log_report(?,?)';
        $parameter =[
            1,
            $user->id
        ];
        DB::select($statement,$parameter);
    }   
}
