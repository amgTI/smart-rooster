<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Auth;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('users.index');
    }
    public function create(){
        return view('users.create');
    }
    public function edit($id){
        $layout = currentUser();
        $statement = 'call get_users_id(?)';
        $parameter=[
            $id
        ];
        $users =  DB::select($statement,$parameter);
        foreach($users as $avatar){
            if($avatar->avatar != null){
                $url = Storage::temporaryUrl(
                    $avatar->avatar,now()->addMinutes(120)
                );
                $avatar->avatar = $url;
            }
        }
        $users[0]->u_roles = json_decode($users[0]->u_roles);
        return view('users.edit' ,compact('layout','users'));
    }
    public function showApi(Request $request)
    {
        return $request->user();
    }
    public function showlogin(Request $request)
    {
        if (\Auth::user()) {
            return redirect('/main');
        } else {
            return redirect('/login');
        }
    }
    public function getCurrentUser(Request $request)
    {
        if(!$request->ajax()){
            return view('layouts.app');
        }else{
            $current_user = json_encode(currentUser());
            return $current_user;
        }

    }
}
