<?php

namespace App\Http\Controllers\Specimens;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Auth;
class SpecimensController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        
        return view('specimens.index');
    }
    public function create(){
        return view('specimens.create');
    }
    public function edit($id){
        $layout = currentUser();
        $statement = 'call get_specimens_id(?)';
        $parameter =[
            $id
        ];
        $specimen = DB::select($statement,$parameter);
        foreach($specimen as $photo){
            if($photo->photo != null){

                $url_thumb = Storage::temporaryUrl(
                    $photo->thumb,now()->addMinutes(120)
                );
                $photo->thumb = $url_thumb;
            }
            if($photo->url_evidence != null){

                $url_evidence_thumb = Storage::temporaryUrl(
                    $photo->url_evidence_thumb,now()->addMinutes(120)
                );
                $photo->url_evidence_thumb = $url_evidence_thumb;
            }
        }
        return view('specimens.edit',compact('layout','specimen'));
    }

}
