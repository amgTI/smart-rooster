<?php

use Illuminate\Support\Facades\DB;

function currentUser(){

    if(Auth::user()){
        $role = DB::select("select  ur.role_id,r.name
    from role r
    join user_role ur on ur.role_id = r.id
    join users u on u.id = ur.user_id
    where ur.user_id = ". Auth::user()->id);

    $query = DB::select("select r.id role_id, r.name role_name, u.id user_id,u.name,u.last_name,
    UPPER(concat(u.name,' ', ifnull(u.last_name,''))) name_user, u.avatar from users u
        join user_role ur on ur.user_id = u.id
        join role r on r.id = ur.role_id
        where u.id = ". Auth::user()->id);
        $query[0]->role = $role;
        foreach($query as $avatar){
            if($avatar->avatar != null){
                $url = Storage::temporaryUrl(
                    $avatar->avatar,now()->addMinutes(120)
                );
                $avatar->avatar = $url;
            }
        }
        $role_name = '';
        foreach($role as $item){
            $role_name = $role_name.'('.$item->name.'), ';
        }

        $query[0]->u_role = substr($role_name,0,-2);
        return $query[0];
    }else{
        return 'a';
    }


}
?>

