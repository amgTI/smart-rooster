@extends('layouts.app')

@section('content')
    @php 
        $global = [
            'users' => $users,
            'layout' => $layout
        ]
    @endphp
<edit-user :global="{{json_encode($global)}}"></edit-user>

@endsection