@extends('layouts.app')

@section('content')

<create-user :global="{{json_encode(currentUser())}}"></create-user>

@endsection