@extends('layouts.app')
@section('content')
    <user-show :global="{{json_encode(currentUser())}}"></user-show>
@endsection