@extends('layouts.app')
@section('content')
<create-specimens :global="{{json_encode(currentUser())}}"></create-specimens>
@endsection