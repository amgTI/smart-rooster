@extends('layouts.app')
@section('content')
<specimens-show :global="{{json_encode(currentUser())}}" :size="{{env('SIZE_VIDEO')}}"></specimens-show>
@endsection