@extends('layouts.app')
@section('content')
    @php
        $global = [
            'layout' => $layout,
            'specimen' => $specimen 
        ]
    @endphp
<edit-specimens :global="{{json_encode($global)}}"></edit-specimens>
@endsection