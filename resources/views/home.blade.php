@extends('layouts.app')

@section('content')
<div class="row col-12 general-options">
        @if(currentUser()->role_id == 1)
        <div class="col options-home">
            <div class="options text-center" onclick="javascript:window.location.href='{{ route('users.index') }}'">
                <div class="style-icon-home">
                    <img src="{{URL::asset('/images/icon-menu/user.png')}}">
                    <label for="">USUARIOS</label>
                </div>
            </div>
        </div>
        @endif
    <div class="col options-home">
        <div class="options text-center" onclick="javascript:window.location.href='{{ route('specimens.index') }}'">
            <div class="style-icon-home">
                <img src="{{URL::asset('/images/icon-menu/ejemplares.png')}}" >
                <label for="">EJEMPLARES</label>
            </div>
        </div>
    </div>
    <div class="col options-home">
        <div class="options text-center">
            <div class="style-icon-home">
                <img src="{{URL::asset('/images/icon-menu/entrenamiento.png')}}" >
                <label for="">ENTRENAMIENTO</label>
            </div>
            
        </div>
    </div>
</div>
<div class="row col-12 general-options">
    <div class="col options-home">
            <div class="options text-center">
                <div class="style-icon-home">
                    <img src="{{URL::asset('/images/icon-menu/encaste.png')}}" >
                    <label for="">ENCASTE</label>
                </div>
            </div>
        </div>
    <div class="col options-home">
            <div class="options text-center">
                <div class="style-icon-home">
                    <img src="{{URL::asset('/images/icon-menu/torneo.png')}}" >
                    <label for="">TORNEOS</label>
                </div>
            </div>
        </div>
    <div class="col options-home">
        <div class="options text-center">
            <div class="style-icon-home">
                <img src="{{URL::asset('/images/icon-menu/administrativo.png')}}">
                <label for="">ADMINISTRATIVO</label>
            </div>
        </div>
    </div>
</div>
@endsection
