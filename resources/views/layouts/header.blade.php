<header>
   <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <span class="name-sistem" href="#">Smart Rooster Bureau</span>
            <div class="role-user text-left">
                <label class=" mb-0 pb-0" for="" >BIENVENIDO <a class="name-user" href={{"/users/edit/" .currentUser()->user_id }} >{{currentUser()->name_user}}</a></label>
                <br>
                <span> {{currentUser()->u_role}}</span>
            </div>
        </div>
      <ul class="nav navbar-nav navbar-right">
        <li>
            <div class="row">
                <div class="w-100 info-user">
                    <div class="mb-0 class-icon-sistem">
                        <div class="img">
                            <img src="{{URL::asset('/images/icono.png')}}" alt="" class="image-sistem">
                        </div>
                        <div class="icon-logout">
                            <a  href="{{ url('/logout') }}" title="Salir">
                                <i class="fas fa-sign-out-alt"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </li>
      </ul>
    </div>
  </nav>
  @if( Request::path() != 'home')
  <div class="nav">
    <input type="checkbox" id="nav-check">
    <div class="nav-btn">
      <label for="nav-check">
        <span></span>
        <span></span>
        <span></span>
      </label>
    </div>
    <div class="nav-links">
      <ul class="list-unstyled components">
        <li class="{{ request()->is('users*') ? 'active-link  ' : '' }}">
            <div class="line-bottom-header" title="Usuarios">
                <a href="{{ route('users.index') }}">
                  <img src="{{URL::asset('/images/icon-menu/user.png')}}" class="image-list" style="width:28px">
                  <span class="spanname" style="margin-left:30px">Usuarios</span>
                </a>
            </div>
        </li>
        <li class="{{ request()->is('specimens*') ?  'active-link  ' : '' }}">
            <div class="line-bottom-header" title="Ejemplares">
                <a href="{{ route('specimens.index') }}">
                    <img src="{{URL::asset('/images/icon-menu/ejemplares.png')}}" class="image-list" style="width:28px">
                    <span class="spanname" style="margin-left:30px">Ejemplares</span>
                </a>
            </div>
        </li>
        <li >
          <div class="line-bottom-header" title="Entrenamiento">
              <a href="#">
                  <img src="{{URL::asset('/images/icon-menu/entrenamiento.png')}}" class="image-list" style="width:28px">
                  <span class="spanname" style="margin-left:30px">Entrenamiento</span>
              </a>
          </div>
      </li>
      <li >
        <div class="line-bottom-header" title="Encaste">
            <a href="#">
                <img src="{{URL::asset('/images/icon-menu/encaste.png')}}" class="image-list" style="width:28px">
                <span class="spanname" style="margin-left:30px">Encaste</span>
            </a>
        </div>
      </li>
      <li >
        <div class="line-bottom-header" title="Torneos">
            <a href="#">
                <img src="{{URL::asset('/images/icon-menu/torneo.png')}}" class="image-list" style="width:28px">
                <span class="spanname" style="margin-left:30px">Torneos</span>
            </a>
        </div>
      </li>
      <li >
        <div class="line-bottom-header" title="Administrativo">
            <a href="#">
                <img src="{{URL::asset('/images/icon-menu/administrativo.png')}}" class="image-list" style="width:28px">
                <span class="spanname" style="margin-left:30px">Administrativo</span>
            </a>
        </div>
      </li>
    </ul>
    </div>
  </div>
@endif
</header>