<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/images/icono.png" type="image/x-icon" />
    <title>{{ config('app.name', 'Rooster') }}</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.5.16/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZjQ5MsQSX0Hibxr8_txLiJyJo6m9Ph00&libraries=places"
    async defer></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://unpkg.com/vue-lazyload/vue-lazyload.js"></script>
    <meta name="keywords" content="video,thumbnails,preview,metadata,blob" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @if ( Request::path() != 'login'  )
        @include('layouts.header')
        @endif
        <div class="wrapper d-flex align-items-stretch">
            @if ( Request::path() != 'login')
                @if ( Request::path() != 'home')

                <nav id="sidebar" class="active">
                    <h1><a href="{{ route('home') }}"><i class="fas fa-home ml-2"  ></i></a></h1>
                    <ul class="list-unstyled components mb-5">
                        <li >
                            <div class="line-bottom">
                                <button class="btn" id="sidebarCollapse" >
                                    <i class="fas fa-ellipsis-v"></i>
                                </button>
                            </div>
                        </li>
                        <li class="{{ request()->is('users*') ? 'active-link  ' : '' }}">
                            <div class="line-bottom" title="Usuarios">
                                <a href="{{ route('users.index') }}">
                                    <img src="{{URL::asset('/images/icon-menu/user.png')}}" class="image-list" style="width:28px">
                                    <span class="spanname" style="margin-left:30px;display:none;">Usuarios</span>
                                </a>
                            </div>
                        </li>
                        <li class="{{ request()->is('specimens*')  ?  'active-link  ' : '' }}">
                            <div class="line-bottom" title="Ejemplares">
                                <a href="{{ route('specimens.index') }}">
                                    <img src="{{URL::asset('/images/icon-menu/ejemplares.png')}}" class="image-list" style="width:28px">
                                    <span class="spanname" style="margin-left:30px;display:none;">Ejemplares</span>
                                </a>
                            </div>
                        </li>
                        <li >
                            <div class="line-bottom" title="Entrenamiento">
                                <a href="#">
                                    <img src="{{URL::asset('/images/icon-menu/entrenamiento.png')}}"  class="image-list" style="width:28px">
                                    <span class="spanname" style="margin-left:30px;display:none;">Entrenamiento</span>
                                </a>
                            </div>
                        </li>
                        <li >
                            <div class="line-bottom" title="Encaste">
                                <a href="#">
                                    <img src="{{URL::asset('/images/icon-menu/encaste.png')}}" class="image-list" style="width:28px">
                                    <span class="spanname" style="margin-left:30px;display:none;">Encaste</span>
                                </a>
                            </div>
                        </li>
                        <li >
                            <div class="line-bottom" title="Torneos">
                                <a href="#">
                                    <img src="{{URL::asset('/images/icon-menu/torneo.png')}}" class="image-list" style="width:28px">
                                    <span class="spanname" style="margin-left:30px;display:none;">Torneos</span>
                                </a>
                            </div>
                        </li>
                        <li >
                            <div class="line-bottom" title="Administrativo">
                                <a href="#">
                                    <img src="{{URL::asset('/images/icon-menu/administrativo.png')}}" class="image-list" style="width:28px">
                                    <span class="spanname" style="margin-left:30px;display:none;">Administrativo</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                @endif
            @endif

            @if( Request::path() == 'home')
            <div id="content" class="content p-5" style="background-image: url('./images/Fondo.jpg');background-size: cover;">
            @elseif(Request::path() == 'login')
            <div id="content" class="content " style="background-image: url('./images/Fondo.jpg');background-size: cover;">
            @else
            <div id="content" class="content p-5" >
            @endif
                @yield('content')
            </div>
        </div>
        <preloader-load></preloader-load>
        <progress-bar></progress-bar>
    </div>
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>

</body>
</html>
