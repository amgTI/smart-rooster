<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/images/icono.png" type="image/x-icon" />
    <title>{{ config('app.name', 'Rooster') }}</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZjQ5MsQSX0Hibxr8_txLiJyJo6m9Ph00&libraries=places"
    async defer></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://unpkg.com/vue-lazyload/vue-lazyload.js"></script>
    <meta name="keywords" content="video,thumbnails,preview,metadata,blob" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container content-login">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div  >
                    <div class="card-body" style="background-image: url('./images/login.jpg');background-size: cover;height:500px">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div id="imglogin">
                                <img src="./images/icono.png" alt="" style="width:100%">
                            </div>
                            <div class="container pb-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="form-group">
                                        <div class="input-login" >
                                            <input id="email" type="email" class="form-control transparent text-center @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Usuario">
                                        </div>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-1">
                                    <div class="form-group">
                                        <div class="input-login" >
                                            <input id="password" type="password" class="form-control transparent text-center @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contrase&ntilde;a">
                                        </div>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 offset-md-1 text-center">
                                <div class="form-group row mb-0 button-login">
                                    <button type="submit" class="btn btn-primary form-control">
                                        {{ __('INICIAR SESION') }}
                                    </button>
                                </div>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Olvidaste tu contrase&ntilde;a?
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-8 offset-md-2 text-left">
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember">
                                            Recu&eacute;rdame
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>

</body>
</html>
