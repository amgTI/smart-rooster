@extends('layouts.app')

@section('content')
<div class="container content-login">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div  >
                <div class="card-body" style="background-image: url('./images/login.jpg');background-size: cover;height:500px">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div id="imglogin">
                            <img src="./images/icono.png" alt="" style="width:100%">
                        </div>
                        <div class="container pb-0 mt-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="form-group">
                                    <div class="input-login" >
                                        <input id="email" type="email" class="form-control transparent text-center @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Usuario">
                                    </div>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-10 offset-md-1">
                                <div class="form-group">
                                    <div class="input-login" >
                                        <input id="password" type="password" class="form-control transparent text-center @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contrase&ntilde;a">
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 offset-md-1 text-center">
                            <div class="form-group row mb-0 button-login">
                                <button type="submit" class="btn btn-primary form-control">
                                    {{ __('INICIAR SESION') }}
                                </button>
                            </div>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Olvidaste tu contrase&ntilde;a?
                                </a>
                            @endif
                        </div>
                        <div class="col-md-8 offset-md-2 text-left">
                            <div class="form-group row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        Recu&eacute;rdame
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
