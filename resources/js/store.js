export default {
    state: {
        currentUser: null,
        userRoles: null,
        arrayNotifications: null,
        amountNotifications: null,
        amountLoteNotifications: null,
        modalDetalleSpecimen: false,
        modalCreateRegisterPostura: false
    },
    getters:{
        currentUser(state){
            return state.currentUser;
        },
        userRoles(state){
            return state.userRoles;
        },
        arrayNotifications(state){
            return state.arrayNotifications
        },
        amountNotifications(state){
            return state.amountNotifications
        },
        amountLoteNotifications(state){
            return state.amountLoteNotifications
        },
        modalDetalleSpecimen(state){
            return state.modalDetalleSpecimen
        },
        modalCreateRegisterPostura(state){
            return state.modalCreateRegisterPostura
        },
        isOwner(state) {
            return state.currentUser ? state.currentUser.role_id == 1 : false;
        },
    },
    mutations:{
        'SET_CURRENT_USER'(state, payload){
            state.currentUser = payload
        },
        'SET_ARRAY_NOTIFICATIONS'(state, payload){
            state.arrayNotifications = payload
        },
        'SET_AMOUNT_NOTIFICATIONS'(state, payload){
            state.amountNotifications = payload
        },
        'SET_AMOUNT_LOTE_NOTIFICATIONS'(state, payload){
            state.amountLoteNotifications = payload
        },
        'SET_USER_ROLES'(state, payload){
            state.userRoles = payload
        },
        'SET_MODAL_DETALLE_SPECIMEN'(state, payload){
            state.modalDetalleSpecimen = payload
        },
        'SET_CREATE_REGISTER_POSTURA'(state, payload){
            state.modalCreateRegisterPostura = payload
        }
    },
    actions:{
        updateModalDetalleSpecimen({commit}, payload){
            commit('SET_MODAL_DETALLE_SPECIMEN', payload)
        },
        updateCreateRegisterPostura({commit}, payload){
            commit('SET_CREATE_REGISTER_POSTURA', payload)
        },
        updateCurrentUser({commit}, payload){
            if(payload){
                commit('SET_CURRENT_USER', payload);

                commit('SET_USER_ROLES', payload.role);
            }
        },
        updateUserRoles({commit}, payload){
            commit('SET_USER_ROLES', payload);
        },
        updateArrayNotifications({commit}, payload){
            axios.post('/api/notification', {
                role_id: payload
            }).then(response=>{
                commit('SET_ARRAY_NOTIFICATIONS', response.data)
                let result = response.data.filter(notification => notification.status == 0);
                commit('SET_AMOUNT_NOTIFICATIONS', result.length);
            })
        },
        getLoteNotifications({commit}, payload){
            axios.get('/api/lote/counter').then(response=>{
                commit('SET_AMOUNT_LOTE_NOTIFICATIONS', response.data);
            })
        },
        updateAmountNotifications({commit}, payload){
            commit('SET_AMOUNT_NOTIFICATIONS', payload);
        },
        updateAmountLoteNotifications({commit}, payload){
            commit('SET_AMOUNT_LOTE_NOTIFICATIONS', payload);
        },
        logout({commit}){
            axios.post('logout').then(response=>{
                commit('SET_CURRENT_USER', null);
              })
        }
    }
}


