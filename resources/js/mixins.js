export const misMixins = {
    methods:{
        convertToSpanishDate: function(date) {
            return new Date(date.replace(/-/g, '\/')).toLocaleDateString("es-ES", { year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit', hour12: true})
        },
        convertToSpanishDateWithoutHours: function(date) {
            return new Date(date.replace(/-/g, '\/')).toLocaleDateString("es-ES", { year: 'numeric', month: 'long', day: 'numeric'})
        }
    }
}
