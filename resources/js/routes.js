import LoginPage from './pages/LoginPage'
import MainPage from './pages/MainPage'
import MenuPage from './pages/MenuPage'
//User's imports
import UsersPage from './pages/users/UsersPage'
import CreateUserPage from './pages/users/CreateUserPage'
import EditUserPage from './pages/users/EditUserPage'
//Specimen's imports
import SpecimensPage from './pages/specimens/SpecimensPage'
import SpecimenMainPage from './pages/specimens/SpecimenMainPage'
import CreateSpecimenPage from './pages/specimens/CreateSpecimenPage'
import EditSpecimenPage from './pages/specimens/EditSpecimenPage'
import NotFoundPage from './pages/NotFoundPage'
//Training's imports
import TrainingPage from './pages/training/TrainingPage'
//Embed's imports
import EmbedPage from './pages/embeds/EmbedPage'
//Tournament's imports
import TournamentsPage from './pages/tournaments/TournamentsPage'
//Administrative's imports
import AdministrativePage from './pages/administrative/AdministrativePage'
import IncomesPage from './pages/administrative/IncomesPage'
import ExpensesPage from './pages/administrative/ExpensesPage'
import DashboardAdministrative from './pages/administrative/Dashboard'
//Employee's import
import CreateEmployeePage from './pages/administrative/employees/CreateEmployeePage';
import EmployeesPage from './pages/administrative/employees/EmployeesPage';
//Reproduction's imports
import ReproductionsPage from './pages/reproductions/ReproductionsPage'
import LitterPlanningPage from './pages/reproductions/LitterPlanningPage'
import RegisterEggsPage from './pages/reproductions/registereggs/RegisterEggsPage'
import CreateRegisterEggsPage from './pages/reproductions/registereggs/CreateRegisterEggsPage'
import RaiseYoungPage from './pages/reproductions/RaiseYoungPage'
import ChickHatchingPage from './pages/reproductions/ChickHatchingPage'
import CreateLitterPlanningPage from './pages/reproductions/litterplanning/CreateLitterPlanningPage'
import RaiseYoungCreatePage from './pages/reproductions/raiseyoung/RaiseYoungCreatePage'
import RaiseYoungInfertilesRevisionPage from './pages/reproductions/raiseyoung/RaiseYoungInfertilesRevisionPage'
import RaiseYoungAlivesRevisionPage from './pages/reproductions/raiseyoung/RaiseYoungAlivesRevisionPage'
import RaiseYoungCintilloAlaRevisionPage from './pages/reproductions/raiseyoung/RaiseYoungCintilloAlaRevisionPage'
// Dead specimens's imports
import DeadMainPage from './pages/deads/DeadMainPage';

//Cintillo's imports

import CintilloPage from './pages/cintillos/CintilloPage'
import CintillosMainPage from './pages/cintillos/CintillosMainPage'

//ChickenLeg's imports

import ChickenLegPage from './pages/cintillos/chickenlegs/ChickenLegPage';

//Combination's imports

import CombinationPage from './pages/cintillos/combinations/CombinationPage';
//Migration's imports
import MigrationPage from './pages/reproductions/migration/MigrationPage';
import MigrationGrilla from './pages/reproductions/migration/MigrationGrilla';
import EditMigration from './pages/reproductions/migration/EditMigration';



// Chicks's imports

import ChicksMainPage from './pages/specimens/chicks/ChicksMainPage';

//Videoteca's imports

import VideoMainPage from './pages/multimedia/videoteca/VideoMainPage';

//Reason's imports
import ReasonMainPage from './pages/reasons/ReasonMainPage';
export const routes = [
    {
        path: '/login',
        component: LoginPage
    },
    {
        path: '/main',
        component: MainPage,
        children: [
            {
                path: '/',
                component: MenuPage
            },
            {
                path: 'usuarios',
                component: UsersPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Usuarios",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'usuarios/crear',
                component: CreateUserPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Usuarios",
                            active: false,
                            to: '/main/usuarios/crear'
                        },
                        {
                            text: "Crear",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'usuarios/editar/:id',
                component: EditUserPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Usuarios",
                            active: false,
                            to: '/main/usuarios/crear'
                        },
                        {
                            text: "Editar",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'ejemplares',
                component: SpecimenMainPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Ejemplares",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'pollitos',
                component: ChicksMainPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Pollos",
                            active: true
                        },
                    ]
                }
            },
            {
                path: 'ejemplares/crear',
                component: CreateSpecimenPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Ejemplares",
                            active: false,
                            to: '/main/ejemplares'
                        },
                        {
                            text: "Crear",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'ejemplares/editar/:id',
                component: EditSpecimenPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Ejemplares",
                            active: false,
                            to: '/main/ejemplares'
                        },
                        {
                            text: "Editar",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'entrenamiento',
                component: TrainingPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Entrenamiento",
                            active: true
                        },
                    ]
                }
            },
            {
                path: 'encaste',
                component: EmbedPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Encaste",
                            active: true
                        },
                    ]
                }
            },
            {
                path: 'torneos',
                component: TournamentsPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Torneos",
                            active: true
                        },
                    ]
                }
            },
            {
                path: 'administrativo',
                component: AdministrativePage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Administrativo",
                            active: true
                        },
                    ]
                },
                children:[
                    {
                        path: 'dashboard',
                        component: DashboardAdministrative,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Administrativo",
                                    active: false,
                                    to: "/main/administrativo/dashboard"
                                },
                                {
                                    text: "Dashboard",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'depositos',
                        component: IncomesPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Administrativo",
                                    active: false,
                                    to: "/main/administrativo/dashboard"
                                },
                                {
                                    text: "Depósitos",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'gastos',
                        component: ExpensesPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Administrativo",
                                    active: false,
                                    to: "/main/administrativo/dashboard"
                                },
                                {
                                    text: "Gastos",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'employees',
                        component: EmployeesPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Administrativo",
                                    active: false,
                                    to: "/main/administrativo/dashboard"
                                },
                                {
                                    text: "Empleados",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'employees/crear',
                        component: CreateEmployeePage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Administrativo",
                                    active: false,
                                    to: "/main/administrativo/dashboard"
                                },
                                {
                                    text: "Empleados",
                                    active: false,
                                    to: "/main/administrativo/employees"
                                },
                                {
                                    text: "Crear",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'employees/editar/:employee_id',
                        component: CreateEmployeePage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Administrativo",
                                    active: false,
                                    to: "/main/administrativo/dashboard"
                                },
                                {
                                    text: "Empleados",
                                    active: false,
                                    to: "/main/administrativo/employees"
                                },
                                {
                                    text: "Editar",
                                    active: true
                                },
                            ]
                        }
                    }
                ]
            },
            {
                path: 'reproduccion',
                component: ReproductionsPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Reproduccion",
                            active: true
                        },
                    ]
                },
                children: [
                    {
                        path: 'planificacion-camadas',
                        component: LitterPlanningPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Posturas",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'registro-huevos',
                        component: RegisterEggsPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Huevos",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'sacar-crias',
                        component: RaiseYoungPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Lotes",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'nacimiento-pollos',
                        component: ChickHatchingPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Registro",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'migracion',
                        component: MigrationGrilla,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Migración",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'migracion/crear',
                        component: MigrationPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Migración",
                                    active: false,
                                    to: '/main/reproduccion/migracion'
                                },
                                {
                                    text: "Crear",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'migracion/edit/:migration_id',
                        component: EditMigration,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Migración",
                                    active: false,
                                    to: '/main/reproduccion/migracion'
                                },
                                {
                                    text: "Editar",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'planificacion-camadas/crear',
                        component: CreateLitterPlanningPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Posturas",
                                    active: false,
                                    to: '/main/reproduccion/planificacion-camadas'
                                },
                                {
                                    text: "Crear",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'sacar-crias/crear',
                        component: RaiseYoungCreatePage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Lotes",
                                    active: false,
                                    to: '/main/reproduccion/sacar-crias'
                                },
                                {
                                    text: "Crear",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'sacar-crias/editar/:lote_id',
                        component: RaiseYoungCreatePage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Lotes",
                                    active: false,
                                    to: '/main/reproduccion/sacar-crias'
                                },
                                {
                                    text: "Editar",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'sacar-crias/revision-infertiles/:lote_id',
                        component: RaiseYoungInfertilesRevisionPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Lotes",
                                    active: false,
                                    to: '/main/reproduccion/sacar-crias'
                                },
                                {
                                    text: "Infértiles",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'sacar-crias/revision-vivos/:lote_id',
                        component: RaiseYoungAlivesRevisionPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Lotes",
                                    active: false,
                                    to: '/main/reproduccion/sacar-crias'
                                },
                                {
                                    text: "Vivos",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'sacar-crias/revision-cintillo-ala/:lote_id',
                        component: RaiseYoungCintilloAlaRevisionPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Lotes",
                                    active: false,
                                    to: '/main/reproduccion/sacar-crias'
                                },
                                {
                                    text: "Cintillo ala",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'registro-huevos/crear',
                        component: CreateRegisterEggsPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Huevos",
                                    active: false,
                                    to: '/main/reproduccion/registro-huevos'
                                },
                                {
                                    text: "Crear",
                                    active: true
                                },
                            ]
                        }
                    },
                    {
                        path: 'registro-huevos/editar/:egg_id',
                        component: CreateRegisterEggsPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Huevos",
                                    active: false,
                                    to: '/main/reproduccion/registro-huevos'
                                },
                                {
                                    text: "Crear",
                                    active: true
                                },
                            ]
                        }
                    }
                ]
            },
            {
                path: 'cintillos',
                component: CintillosMainPage,
                children: [
                    {
                        path: 'colores',
                        component: CintilloPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Colores",
                                    active: true,
                                }
                            ]
                        }
                    },
                    {
                        path: 'patas',
                        component: ChickenLegPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Patas",
                                    active: true,
                                }
                            ]
                        }
                    },
                    {
                        path: 'combinaciones',
                        component: CombinationPage,
                        meta: {
                            breadcrumbs: [
                                {
                                    text: "Combinaciones",
                                    active: true,
                                }
                            ]
                        }
                    }
                ],
            },
            {
                path: 'muertos',
                component: DeadMainPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Muertos",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'videoteca',
                component: VideoMainPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Videoteca",
                            active: true,
                        }
                    ]
                }
            },
            {
                path: 'motivos',
                component: ReasonMainPage,
                meta: {
                    breadcrumbs: [
                        {
                            text: "Motivos",
                            active: true,
                        }
                    ]
                }
            }

        ]
    },
    {
        path: '*',
        component: NotFoundPage
    }
]
