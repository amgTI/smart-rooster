/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import { routes } from './routes'
import StoreData from './store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';
import VCalendar from 'v-calendar';
import VueTree from '@ssthouse/vue-tree-chart'
import { Form, HasError, AlertError } from 'vform';
import VueLoading from 'vuejs-loading-plugin'
import htmlToPdf from './components/commons/htmlToPdf'
import vSelect from 'vue-select'
import moment from 'moment';
// using default options
Vue.use(VueLoading)
Vue.use(VueLoading, {
    dark: true, // default false
    text: 'Cargando', // default 'Loading'
    loading: true, // default false
    background: 'rgb(255,255,255)', // set custom background
    classes: ['myclass'] // array, object or string
})
// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../sass/themebootstrap.scss'
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(htmlToPdf)
Vue.use(VCalendar, {
    componentPrefix: 'v',  // Use <vc-calendar /> instead of <v-calendar />,                // ...other defaults
});
Vue.use(VueRouter)
Vue.use(Vuex)
window.swal = Swal;
window.Form = Form;
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

extend('secret', {
    validate: value => value === 'example',
    message: 'This is not the magic word'
});

extend('required', {
    ...required,
    message: 'Este campo es obligatorio'
})

extend('tel', {
    validate: value => value.length <= 9,
    message: 'Este campo requiere 9 digitos o menos'
})


window.toast = toast;

const router = new VueRouter({
    routes,
    mode: 'history'
})

const store = new Vuex.Store(StoreData)



Vue.filter('myGlobalDay', function (date) {
    if (date) return new Date(date.replace(/-/g, '\/')).toLocaleDateString("es-ES", { year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit', hour12: true })
    return '-'
});
Vue.filter('myGlobalDayShort', function (date) {
    if (date) return moment(date).format('MM/DD/YY');
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('v-select', vSelect)
Vue.component('vue-tree', VueTree)
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
//Directives

// users
Vue.component('modal-sessions', require('./components/users/ModalSessions.vue').default);

//Common components

Vue.component('spinner', require('./components/commons/SpinnerComponent').default);
Vue.component('drag-and-drop-component', require('./components/commons/DragAndDropComponent.vue').default);



//specimens
Vue.component('create-specimens', require('./components/specimens/CreateSpecimens.vue').default);
Vue.component('view-image-specimens', require('./components/specimens/ViewImageSpecimen.vue').default);
Vue.component('preloader-load', require('./components/Preloader.vue').default);
Vue.component('progress-bar', require('./components/Progressbar.vue').default);
Vue.component('upload-video', require('./components/videos/UploadVideos.vue').default);
Vue.component('modal-tracking', require('./components/specimens/Tracking.vue').default);
Vue.component('modal-tracking-status', require('./components/specimens/SaveTrackingStatus.vue').default);
Vue.component('app-component', require('./App.vue').default);

import 'vue-select/dist/vue-select.css';
//Vue.component('index-specimens', require('./components/specimens/Index.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        message: 'Hggg!'
    },
    router,
    store
});
